/*jslint jasmine: true */
/**
 * test salus and ayla connectors
 */
define([
	"underscore",
	"spec/SpecHelper",
	"common/util/CoreValidator"
], function (_, SpecHelper, CV) {
	'use strict';

	describe("Basic validation code", function () {
		it("should validate password is long enough", function () {
			expect(CV.password("abcd").valid).toBeFalsy();
			expect(CV.password("abcde").valid).toBeFalsy();
			expect(CV.password("abcd58").valid).toBeTruthy();
			expect(CV.password("8568etu").valid).toBeTruthy();
		});

		// Note: This does not validate that it is an email, just that Ayla will think it is an email!
		it("should validate email is right format (per ayla)", function () {
			expect(CV.email("abcd").valid).toBeFalsy();
			expect(CV.email("ab@cde").valid).toBeFalsy();
			expect(CV.email("ab.cde").valid).toBeFalsy();
			expect(CV.email("a.bc@de").valid).toBeFalsy();
			expect(CV.email("ab@cd5.8").valid).toBeFalsy();
			expect(CV.email("abc@cd5.a").valid).toBeFalsy();
			expect(CV.email("abc@cd5.ab5").valid).toBeFalsy();
			expect(CV.email("abc@cd5.879").valid).toBeFalsy();
			expect(CV.email("abc@cd5.athnn").valid).toBeFalsy();
			expect(CV.email("ab_c   @cd5.aaub").valid).toBeFalsy();
			expect(CV.email("ab  _c@cd5.aaub").valid).toBeFalsy();
			expect(CV.email("ab_c@   cd5.aaub").valid).toBeFalsy();
			expect(CV.email("ab_c@cd   5.aaub").valid).toBeFalsy();
			expect(CV.email("ab_c@cd5   .aaub").valid).toBeFalsy();
			expect(CV.email("ab_c@cd5.   aaub").valid).toBeFalsy();
			expect(CV.email("ab_c@cd5.aa   ub").valid).toBeFalsy();

			expect(CV.email("789@cd5.ab").valid).toBeTruthy();
			expect(CV.email("abc@5555.ab").valid).toBeTruthy();
			expect(CV.email("5@cd5.ab").valid).toBeTruthy();
			expect(CV.email("abc@cd5.ab").valid).toBeTruthy();
			expect(CV.email("8.56@8e.teu").valid).toBeTruthy();
			expect(CV.email("ab-c@cd5.aaub").valid).toBeTruthy();
			expect(CV.email("ab.c@cd5.ab").valid).toBeTruthy();
			expect(CV.email("ab+c@cd5.aeb").valid).toBeTruthy();
			expect(CV.email("ab_c@cd5.aaub").valid).toBeTruthy();
			expect(CV.email("    ab_c@cd5.aaub     ").valid).toBeTruthy();
		});

		it("should validate zip is only digits and from 1 to 9 digits log", function () {
			expect(CV.zip("abcd").valid).toBeFalsy();
			expect(CV.zip("1    23     ").valid).toBeFalsy();
			expect(CV.zip("").valid).toBeFalsy();
			expect(CV.zip(" ").valid).toBeFalsy();
			expect(CV.zip("z").valid).toBeFalsy();
			expect(CV.zip("?").valid).toBeFalsy();

			expect(CV.zip("1").valid).toBeTruthy();
			expect(CV.zip("12345").valid).toBeTruthy();
			expect(CV.zip("123456789").valid).toBeTruthy();

			expect(CV.zip("1234567890").valid).toBeFalsy();
		});

		it("should validate words to only contain letters and spaces.", function () {
			expect(CV.words("apple").valid).toBeTruthy();
			expect(CV.words("I like to east Apples").valid).toBeTruthy();

			expect(CV.words("1").valid).toBeFalsy();
			expect(CV.words("abc-def").valid).toBeFalsy();
			expect(CV.words("$ dollars").valid).toBeFalsy();
		});
	});
});
