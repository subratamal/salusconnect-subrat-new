"use strict";

define([
	"app",
	"common/constants",
	"common/config",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusPage",
	"consumer/views/mixins/mixins",
	"consumer/myStatus/views/MyStatusGroup.view"
], function (App, constants, config, consumerTemplates, SalusPage, ConsumerMixins, StatusGroup) {

	App.module("Consumer.MyStatus.Views", function (Views, App, B, Mn) {
		Views.MyStatusPageView = Mn.LayoutView.extend({
			id: "myStatus-page",
			template: consumerTemplates["myStatus/myStatus"],
			className: "container",
			regions: {
				myStatusRegion: "#bb-add-status-view-region"
			},
			ui: {
				noGatewayText: ".bb-no-gateway-text"
			},
			initialize: function () {
				_.bindAll(this, "initiateRefreshPoll", "clearRefreshPollTimeout", "refreshPoll");
			},
			onBeforeDestroy: function() {
				this.clearRefreshPollTimeout();
			},
			onRender: function () {
				var that = this;

				this.showSpinner({textKey: constants.spinnerTextKeys.loading}, this.$el.find("#bb-add-status-view-region"));

				App.salusConnector.getDataLoadPromise(["devices", "ruleGroups"]).then(function () {
					var ruleGroupCollec = App.salusConnector.getRuleGroupCollection();

					if (ruleGroupCollec) {
                        if(!App.Consumer.MyStatus.Controller.newMyStatus) {
                            ruleGroupCollec.refresh();
                        }
					}

					if (!App.hasCurrentGateway()) {
						that.ui.noGatewayText.removeClass("hidden");
					}

					that.statusGroupsView = new StatusGroup({
						collection: ruleGroupCollec
					});

					that.myStatusRegion.show(that.statusGroupsView);
				});
				
				this.initiateRefreshPoll();
			},
			initiateRefreshPoll: function () {
				this.pollingInterval = setInterval(this.refreshPoll, config.devicePollingInformation.devicePropsPollingInterval);
			},
			clearRefreshPollTimeout: function () {
				clearInterval(this.pollingInterval);
			},
			refreshPoll: function () {
				App.getCurrentGateway().getGatewayNode().getDeviceProperties(true);
			}
		}).mixin([SalusPage], {
			analyticsSection: "myStatus",
			analyticsPage: "myStatus"
		});
	});

	return App.Consumer.MyStatus.Views.MyStatusPageView;
});