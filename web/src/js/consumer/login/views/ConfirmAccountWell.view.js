"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView"
], function (App, consumerTemplates, SalusView) {

	App.module("Consumer.Login.Views", function (Views, App, B, Mn) {
		Views.ConfirmAccountWell = Mn.ItemView.extend({
			id: "confirm-account-well",
			template: consumerTemplates["login/confirmAccountWell"],
			ui: {
				thanks: "#bb-thanks-for-signup"
			},
			initialize: function () {
				// get user and clear it right after
				this.userObj = App.salusConnector.getSessionUser();
				App.salusConnector.unsetAylaUser();
			},
			onRender: function () {
				if (this.userObj) {
					var text = this.userObj.get("firstname") || this.userObj.get("email");
					if (text === this.userObj.get("email")) {
						this.ui.thanks.text(App.translate("login.confirm.resend", {
							"user_name": text
						}));
					} else {
						this.ui.thanks.text(App.translate("login.confirm.thanks", {
							"user_name": text
						}));
					}
				} else {
					// in case of page refresh...
					App.navigate("");
				}
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.Login.Views.ConfirmAccountWell;
});
