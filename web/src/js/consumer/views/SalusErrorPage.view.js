"use strict";

define([
	"app",
	"common/constants",
	"consumer/consumerTemplates",
	'consumer/views/mixins/mixin.salusPage',
	"consumer/views/mixins/mixin.salusCollapsableView"
], function (App, constants, consumerTemplates, SalusPage, SalusCollapsableMixin) {

	App.module("Consumer.Views", function (Views, App, B, Mn) {

		Views.ErrorDetailView = Mn.ItemView.extend({
			template: consumerTemplates["base/errorDetail"],
			bindings: {
				".bb-status-code": "status",
				".bb-service-endpoint": "endpoint",
				".bb-status-text": "statusText",
				".bb-response-text": "reponse"
			}
		}).mixin([SalusCollapsableMixin]);

		Views.SalusErrorPage = Mn.ItemView.extend({
			template: consumerTemplates["base/errorPage"],
			className: "container",
			id: "salus-error-page",
			bindings: {
				".bb-more-detail": {
					observe: "isDetailsExpanded",
					onGet: function (value) {
						if (value) {
							return App.translate("common.lessDetails");
						} else {
							return App.translate("common.moreDetails");
						}
					}
				}
			},
			events: {
				"click .bb-more-detail": "expandCollapseDetails"
			},
			initialize: function () {
				this.detailView = new Views.ErrorDetailView({ model: this.model });
				this.model.set("isDetailsExpanded", false);
			},
			onRender: function () {
				this.$(".bb-detail-section").append(this.detailView.render().$el);
				App.stopWindow(); // stop all requests
				App.vent.trigger("window:stop");
			},
			onDestroy: function () {
				this.detailView.destroy();
			},
			expandCollapseDetails: function () {
				this.detailView.collapseOrExpand(this.model.get("isDetailsExpanded"));
				this.model.set("isDetailsExpanded", !this.model.get("isDetailsExpanded"));

			}
		}).mixin([SalusPage], {
			analyticsSection: "error",
			analyticsPage: "networkError"
		});
	});

	return App.Consumer.Views.SalusErrorPage;
});