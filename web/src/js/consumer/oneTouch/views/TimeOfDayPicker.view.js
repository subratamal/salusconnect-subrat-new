"use strict";

define([
	"app",
	"moment",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/FormTextInput.view"
], function (App, moment, constants, consumerTemplates, SalusView, FormTextInput) {

	App.module("Consumer.OneTouch.Views", function (Views, App, B, Mn, $, _) {
		Views.TimeOfDayPickerView = Mn.ItemView.extend({
			className: "time-of-day-menu",
			template: consumerTemplates["oneTouch/timeOfDayPicker"],
			ui: {
				startTime: ".bb-start-time",
				endTime: ".bb-end-time"
			},
			initialize: function (/*options*/) {
				_.bindAll(this, "showError", "getRuleData");

				this.startTimeInput = new FormTextInput({
					labelText: "equipment.oneTouch.menus.when.timeOfDay.startTime",
					value: moment().format("HH:mm"),
					classes: "time-input-box"
				});

				this.endTimeInput = new FormTextInput({
					labelText: "equipment.oneTouch.menus.when.timeOfDay.endTime",
					value: moment(moment() + 60000).format("HH:mm"),
					classes: "time-input-box"
				});
			},
			onRender: function () {
				this.ui.startTime.append(this.startTimeInput.render().$el);
				this.ui.endTime.append(this.endTimeInput.render().$el);
			},
			onDestroy: function () {
				this.startTimeInput.destroy();
				this.endTimeInput.destroy();
			},
			getRuleData: function () {
				var startMoment = moment(this.startTimeInput.getValue(), "HH:mm"),
					endMoment = moment(this.endTimeInput.getValue(), "HH:mm");

				// hold some bools
				var valid = {
					start: startMoment.isValid(),
					end: startMoment.isValid()
				};

				// didn't make it through parse - show a format error
				if (!valid.start) {
					this.showError("equipment.oneTouch.menus.when.timeOfDay.formatError", true);
				}

				if (!valid.end) {
					this.showError("equipment.oneTouch.menus.when.timeOfDay.formatError");
				}

				if (valid.start && valid.end) {
//                if (valid.start) {
					if (startMoment.isAfter(endMoment)) {
						this.showError("equipment.oneTouch.menus.when.timeOfDay.endTimeError");
						return false;
					}
                    
//                    var endTime=parseInt(moment(this.startTimeInput.getValue(), "HH:mm").add(1,"minutes").format("HHmmss"));

					return {
						type: "tod",
						startTime: parseInt(startMoment.format("HHmmss")),
						endTime: parseInt(endMoment.format("HHmmss"))
					};
				} else {
					return false;
				}
			},
			/**
			 * show an error on textboxs
			 * @param errorKey
			 * @param onStartTimeView - default no param - show on endTimeInput
			 */
			showError: function (errorKey, onStartTimeView) {
				if (onStartTimeView) {
					this.startTimeInput.showErrors(errorKey);
				} else {
					this.endTimeInput.showErrors(errorKey);
				}
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.OneTouch.Views.TimeOfDayPickerView;
});