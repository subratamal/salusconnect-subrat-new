"use strict";

define([
	"app",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/equipment/views/myEquipment/AddNewTile.view"
], function (App, constants, consumerTemplates, SalusViewMixin) {

	App.module("Consumer.Schedules.Views", function (Views, App, B, Mn) {

		Views.ScheduleEmptyLayoutView = Mn.LayoutView.extend({
			id: "schedule-empty-layout",
			className: "col-xs-12",
			template: consumerTemplates["schedules/scheduleEmptyLayout"],
			regions: {
				addEquipmentTileRegion: ".bb-add-equipment-tile"
			},
			ui: {
				messageText: ".bb-empty-view-message"
			},

			initialize: function () {
				this.deviceCategory = this.options.deviceCategory;

				this.addEquipmentTile = new App.Consumer.Equipment.Views.AddNewTileView({
					i18nTextKey: "equipment.myEquipment.groups.addNewEquipment",
					size: "regular",
					classes: "schedule-add-new-equipment-tile",
					clickDestination: "/equipment"
				});
			},

			onRender: function () {
				var deviceType,
					categories = constants.categoryTypes;

				switch (this.deviceCategory) {
					case categories.THERMOSTATS:
						deviceType = App.translate("schedules.deviceTypes.thermostat");
						break;
					case categories.WATERHEATERS:
						deviceType = App.translate("schedules.deviceTypes.waterHeater");
						break;
					case categories.SMARTPLUGS:
						deviceType = App.translate("schedules.deviceTypes.smartPlug");
						break;
					default:
						throw new Error("Device category is not valid");
				}

				this.ui.messageText.text(App.translate("schedules.emptyView.message", {type: deviceType}));

				this.addEquipmentTileRegion.show(this.addEquipmentTile);
			}
		}).mixin([SalusViewMixin]);
	});

	return App.Consumer.Schedules.Views.ScheduleEmptyLayoutView;
});
