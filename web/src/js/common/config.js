"use strict";

define([], function () {

	var config = {
		languagePrefix: "common.languages",
		languages: [
            "en-us", //"English (US)"
            "da", //"Danish"
            "nl", //"Dutch"
            "fr", //"French"
			"de", //"German"
			//"en-uk", //"English (UK)"
            "no", //"Norwegian"
            "pl", //"Polish"
			//"hu", //"Hungarian"
			"ro", //"Romanian"
			//"es", //"Spanish"
			"ru", //"Russian"
			//"cs", //"Czech"
			//"it", //"Italian"
			//"sk", //"Slovak"
			"sv"//"Swedish"
			//"sl", //"Slovenian"
		],
        languages_da: [
            "en-us", //"English (US)"
            "da", //"Danish"
            "fr", //"French"
            "nl", //"Dutch"
            "no", //"Norwegian"
            "pl", //"Polish"
			"ro", //"Romanian"
			"ru", //"Russian"
			"sv",//"Swedish"
            "de" //"German"
		],
        languages_nl: [
            "en-us", //"English (US)"
            "da", //"Danish"
            "de", //"German"
            "fr", //"French"
            "nl", //"Dutch"
            "no", //"Norwegian"
            "pl", //"Polish"
			"ro", //"Romanian"
			"ru", //"Russian"
			"sv"//"Swedish"
		],
        languages_fr: [
            "en-us", //"English (US)"
            "de", //"German"
            "da", //"Danish"
            "fr", //"French"
            "nl", //"Dutch"
            "no", //"Norwegian"
            "pl", //"Polish"
			"ro", //"Romanian"
			"ru", //"Russian"
			"sv"//"Swedish"
		],
        languages_de: [
            "en-us", //"English (US)"
            "da", //"Danish"
            "de", //"German"
            "fr", //"French"
            "nl", //"Dutch"
            "no", //"Norwegian"
            "pl", //"Polish"
			"ro", //"Romanian"
			"ru", //"Russian"
			"sv"//"Swedish"
		],
        languages_no: [
            "en-us", //"English (US)"
            "da", //"Danish"
            "fr", //"French"
            "nl", //"Dutch"
            "no", //"Norwegian"
            "pl", //"Polish"
			"ro", //"Romanian"
			"ru", //"Russian"
			"sv",//"Swedish"
            "de" //"German"
		],
        languages_pl: [
            "en-us", //"English (US)"
            "da", //"Danish"
            "fr", //"French"
            "nl", //"Dutch"
			"de", //"German"
            "no", //"Norwegian"
            "pl", //"Polish"
            "ru", //"Russian"
			"ro", //"Romanian"
			"sv"//"Swedish"
		],
        languages_ro: [
            "en-us", //"English (US)"
            "da", //"Danish"
            "fr", //"French"
			"de", //"German"
            "no", //"Norwegian"
            "nl", //"Dutch"
            "pl", //"Polish"
			"ro", //"Romanian"
			"ru", //"Russian"
			"sv"//"Swedish"
		],
        languages_ru: [
            "en-us", //"English (US)"
            "da", //"Danish"
            "nl", //"Dutch"
            "fr", //"French"
			"de", //"German"
            "no", //"Norwegian"
            "pl", //"Polish"
			"ro", //"Romanian"
			"ru", //"Russian"
			"sv"//"Swedish"
		],
        languages_sv: [
            "en-us", //"English (US)"
            "da", //"Danish"
            "fr", //"French"
            "nl", //"Dutch"
            "no", //"Norwegian"
            "pl", //"Polish"
			"ro", //"Romanian"
			"ru", //"Russian"
			"sv",//"Swedish"
            "de" //"German"
		],
		countryPrefix: "common.countries",
		//Alphabetical order by English
		countries: [
			"at", //Austria
			"be", //Belgium
			"cz", //Czech Republic
			"dk", //Denmark
			"fi", //Finland
			"fr", //France
			"de", //Germany
			"is", //Ireland
			"it", //Italy
			"nl", //Netherlands
			"no", //Norway
			"pl", //Poland
			"ro", //Romania
			"ru", //Russia
			"es", //Spain
			"sv", //Sweden
			"ch", //Switzerland
			"ua", //Ukraine
			"uk", //United Kingdom
			"us", //United States
		],
        timeZonePrefix: "common.timeZones",
        timeZoneConvertedPrefix: "common.timeZonesConverted",
        timeZones: [
            "Amsterdam",
            "Andorra",
            "Athens",
            "Belfast",
            "Belgrade",
            "Berlin",
            "Bratislava",
            "Brussels",
            "Bucharest",
            "Budapest",
            "Busingen",
            "Chisinau",
            "Copenhagen",
            "Dublin",
            "Gibraltar",
            "Guernsey",
            "Helsinki",
            "Isle_of_Man",
            "Istanbul",
            "Jersey",
            "Kaliningrad",
            "Kiev",
            "Lisbon",
            "Ljubljana",
            "London",
            "Luxembourg",
            "Madrid",
            "Malta",
            "Mariehamn",
            "Minsk",
            "Monaco",
            "Moscow",
            "Nicosia",
            "Oslo",
            "Paris",
            "Podgorica",
            "Prague",
            "Riga",
            "Rome",
            "Samara",
            "San_Marino",
            "Sarajevo",
            "Simferopol",
            "Skopje",
            "Sofia",
            "Stockholm",
            "Tallinn",
            "Tirane",
            "Tiraspol",
            "Uzhgorod",
            "Vaduz",
            "Vatican",
            "Vienna",
            "Vilnius",
            "Volgograd",
            "Warsaw",
            "Zagreb",
            "Zaporozhye",
            "Zurich"
        ],
        timeZonePrefix_us: "common.timeZones_us",
        timeZoneConvertedPrefix_us: "common.timeZonesConverted_us",
        timeZones_us: [
            "Adak",
            "Anchorage",
            "Anguilla",
            "Antigua",
            "Araguaina",
            "Buenos_Aires",
            "Catamarca",
            "ComodRivadavia",
            "Cordoba",
            "Jujuy",
            "La_Rioja",
            "Mendoza",
            "Rio_Gallegos",
            "Salta",
            "San_Juan",
            "San_Luis",
            "Tucuman",
            "Ushuaia",
            "Aruba",
            "Asuncion",
            "Atikokan",
            "Atka",
            "Bahia",
            "Bahia_Banderas",
            "Barbados",
            "Belem",
            "Belize",
            "Blanc-Sablon",
            "Boa_Vista",
            "Bogota",
            "Boise",
            "Buenos_Aires",
            "Cambridge_Bay",
            "Campo_Grande",
            "Cancun",
            "Caracas",
            "Catamarca",
            "Cayenne",
            "Cayman",
            "Chicago",
            "Chihuahua",
            "Coral_Harbour",
            "Cordoba",
            "Costa_Rica",
            "Creston",
            "Cuiaba",
            "Curacao",
            "Danmarkshavn",
            "Dawson",
            "Dawson_Creek",
            "Denver",
            "Detroit",
            "Dominica",
            "Edmonton",
            "Eirunepe",
            "El_Salvador",
            "Ensenada",
            "Fort_Wayne",
            "Fortaleza",
            "Glace_Bay",
            "Godthab",
            "Goose_Bay",
            "Grand_Turk",
            "Grenada",
            "Guadeloupe",
            "Guatemala",
            "Guayaquil",
            "Guyana",
            "Halifax",
            "Havana",
            "Hermosillo",
            "Indianapolis",
            "Knox",
            "Marengo",
            "Petersburg",
            "Tell_City",
            "Vevay",
            "Vincennes",
            "Winamac",
            "Indianapolis_",
            "Inuvik",
            "Iqaluit",
            "Jamaica",
            "Jujuy",
            "Juneau",
            "Louisville",
            "Monticello",
            "Knox_IN",
            "Kralendijk",
            "La_Paz",
            "Lima",
            "Los_Angeles",
            "Louisville",
            "Lower_Princes",
            "Maceio",
            "Managua",
            "Manaus",
            "Marigot",
            "Martinique",
            "Matamoros",
            "Mazatlan",
            "Mendoza",
            "Menominee",
            "Merida",
            "Metlakatla",
            "Mexico_City",
            "Miquelon",
            "Moncton",
            "Monterrey",
            "Montevideo",
            "Montreal",
            "Montserrat",
            "Nassau",
            "New_York",
            "Nipigon",
            "Nome",
            "Noronha",
            "Beulah",
            "Center",
            "New_Salem",
            "Ojinaga",
            "Panama",
            "Pangnirtung",
            "Paramaribo",
            "Phoenix",
            "Port-au-Prince",
            "Port_of_Spain",
            "Porto_Acre",
            "Porto_Velho",
            "Puerto_Rico",
            "Rainy_River",
            "Rankin_Inlet",
            "Recife",
            "Regina",
            "Resolute",
            "Rio_Branco",
            "Rosario",
            "Santa_Isabel",
            "Santarem",
            "Santiago",
            "Santo_Domingo",
            "Sao_Paulo",
            "Scoresbysund",
            "Shiprock",
            "Sitka",
            "St_Barthelemy",
            "St_Johns",
            "St_Kitts",
            "St_Lucia",
            "St_Thomas",
            "St_Vincent",
            "Swift_Current",
            "Tegucigalpa",
            "Thule",
            "Thunder_Bay",
            "Tijuana",
            "Toronto",
            "Tortola",
            "Vancouver",
            "Virgin",
            "Whitehorse",
            "Winnipeg",
            "Yakutat",
            "Yellowknife"
        ],
		notifications: {
			monthsDisplayed: 2,
			shortenedTextMaxLength: 300
		},
		tiles: {
			frontTitleMaxLength: 28,
			backTitleMaxLength: 78,
			ungroupedEquipment: {
				titleMaxLength: 30
			}
		},
		widgets: {
			statusWidget: {
				titleMaxLength: 27
			}
		},
		dashboardHeaderRenderInterval: 60000,
		devicePollingInformation: {
			interval: 3000, //间隔几秒刷新当前gateway下所有设备数据
			fullRefreshCount: 5, //当前gateway下所有设备刷新几次后,就刷新所有gateway下所有设备数据
			refreshLimit: 30, //当前gateway下所有设备刷新几次数据后,就停止刷新（30*30sec)
			devicePropsPollingInterval: 3000, //设备的设置界面和翻转界面,几秒刷新一次
			oneTouchsPollingInterval: 8000 //oneTouch界面,几秒刷新一次
		},
		thermostatMinimumTemp: 5, //degrees Celsius
		usDefaultTimezone: "America/Los_Angeles",
		euDefaultTimezone: "Europe/London",
		thermostatMaximumTemp: 35, //degrees Celsius
		setPermitJoinPeriodLength: 120, // 120 seconds, or 2 minutes
		setPermitJoinRefresh: 100 * 1000, // 100 seconds
		waterHeaterSetModeDelay: 1000, // 1 sec
		thermostatSetPointDelay: 2000, // 2 sec
		provisionDevicesPollRefresh: 5, // 5 seconds
		provisionDevicesPollEnd: 120, // 2 minutes
		setIndicatorPeriodLength: 600, // 60 seconds
		salusAjaxQueueMaxRequests: 6,
		pollingTimeAccumulatorArray: [2, 2, 2, 2, 2, 2, 2, 2, 2, 2], //seconds
		appBackgroundImages: [  // these file paths must remain full to be found by our file hashing script
			"/images/backgrounds/background_aspen_50.jpg",
			"/images/backgrounds/background_france_50.jpg",
			"/images/backgrounds/background_germany_50.jpg",
			"/images/backgrounds/background_italy_50.jpg",
			"/images/backgrounds/background_nederland_50.jpg",
			"/images/backgrounds/background_newyork_50.jpg",
			"/images/backgrounds/background_norway_50.jpg",
			"/images/backgrounds/background_pittsburgh_50.jpg",
			"/images/backgrounds/background_uk_50.jpg",
			"/images/backgrounds/background_uk2_50.jpg"
		],
		splunkHeader: "Splunk A817C168-151C-4C50-A2D0-D0025DA5DCD6",
        encryptParams: {
            key: "54b544979a4ba190ac2b5139b32c3528",
            iv: "be4480f9c146eaf9"
        }
	};

	return config;
});
