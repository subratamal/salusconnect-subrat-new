"use strict";

/**
 * stores session tokens
 */
define([
	"app",
	"bluebird",
	"common/AylaConfig",
	"common/util/utilities",
    "md5"
], function (App, P, AylaConfig, utilities) {

	App.module("Models", function (Models, App, B, Mn, $, _) {
		Models.SessionManager = B.Model.extend({
			_pendingPromise: null,
			defaults: {
				isLoggedIn: null,
				isLoggingIn: null,
				accessToken: "",
				refreshToken: "",
				keepLoggedIn: null
			},

			initialize: function () {
				_.bindAll(this, "loginWithUsername", "refreshSession", "_makeAuthCall", "_processAuthResponse",
					"_onAuthFailure", "_triggerOnLogin", "_triggerOnLogOut");
			},

			start: function () {
				this._setTokensFromCookie();

				this.listenTo(this, "change:accessToken", this._onTokenChange);
				this.listenTo(this, "change:refreshToken", this._onTokenChange);
			},

			_setTokensFromCookie: function () {
				var tokenList,
					storedTokens = window.localStorage.getItem("tokens");

				if (!storedTokens) {
					storedTokens = window.sessionStorage.getItem("tokens");
				} else {
					this.set("keepLoggedIn", true);
				}

				if (storedTokens) {
					tokenList = storedTokens.split("|");
					if (tokenList && tokenList.length === 2 && tokenList[0] && tokenList[1]) {
						this.set({accessToken: tokenList[0], refreshToken: tokenList[1]});
						this.set("isLoggingIn", true);
						this.refreshSession()
							.catch(this._triggerOnLogOut)
							.then(this._triggerOnLogin);
						return;
					}
				}
				window.localStorage.removeItem("tokens");
				window.sessionStorage.removeItem("tokens");
			},

			_onTokenChange: function () {
				var access = this.get("accessToken"),
					refresh = this.get("refreshToken");
				if (this.get("keepLoggedIn")) {
					window.localStorage.setItem("tokens", access + "|" + refresh);
				} else {
					window.sessionStorage.setItem("tokens", access + "|" + refresh);
				}
			},

			/**
			 * several api calls require this application object, so we abstract it here
			 * @param model
			 */
			addAppIdPayload: function (model) {

				App.warn("AddAppIdPayload has been depricated.");

				model.application = {
					app_id: AylaConfig.appId,
					app_secret: AylaConfig.appSecret
				};
			},

			/**
			 * things to do once login is complete
			 * @private
			 */
			_triggerOnLogin: function (shouldTrigger) {
				if (shouldTrigger === undefined || shouldTrigger === true) {
					this.set("isLoggedIn", true);
					this.set("isLoggingIn", false);
					this.trigger("onLogin");
				}
			},

			/**
			 * things to do once we find we are not logged in
			 * @private
			 */
			_triggerOnLogOut: function () {
				var modelProps = {
					accessToken: "",
					refreshToken: "",
					isLoggedIn: false,
					isLoggingIn: false
				};

				this.set(modelProps);
				window.localStorage.removeItem("tokens");
				window.sessionStorage.removeItem("tokens");
                // Dev_SCS-3284
                window.sessionStorage.removeItem("isDemo");
				this.trigger("onLogout");
			},

			makeCordovaCall: function (resolve, reject, method, paramList) {
				// Ask cordova to execute a method on our FileWriter class
				return window.cordova.exec(
					// Register the callback handler
					resolve,
					// Register the errorHandler
					reject,
					// Define what class to route messages to
					'AylaUserPlugin',
					// Execute this method on the above class
					method,
					// An array containing one String (our newly created Date String).
					paramList
				);
			},

			/**
			 * does the login with a username and password. This returns a promise that is resolved on successful login
			 * @param username
			 * @param password
			 */
			loginWithUsername: function (username, password) {
				var that = this;

				if (App.isCordovaApiReady("loginWithUsername")) {
					var appId = AylaConfig.appId,
						appSecret = AylaConfig.appSecret;

					return this._makeCordovaAuthCall("login", [username, password, appId, appSecret])
						.then(that._triggerOnLogin);

				} else {
					var payload = {
						user: {
							email: username,
							password: password
						}
					};

					return this._makeAuthCall(AylaConfig.endpoints.login, payload)
						.then(that._triggerOnLogin);
				}
			},

			/**
			 * log out of the current user
			 */
			logout: function () {
				var that = this,
					promise;

				App.stopWindow();

				if (App.isCordovaApiReady("logout")) {
					var appId = AylaConfig.appId,
						appSecret = AylaConfig.appSecret;

					promise = new P.Promise(function (resolve, reject) {
						return that.makeCordovaCall(resolve, reject, "logout", [appId, appSecret]);
					});

				} else {
					var payload = {
						user: {
							access_token: this.get("accessToken")
						}
					};

					promise = P.resolve(this._jQueryJsonPost(AylaConfig.buildLocalURLForAyla(AylaConfig.endpoints.logout), JSON.stringify(payload, null, 0)));
				}

				return promise.then(function () {
					that._triggerOnLogOut();
				}).catch(function () {
					App.error("Logout failed");
				});
			},

			/**
			 * refresh the access token with the refresh token. This will return a promise. In the event a token refresh is
			 * already running, this will return the existing promise. If no refresh is running this will return, and cache,
			 * a new promise.
			 * @returns {*}
			 */
			refreshSession: function () {
				if (!this.get("refreshToken")) {
					return P.reject("No user session to refresh");
				}

				var promise = this._pendingPromise;
				if (promise) {
					return promise;
				}

				if (App.isCordovaApiReady("refreshSession")) {
					var refresh_token = this.get("refreshToken");
					promise = this._pendingPromise = this._makeCordovaAuthCall("refreshAccessToken", [refresh_token]);

				} else {
					var payload = {
						user: {
							refresh_token: this.get("refreshToken")
						}
					};

					promise = this._pendingPromise = this._makeAuthCall(AylaConfig.endpoints.refreshToken, payload);
				}

				return promise;
			},

			_jQueryJsonPost: function (url, data) {
				return $.ajax({
					url: url,
					contentType: "application/json",
					type: "POST",
					dataType: "json",
					data: data
				});
			},

			/**
			 * after setup calling the login and refresh apis is the same, so abstract that out.
			 * @param url
			 * @param payload (this needs to be stringified to work with the salus web service endpoint)
			 * @returns {*}
			 * @private
			 */
			_makeAuthCall: function (url, payload) {
				var promise = P.resolve(this._jQueryJsonPost(AylaConfig.buildLocalURLForAyla(url), JSON.stringify(payload, null, 0)))
					.then(this._processAuthResponse)
					.catch(this._onAuthFailure);
				return promise;
			},

			_makeCordovaAuthCall: function (method, paramList) {
				var that = this;
				this.set("isLoggingIn", true);
				return new P.Promise(function (resolve, reject) {
					return that.makeCordovaCall(resolve, reject, method, paramList);
				})
					.then(that._processAuthResponse)
					.catch(that._onAuthFailure);
			},

			/**
			 * handle an authentication success response
			 * @param result
			 * @returns {boolean}
			 * @private
			 */
			_processAuthResponse: function (result) {
				var access, refresh;
				this._pendingPromise = null;

				access = result.access_token;
				refresh = result.refresh_token;

				if (!access || !refresh) {
					App.log("token refresh failed");
					this.set({accessToken: null, refreshToken: null});
					App.logout();
					return false;
				}

				this.set({accessToken: access, refreshToken: refresh});
				this.set("isLoggingIn", false);
				return true;
			},

			/**
			 * log an authentication error response and propagate the failure
			 * @param err
			 * @returns {*}
			 * @private
			 */
			_onAuthFailure: function (err) {
				this._pendingPromise = null;
				App.error("Login failed with message: " + (err.responseText || err.message || err.statusText));
				App.vent.trigger("onLoginFailure");
				this.set("isLoggingIn", false);

				return P.reject(err); // this does not eat the error, but rather propegates it after logging it
			},

			_submitData: function (url, method, data) {
				var options = {
					type: method,
					url: AylaConfig.buildLocalURLForAyla(url),
					data: data,
					dataType: "json",
					contentType: "application/json"
				};

				if (_.isObject(data)) {
					options.data =  JSON.stringify(data, null, 0);
				}

				return P.resolve($.ajax(options));
			},

			/**
			 * confirm the email token ownership
			 * @param token
			 */
			confirmUserWithEmailVerificationToken: function (token) {
				return this._submitData(AylaConfig.endpoints.auth.confirmUserToken, "put", {confirmation_token: token});
			},

			/**
			 * Resend the email confirmation instructions to an unconfirmed user.
			 *
			 * On error returns: status:422
			 * {"errors":{"email":["was already confirmed, please try signing in"]}}
			 */
			resendConfirmationToken: function (emailAddress) {
				var url = AylaConfig.endpoints.auth.resendConfirmationToken,
					data = {user: {email: emailAddress}},
					subject = App.translate("email.signup_confirmation.emailSubject");

				App.salusConnector.setAylaUser(new B.Model({email: emailAddress}));

				url = utilities.appendEmailTemplateParameters(url, "signup_confirmation", subject);
				return this._submitData(url, "post", data);
			},

			/**
			 * request a password reset token for an email address
			 * @param email
			 * @returns {*}
			 */
			sendResetPasswordToken: function (email) {
				var data = {user: {email: email}},
					url;

				// we don't have a token yet, should build url with token param
				url = this._buildPasswordURL(false);

				return this._submitData(url, "post", data);
			},

			/**
			 * with an emailed token, set a new password
			 * @param pass1
			 * @param pass2
			 * @param token
			 * @returns {*}
			 */
			//TODO: update to app.validate pattern
			setPasswordWithToken: function (pass1, pass2, token) {
				if (!pass1 || !pass2 || !token) {
					throw new Error("missing parameters");
				}

				if (pass1 !== pass2) {
					throw new Error("passwords must match");
				}

				var url = this._buildPasswordURL(true),
					data = {user: {reset_password_token: token, password: pass1, password_confirmation: pass2}};

				return this._submitData(url, "put", data);
			},

			/**
			 * build password url for cases of sending password reset email and updating of password
			 * @param hasToken
			 * @private
			 */
			_buildPasswordURL: function (hasToken) {
				var query_string_params,
					subject = App.translate("email.password_reset.emailSubject");

				if (hasToken) {
					// do not append to password url if we call when we have a token
					query_string_params = "";
				} else {
					query_string_params = utilities.appendEmailTemplateParameters("", "password_reset", subject);
				}

				return _.template(AylaConfig.endpoints.auth.mgPassword)({query_string_params: query_string_params});
			}
		});
	});

	return App.Models.SessionManager;
});
