"use strict";

define([
	"app",
	"common/constants"
], function (App, constants) {
	App.module("Models", function (Models, App, B, Mn, $, _) {
		Models.IsInDashboardMixin = function () {
			this.setDefaults({
				_updateIsInDash: function (tileOrderCollection) {
					var gatewayDsn;

					if (this.ruleRefType === "device") {
						gatewayDsn = this.get("gateway_dsn");

						if (this.modelType === constants.modelTypes.GATEWAY) {
							gatewayDsn = this.get("dsn");
						}
					} else if (this.ruleRefType === "rule") {
						gatewayDsn = this.get("dsn");
					}

					// unconnected devices cannot be pinned
					if (!gatewayDsn) {
						return;
					}

					// get tileOrderCollection directly from session user if its undefined (only on initial creation of FullDashboardTileOrder)
					tileOrderCollection = !tileOrderCollection ? App.salusConnector.getSessionUser().get("tileOrderCollection").getTileOrderForGateway(gatewayDsn) : tileOrderCollection;

					//devices can only be pinned to their gateway's dashboard
					if (tileOrderCollection && tileOrderCollection.gatewayDsn === gatewayDsn) {
						var isInCollec = tileOrderCollection.findWhere({
							referenceId: this.id,
							referenceType: this.ruleRefType
						});

						this.set("isInDashboard", !!isInCollec);
					}
				}
			});


			this.before("initialize", function () {
				_.bindAll(this, "_updateIsInDash");

				this.set("isInDashboard", false);

				this.listenTo(App.salusConnector.getSessionUser().get("tileOrderCollection"), "tileOrder:modification", this._updateIsInDash);
			});
		};

		return Models.IsInDashboardMixin;
	});
});
