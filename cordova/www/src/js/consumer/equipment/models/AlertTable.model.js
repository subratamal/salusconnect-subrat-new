"use strict";

define([
	"app"
], function (App) {

	App.module("Consumer.Equipment.Models", function (Models, App, B) {
		Models.AlertTableModel = B.Model.extend({
			defaults: {
				sortBy: "NAME",
				sortDesc: true
			},

			sortByEnum: {
				"NAME": {
					isDeviceProp: false,
					modelKey: "name"
				},
				"EQUIP_NAME": {
					isDeviceProp: true,
					modelKey: "product_name"
				},
				"ALIAS": {
					isDeviceProp: true,
					modelKey: "alias"
				},
				"DATE": {
					isDeviceProp: false,
					modelKey: "dateTime"
				}
			}
		});
	});

	return App.Consumer.Equipment.Models.AlertTableModel;
});