"use strict";

define([
	"app"
], function (App) {

	App.module("Consumer.Models", function (Models, App, B) {
		Models.LinkViewModel = B.Model.extend({
			defaults: {
				displayTextKey: "",
				displayTextNonKey: "",
				href: "#noOp",
				clickHandler: null,
				classes: "",
				target: "_self"
			}
		});

		Models.LinkViewModelCollection = B.Collection.extend({
			model: Models.LinkViewModel
		});
	});

	return App.Consumer.Models;
});