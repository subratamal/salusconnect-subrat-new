"use strict";

define([
	"app",
	"bluebird",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/views/modal/ModalCloseButton.view"
], function (App, P, consumerTemplates, SalusView, SalusButton, ModalCloseButton) {

	App.module("Consumer.OneTouch.Views", function (Views, App, B, Mn, $, _) {
		Views.DeleteOneTouchModalView = Mn.LayoutView.extend({
			className: "modal-body delete-one-touch-modal-content",
			regions: {
				leftButtonRegion: "#bb-left-button-region",
				rightButtonRegion: "#bb-right-button-region"
			},
			ui: {
				mainText: "#bb-main-text"
			},
			template: consumerTemplates["oneTouch/deleteOneTouchModal"],
			initialize: function () {
				_.bindAll(this, "handleDeleteClick", "handleCancelClick");

				this.cancelButton = new ModalCloseButton({
					id: "delete-one-touch-btn",
					classes: "width100",
					buttonTextKey: "common.labels.cancel",
					clickedDelegate: this.handleCancelClick
				});

				this.deleteButton = new SalusButton({
					id: "delete-one-touch-btn",
					classes: "btn-danger width100 pull-right",
					buttonTextKey: "common.labels.delete",
					clickedDelegate: this.handleDeleteClick
				});
			},
			onRender: function () {
				this.ui.mainText.text(App.translate("equipment.oneTouch.details.delete.oneTouch", {
					one_touch: this.model.get("name")
				}));

				this.leftButtonRegion.show(this.cancelButton);
				this.rightButtonRegion.show(this.deleteButton);
			},
			handleDeleteClick: function () {
				var that = this;

				this.deleteButton.showSpinner();
				return this.model.unregister().then(function () {
					// clean tile order and clean myStatus'
					App.salusConnector.getSessionUser().get("tileOrderCollection").findAndRemove(that.model, "rule");
					App.getCurrentGateway().trigger("remove:rule", that.model);

					return App.salusConnector.getRuleCollection().refresh().then(function () {
						that.deleteButton.hideSpinner();
						App.hideModal();
						App.navigate("oneTouch");
					});
				}).catch(function (err) {
					that.deleteButton.hideSpinner();
					return P.reject(err);
				});
			},
			handleCancelClick: function () {
				App.hideModal();
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.OneTouch.Views.RemoveOneTouchModalView;
});