"use strict";

define([
	"app"
], function (App) {

	App.module("Consumer.Login.Models", function (Models, App, B) {
		Models.ChangePasswordWithTokenWellModel = B.Model.extend({
			defaults: {
				'newPassword': null,
				'confirmPassword': null,
				'secureToken': null
			}
		});
	});

	return App.Consumer.Login.Models.ChangePasswordWithTokenWellModel;
});