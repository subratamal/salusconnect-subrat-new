"use strict";

define([
	"app",
	"common/config",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusPage",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/mixins/mixins",
	"consumer/models/SalusButtonPrimary",
	"consumer/models/SalusRadioViewModel",
	"consumer/views/FormTextInput.view",
	"consumer/views/SalusButtonPrimary.view",
], function (App, config, consumerTemplates,
			 SalusPage, SalusViewMixin, ConsumerMixins) {

	App.module("Consumer.Settings.Views", function (Views, App, B, Mn, $, _) {
		Views.ChangePasswordPage = Mn.LayoutView.extend({
			template: consumerTemplates["settings/profile/changePassword"],
			id: "change-password",
			regions: {
				"oldPasswordRegion": ".bb-old-password",
				"newPasswordRegion": ".bb-new-password",
				"passwordConfirmRegion":".bb-confirm-password",
				"submitButtonRegion": ".bb-submit-button",
				"cancelButtonRegion": ".bb-cancel-button"
			},
			events: {
				"mouseover .bb-submit-button": "validateForm"
			},
			initialize: function() {
				_.bindAll(this,
						"_validatePasswordConfirm",
						"handleSubmitClicked",
						"validateForm",
						"handleCancelClicked");

				this.oldPasswordField = new App.Consumer.Views.FormTextInput({
					labelText: "settings.profile.changePassword.formFields.oldPassword",
					type: "password",
					required: true,
					model: this.model
				});

				this.newPasswordField = new App.Consumer.Views.FormTextInput({
					labelText: "settings.profile.changePassword.formFields.newPassword",
					type: "password",
					required: true,
					model: this.model
				});

				this.passwordConfirmField = new App.Consumer.Views.FormTextInput({
					labelText: "settings.profile.changePassword.formFields.confirmPassword",
					type: "password",
					validationMethod: this._validatePasswordConfirm,
					required: true,
					model: this.model,
					showFeedback: true,
					preventTrigger: true // textbox that is 2nd pass confirmation should have this
				});

				this.submitButton = new App.Consumer.Views.SalusButtonPrimaryView({
					buttonTextKey: "settings.profile.formFields.submitButton",
					classes: "width100 disabled",
					clickedDelegate: this.handleSubmitClicked
				});

				this.cancelButton = new App.Consumer.Views.SalusButtonPrimaryView({
					buttonTextKey: "common.labels.cancel",
					classes: "col-xs-6",
					className: "width100 btn btn-default",
					clickedDelegate: this.handleCancelClicked
				});

				this.validationNeeded.push(this.oldPasswordField, this.passwordConfirmField, this.newPasswordField);

				this.attachValidationListener(); // to validationNeeded items
			},

			onRender: function() {
				this.oldPasswordRegion.show(this.oldPasswordField);
				this.newPasswordRegion.show(this.newPasswordField);
				this.passwordConfirmRegion.show(this.passwordConfirmField);
				this.submitButtonRegion.show(this.submitButton);
				this.cancelButtonRegion.show(this.cancelButton);
			},
			handleCancelClicked: function () {
				App.navigate("settings/profile");
			},
			_validatePasswordConfirm: function () {
				var pass1 = this.newPasswordRegion.currentView.getValue(),
						pass2 = this.passwordConfirmRegion.currentView.getValue();

				return this.model.validatePassword(pass1, pass2);
			},
			validateForm: function () {
				var valid = this.isValid();

				if (!valid) {
					this.submitButtonRegion.currentView.disable();
				} else {
					this.submitButtonRegion.currentView.enable();
				}

				return valid;
			},
			changePassword: function() {
				var oldPass = this.oldPasswordRegion.currentView.getValue(),
						newPass = this.newPasswordRegion.currentView.getValue();

				return this.model.changePassword(newPass, oldPass);
			},
			handleSubmitClicked: function () {
				var that = this;
				// make sure the form validates before we submit
				if (!this.validateForm()) {
					return;
				}
                
                var userPsw = App.salusConnector.getAylaUserPassword();
                var oldPass = this.oldPasswordRegion.currentView.getValue(),
                    newPass = this.newPasswordRegion.currentView.getValue(),
                    errorMessage = "";
            
                if(oldPass !== userPsw){
                    errorMessage = "Please input correct old password";
                }
                else if(newPass === userPsw){
                    errorMessage = "Old password can not be same with the new password";
                }
                if(errorMessage !== ""){
                     this.errorModal = new App.Consumer.Views.SalusAlertModalView({
                            model: new App.Consumer.Models.AlertModalViewModel({
                                iconClass: "icon-warning",
                                primaryLabelText: errorMessage,
                                rightButton: new App.Consumer.Views.ModalCloseButton({
                                    classes: "btn-danger width100",
                                    buttonTextKey: "common.labels.ok",
                                    clickedDelegate: App.hideModal()
                                }),
                                leftButton: new App.Consumer.Views.ModalCloseButton({
                                    classes: "width100",
                                    buttonTextKey: "common.labels.cancel",
                                    clickedDelegate: App.hideModal()
                                })
                            })
                        });

                        App.modalRegion.show(this.errorModal);
                        App.showModal();
                        return;
                }
                
               

				this.submitButton.showSpinner();

				return this.changePassword().then(function () {
					that.submitButton.hideSpinner();
                    App.salusConnector.setAylaUserPassword(newPass);
					App.navigate("/settings/profile");
				}).catch(function () {
					that.submitButton.hideSpinner();
				});
			}
		}).mixin([SalusPage, ConsumerMixins.FormWithValidation], {
			analyticsSection: "login",
			analyticsPage: "changePassword" //TODO: This should be a function so the equipment Id is recorded.
		});
	});

	return App.Consumer.Settings.Views.ChangePasswordPage;
});
