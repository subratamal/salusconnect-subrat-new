"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/modal/ModalCloseButton.view",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/mixins/mixin.registeredRegions"
], function (App, templates) {

	App.module("Consumer.Settings.Views", function (Views, App, B, Mn, $, _) {
		Views.DeviceShareDeleteModalView = Mn.LayoutView.extend({
			template: templates["settings/manageDevices/deviceShareDeleteModal"],
			className: "modal-body",
			regions: {
				closeButton: ".bb-btn-close",
				deleteButton: ".bb-btn-delete"
			},
			initialize: function () {
				_.bindAll(this, "handleDeleteClick");

				this.registerRegion("closeButton",  new App.Consumer.Views.ModalCloseButton({
					id: "delete-equip-btn",
					buttonTextKey: "common.labels.cancel",
					classes: "width100"
				}));

				this.registerRegion("deleteButton", new App.Consumer.Views.SalusButtonPrimaryView({
					id: "delete-equip-btn",
					buttonTextKey: "common.labels.delete",
					className: "btn btn-danger",
					classes: "width100",
					clickedDelegate: this.handleDeleteClick
				}));
			},
			templateHelpers: function () {
				var userProfile = this.model.get("user_profile");

				return {
					deletePrompt: App.translate("settings.manageDevices.devices.deleteShareModal") +
						" " + userProfile.firstname + " " + userProfile.lastname
				};
			},
			handleDeleteClick: function () {
				var that = this;

				var email = this.model.get("user_email");

				App.Consumer.Settings.Controller.deleteDeviceShare(email).then(function () {
					that.trigger("close:modal");
				});

			}
		}).mixin([App.Consumer.Views.Mixins.RegisteredRegions, App.Consumer.Views.Mixins.SalusView]);
	});

	return App.Consumer.Settings.Views.DeviceShareDeleteModalView;
});