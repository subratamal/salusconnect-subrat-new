"use strict";

define([
	"app",
	"common/AylaConfig",
	"common/model/ayla/mixin.AylaBacked"
], function (App, AylaConfig, AylaBackedMixin) {
	App.module("Models", function (Models, App, B, Mn, $, _) {
		Models.AylaTriggerAppModel = B.Model.extend({
			defaults: {
				name: null // (string) sms || email
			},

			// overriding toJSON to form server looking json and not our models
			toJSON: function () {
				var json = {};

				if (this.get("name") === "sms") {
					json.name = "sms";
					json.param1 = this.get("countryCode");
					json.param2 = this.get("recipient");
					json.param3 = this.get("message");

				} else if (this.get("name") === "email") {
					json.name = "email";
					json.param1 = this.get("recipient");
					json.param3 = this.get("message");
					json.email_subject = this.get("email_subject");
					json.email_template_id = "trigger_app_en";
					json.email_body_html = this.get("message");
					json.username = this.get("username");
				}

				return json;
			},

			aParse: function (data) {
				var parsedData;
				if (data.name === "sms") {
					parsedData = {
						name: "sms",
						countryCode: data.param1,
						recipient: data.param2,
						message: data.param3,
						key: data.key
					};
				} else {
					parsedData = {
						name: "email",
						recipient: data.param1,
						message: data.param3,
						email_subject: data.email_subject,
						email_template_id: data.email_template_id,
						email_body_html: data.email_body_html,
						username: data.username,
						key: data.key
					};
				}

				return parsedData;
			},

			remove: function () {
				if (this.get("key")) {
					var that = this,
							url = _.template(AylaConfig.endpoints.aylaTrigger.triggerAppDelete)({ key: this.get("key")});

					return App.salusConnector.makeAjaxCall(url, null, "DELETE", "text").then(function () {
						that.destroy();
					});
				} else {
					this.destroy();
				}

			}
		}).mixin(AylaBackedMixin, {
			apiWrapperObjectName: "trigger"
		});


		Models.AylaTriggerAppCollection = B.Collection.extend({
			model: Models.AylaTriggerAppModel
		});
	});

	return App.Models;
});