"use strict";

define([
	"app",
	"common/config",
	"consumer/consumerTemplates",
	"consumer/equipment/views/GatewayForm.view",
	"common/model/ayla/deviceTypes/GatewayAylaDevice",
	"consumer/views/mixins/mixin.salusPage",
	"consumer/setupWizard/mixins/mixin.setupWizardPage",
	"consumer/setupWizard/views/WelcomePageTitle.view"
], function (App, config, consumerTemplates, GatewayView, GatewayDevice,
			 SalusPage, SetupWizardPage, WelcomePageTitle) {

	App.module("Consumer.SetupWizard.Views", function (Views, App, B, Mn, $, _) {
		Views.SetupGatewayPage = Mn.LayoutView.extend({
			template: consumerTemplates["setupWizard/gateway"],
			regions: {
				pageTitle: ".bb-page-title",
				gatewayRegion: ".bb-gateway"
			},
			initialize: function () {
				_.bindAll(this, "backToSetup");
			},
			onRender: function () {
				var that = this;

				App.salusConnector.getDoOnLoginPromise().then(function () {
					that.pageTitle.show(new WelcomePageTitle({}));

					that.formView = new GatewayView({
						model: new GatewayDevice(),
						startExpanded: true,
						mode: "create"
					});
                    
                    that.listenTo(that.formView, "added:device", function () {
						// TODO make soft refresh which resolves when devices.json resolves and makes properties/metadata in background
						App.salusConnector.getFullDeviceCollection().load().then(function () {
							that.formView.model.set("product_name", that.formView.model.get("alias"));
							that.formView.model.set("name", that.formView.model.get("alias"));
							that.formView.model.saveProductName();
							App.navigate("setup");
                            App.salusConnector.get("currentGateway").set("name", that.formView.model.get("alias"));
						});
                    });

//					that.listenTo(that.formView, "added:device", that.backToSetup);

					that.gatewayRegion.show(that.formView);
                    $("#bb_main_gateway_header").addClass("hidden");
				});
			},
			backToSetup: function () {
				App.navigate("setup");
			}
		}).mixin([SalusPage, SetupWizardPage], {
			analyticsSection: "setupWizard",
			analyticsPage: "gateway"
		});
	});

	return App.Consumer.SetupWizard.Views.SetupGatewayPage;
});