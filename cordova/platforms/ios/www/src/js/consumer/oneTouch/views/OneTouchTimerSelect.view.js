"use strict";

define([
	"app",
	"momentWrapper",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/SalusCheckbox.view",
	"consumer/models/SalusCheckboxViewModel"
], function (App, moment, constants, consumerTemplates, SalusView) {
	App.module("Consumer.OneTouch.Views", function (Views, App, B, Mn, $, _) {

		Views.OneTouchTimerSelectView = Mn.ItemView.extend({
			className: "timer-do-later",
			template: consumerTemplates["oneTouch/oneTouchTimerSelect"],
			bindings: {
			 	".bb-hour-input input" : "timerHour",
				".bb-minute-input input" : "timerMin"
			},
			initialize: function () {
				_.bindAll(this, "getRuleData");

				this.model = new B.Model({
					"timerHour": 0,
					"timerMin": 0
				});

				this.hoursTextBox = new App.Consumer.Views.SalusTextBox({
					wrapperClassName: "timer-hour-set",
					inputPlaceholder: "",
					inputType: "number",
					overlayedUnits: "",
					hideErrorTriangle: true,
					hideArrows: true
				});

				this.minutesTextBox = new App.Consumer.Views.SalusTextBox({
					wrapperClassName: "timer-minute-set",
					inputPlaceholder: "",
					inputType: "number",
					overlayedUnits: "",
					hideErrorTriangle: true,
					hideArrows: true
				});
			},
			onRender: function () {
				this.$(".bb-hour-set-row .bb-hour-input").append(this.hoursTextBox.render().$el);
				this.$(".bb-minute-set-row .bb-minute-input").append(this.minutesTextBox.render().$el);
			},
			onDestroy: function () {
				this.hoursTextBox.destroy();
				this.minutesTextBox.destroy();
			},
			getRuleData: function () {
				var totalTime = 0,
					subtype = constants.thenDoLaterSubTypes.none,
					data = [],
					timeHour = parseInt(this.model.get("timerHour")),
					timeMin = parseInt(this.model.get("timerMin"));

				if(_.isNumber(timeHour) && timeHour > 0) {
					totalTime += (timeHour * 60);
				}
				if(_.isNumber(timeMin) && timeMin > 0) {
					totalTime += timeMin;
				}

				if (this.options.previousUniqueViewRuleData) {
					subtype = this.options.previousUniqueViewRuleData.type;
					data = this.options.previousUniqueViewRuleData;
				}

				return {
					type: "timer",
					subType: subtype,
					totalTime: totalTime,
					data: data
				};
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.OneTouch.Views.OneTouchTimerSelectView;
});