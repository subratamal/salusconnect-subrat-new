"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/modal/ModalCloseButton.view",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/mixins/mixin.registeredRegions",
	"consumer/schedules/views/modal/SingleControlModalWidget.view",
	"consumer/schedules/views/modal/IndividualControlCollection.view",
	"consumer/schedules/views/modal/SingleControlCollection.view"
], function (App, templates) {

	App.module("Consumer.Schedules.Views.Modal", function (Views, App, B, Mn, $, _) {
		Views.EditSingleControlModalView = Mn.LayoutView.extend({
			template: templates["schedules/modal/editSingleControlModal"],
			className: "edit-single-control-modal modal-body",
			ui: {
				alert: ".bb-alert",
				closeAlertButton: ".bb-close-button"
			},
			events: {
				"click @ui.closeAlertButton": "_toggleAlert"
			},
			regions: {
				closeButton: ".bb-btn-close",
				finishedButton: ".bb-btn-finished",
				addNewButton: ".bb-btn-add-new",
				singleControlsRegion: ".bb-single-controls-region",
				individualControlsRegion: ".bb-individual-controls-region"
			},
			initialize: function () {
				var that = this;
				_.bindAll(this, "handleFinishedClick", "handleAddNewSingleControl");
				this.showAlert = true;

				this.tscCollection = new B.Collection();
				this.tscCollection.set(App.salusConnector.getTSCCollection().models);

				var singleControlCollectionView = new App.Consumer.Schedules.Views.Modal.SingleControlCollection({
					collection: this.tscCollection
				});
				this.registerRegion("singleControlsRegion", singleControlCollectionView);

				this.registerRegion("closeButton",  new App.Consumer.Views.ModalCloseButton({
					id: "close-equip-btn",
					buttonTextKey: "common.labels.cancel",
					classes: "width100"
				}));

				this.registerRegion("finishedButton", new App.Consumer.Views.SalusButtonPrimaryView({
					id: "finished-equip-btn",
					buttonTextKey: "common.labels.finished",
					className: "btn btn-primary",
					classes: "width100",
					clickedDelegate: this.handleFinishedClick
				}));

				this.registerRegion("addNewButton", new App.Consumer.Views.SalusButtonPrimaryView({
					id: "add-new-equip-btn",
					buttonTextKey: "schedules.edit.modal.addNew",
					className: "btn btn-primary",
					classes: "width100",
					clickedDelegate: this.handleAddNewSingleControl
				}));

				this.registerRegion("individualControlsRegion", new App.Consumer.Schedules.Views.Modal.IndividualControlCollection({
					collection: App.salusConnector.getIndividualControlThermostats()
				}));

				this.listenTo(singleControlCollectionView, "tsc:post:success", function () {
					that.finishedButton.currentView.hideSpinner();
					App.hideModal();
					App.salusConnector.getTSCCollection().refresh().then(function () {
						App.Consumer.Schedules.PageEventController.trigger("tsc:get:success");
					});
				});

				this.listenTo(singleControlCollectionView, "tsc:post:fail", function () {
					that.finishedButton.currentView.hideSpinner();
					that._toggleAlert();
				});
			},
			handleAddNewSingleControl: function () {
				// There cannot be more single controls than there are thermostats
				if (this.tscCollection.length < App.salusConnector.getThermostatCollection().length) {
					this.tscCollection.push({
						isEdit: true,
						linkedDevices: {
							name: "",
							devices: []
						}
					});
				}
			},
			handleFinishedClick: function () {
				this.finishedButton.currentView.showSpinner();
				this.singleControlsRegion.currentView.trigger("save:tsc");
			},
			_toggleAlert: function () {
				this.showAlert = !this.showAlert;
				this.ui.alert.toggleClass("hidden", this.showAlert);
			}
		}).mixin([App.Consumer.Views.Mixins.RegisteredRegions, App.Consumer.Views.Mixins.SalusView]);
	});

	return App.Consumer.Schedules.Views.Modal.EditSingleControlModalView;
});