"use strict";

define([
    "app",
    "common/constants",
    "consumer/consumerTemplates",
    "consumer/views/mixins/mixin.salusView",
    "consumer/views/modal/ModalDefaultHeader.view",
    "consumer/views/SalusModal.view",
    "consumer/schedules/views/modal/EditSingleControlModal.view",
    "consumer/equipment/views/tsc/ThermostatSingleControlPage.view",
    "consumer/schedules/views/thermostat/ThermostatCollection.view",
    "consumer/schedules/views/ScheduleDaysCollection.view",
    "consumer/schedules/views/modal/duplicate/DuplicateScheduleModal.view",
    "consumer/schedules/views/ScheduleLayout.view",
    "consumer/schedules/views/ScheduleEmptyLayout.view"
], function (App, constants, consumerTemplates, SalusViewMixin) {

    App.module("Consumer.Schedules.Views", function (Views, App, B, Mn) {

        Views.ScheduleCollectionView = Mn.CollectionView.extend({
            childView: App.Consumer.Schedules.Views.ScheduleLayoutView,
            childViewOptions: function () {
                return this.options;
            },
            emptyViewOptions: function () {
                return {
                    deviceCategory: this.options.deviceCategory
                };
            },
            initialize: function () {
                this.type = this.options.type;
                this.key = this.options.key;
                this.deviceCategory = this.options.deviceCategory;
                this.singleControl = this.options.singleControl;

                this.collection = new B.Collection();
                this.listenTo(App.Consumer.Schedules.PageEventController, "singleControl:removed", this._setCollection);
                this.listenTo(App.Consumer.Schedules.PageEventController, "tsc:get:success", this._setCollection);
            },
            onRender: function () {
                this._setCollection();
            },
            //set the collection based on the deviceCategory and a distinct identifier (e.g. THERMOSTATS: singleControl, individualControl)
            _setCollection: function () {
                var that = this,
                        categories = constants.categoryTypes;

                App.salusConnector.getDataLoadPromise(["devices", "tscs"]).then(function () {
                    if (that.key) {
                        if (that.singleControl) {
                            that.collection.set(that.singleControl);
                        } else {
                            that.collection.set(App.salusConnector.getDeviceByKey(that.key));
                        }
                    } else {
                        switch (that.deviceCategory) {
                            case categories.THERMOSTATS:
                                var thermostatTypes = constants.scheduleTypes.thermostats;

                                if (that.type === thermostatTypes.singleControl) {
                                    that.collection.set(App.salusConnector.getTSCCollection().models);
                                } else if (that.type === thermostatTypes.individualControl) {
                                    that.collection.set(App.salusConnector.getIndividualControlThermostats().models);
                                }
                                break;
                            case categories.WATERHEATERS:
                                that.collection.set(App.salusConnector.getWaterHeaterCollection().models);
                                break;
                            case categories.SMARTPLUGS:
                                that.collection.set(App.salusConnector.getSmartPlugCollection().models);
                                break;
                            default:
                                throw new Error("Cannot retrieve invalid device type");
                        }
                    }
                });
            }
        }).mixin([SalusViewMixin]);
    });

    return App.Consumer.Schedules.Views.ScheduleCollectionView;
});
