"use strict";

define([
	"momentWrapper",
	"app",
	"common/config",
	"common/util/utilities",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/mixins/mixin.salusTabContent",
	"common/model/salusWebServices/NotificationCollection",
	"affix"
], function (moment, App, config, utilities, templates, SalusViewMixin, SalusTabViewMixin) {

	App.module("Consumer.Equipment.Views", function (Views, App, B, Mn /*, $*/) {
		Views.NotificationTableRow = Mn.ItemView.extend({
			className: "notification-row row",
			template: templates["equipment/messageCenter/notificationTableRow"],
			events: {
				"click .bb-ellipses": "_expandText"
			},
			bindings: {
				".bb-notif-title": "title",
				".bb-notif-text": {
					observe: ["text", "isExpanded"],
					onGet: function (values) {
						var text = values[0],
							isExpanded = values[1];

						if (isExpanded) {
							return text;
						}

						return isExpanded ? text : utilities.shortenText(text, config.notifications.shortenedTextMaxLength);
					},
					update: function ($el, val, model) {
						var appendStr = val;

						if (!model.get("isExpanded")) {
							appendStr += ' <span class="bb-ellipses">[...]</span>';
						}

						$el.html("");
						// TODO Add an open animation to this
						$el.append(appendStr);
					}
				},
				".bb-notif-date-time": {
					observe: "dateTime",
					onGet: function (value) {
						return moment(value).format("MMM DD  h:mm a");
					}
				}
			},
			_expandText: function () {
				this.model.set('isExpanded', true);
			}
		}).mixin([SalusViewMixin]);

		// TODO Date needs to be set from moment or i18next not a string model attribute
		Views.NotificationCompositeTable = Mn.CompositeView.extend({
			className: "row",
			template: templates["equipment/messageCenter/notificationCompositeTable"],
			childView: Views.NotificationTableRow,
			childViewContainer: ".bb-table-content-container",
			ui: {
				bbNotifMonthHeader: ".bb-notif-month-header"
			},
			initialize: function () {
				var initModel = this.model;

				this.collection = initModel.get('subCollection');
				this.model = initModel.get('displayModel');
			},
			onRender: function () {
				this.ui.bbNotifMonthHeader.append(App.translate("common.months." + this.model.get("month")));
			}
		}).mixin([SalusViewMixin]);


		Views.NotificationsTableView = Mn.CollectionView.extend({
			id: "bb-notifications-tab",
			childView: Views.NotificationCompositeTable,
			initialize: function () {
				this.collection = App.salusConnector.getNotificationCollection().toDisplayCollection(config.notifications.monthsDisplayed);
			}
		}).mixin([SalusTabViewMixin]);
	});

	return App.Consumer.Equipment.Views;
});