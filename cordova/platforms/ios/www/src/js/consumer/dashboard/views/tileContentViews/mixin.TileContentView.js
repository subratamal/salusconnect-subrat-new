"use strict";

define([
	"app",
	"underscore",
	"common/util/utilities",
	"common/config",
	"common/constants",
	"consumer/views/mixins/mixin.salusView"
], function (App, _, utilities, config, constants, SalusView) {

	App.module("Consumer.Dashboard.Views.TileContentViews", function (Views) {
		Views.TileContentViewMixin = function (options) {
			this.mixin([
				SalusView // by mixing in TileContentView we also mixin SalusView
			], options);

			this.addToObj({
				events: {
					"click .bb-pin-icon": "unpinFromDashboard"
				},
				bindings: {
					".bb-front-tile-title": {
						observe: "name",
						onGet: function (name) {
							return utilities.shortenText(name, config.tiles.frontTitleMaxLength);
						}
					},
					".bb-back-tile-title": {
						observe: "name",
						onGet: function (name) {
							return utilities.shortenText(name, config.tiles.backTitleMaxLength);
						}
					}
				}
			});

			this.setDefaults({
				unpinFromDashboard: function () {
					if (this.model.get("isInDashboard")) {
						var refType = "device";

						if (this.model.get("conditions")) { // chosen arbitrarily to identify rule model
							refType = "rule";
						}

						App.salusConnector.unpinFromDashboard(this.model, refType);
						this.trigger("unpinned");
					}
				},
				showSmallTileSpinner: utilities.noOp,
				//		function () {
				//	this.trigger("toggle:smallSpinner", true);
				//},

				hideSmallTileSpinner: utilities.noOp
				//		function () {
				//	this.trigger("toggle:smallSpinner", false);
				//}
			});

			this.before("initialize", function (options) {
				this.isLargeTile = !!options.isLargeTile;

				if (options.frontView) {
					this.frontView = options.frontView;
				}
				
				this.$el.addClass("tile-content-view");
				
				this.tileSpinnerOptions = {
					textKey: constants.spinnerTextKeys.processing,
					greyBg: true
				};

				_.bindAll(this, "unpinFromDashboard");
			});
		};
	});

	return App.Consumer.Dashboard.Views.TileContentViews.TileContentViewMixin;
});
