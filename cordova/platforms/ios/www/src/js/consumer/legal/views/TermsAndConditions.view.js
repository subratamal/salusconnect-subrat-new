"use strict";

define([
    "app",
    "consumer/consumerTemplates",
    "consumer/views/mixins/mixin.salusPage",
    "consumer/views/SalusButtonPrimary.view"
], function(App, consumerTemplates, SalusPage) {
    App.module("Consumer.Legal.Views", function(Views, App, B, Mn, $, _) {
        Views.TermsAndConditions = Mn.LayoutView.extend({
            template: consumerTemplates["legal/termsAndConditions"],
            id: "terms-conditions",
            className: "container terms-page",
            
            regions: {
                backButtonRegion: ".back-button-region"
            },
            initialize: function() {
                _.bindAll(this, "handleBackClick");
                
                this.backButtonView = new App.Consumer.Views.SalusButtonPrimaryView({
					classes: "btn",
					buttonTextKey: "common.labels.back",
					clickedDelegate: this.handleBackClick
				});
            },
            
            onRender: function() {
                var that = this; 
                that.backButtonRegion.show(that.backButtonView); 
            },
            
            handleBackClick: function() {
                window.history.back();
            }
            
        }).mixin([SalusPage]);
    });
    
    return App.Consumer.Legal.Views.TermsAndConditions;
});