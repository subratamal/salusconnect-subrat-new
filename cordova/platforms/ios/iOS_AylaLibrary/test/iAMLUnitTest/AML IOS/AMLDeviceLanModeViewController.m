
//
//  AMLDeviceLanModeViewController.m
//  AML IOS
//
//  Created by Yipei Wang on 3/21/13.
//  Copyright (c) 2013 Ayla Networks. All rights reserved.
//

#import "AMLDeviceLanModeViewController.h"
#import "AylaTest.h"
#import "AylaNetworks.h"
#import "AylaDiscovery.h"
#import "AylaSystemUtilsSupport.h"

@interface AMLDeviceLanModeViewController (){
    NSThread *testThread;
    dispatch_semaphore_t semaphore;
    
    //dispatch_semaphore_t mainThread_semaphore;
    int totalLanModeLoops;
    int currentLoopNum ;
    //BOOL threadWait;
   // int reachabilityFailureTimes;
    BOOL newStart ;
    
    AylaProperty *property1;
}
@end


@implementation AMLDeviceLanModeViewController
@synthesize textView = _textView;
@synthesize statusField = _statusField;
@synthesize button = _button;

static TestSequencer *testSequencer;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
 
    totalLanModeLoops = LAN_MODE_TEST_LOOPS;
    currentLoopNum = 0;
    newStart = true;
    //reachabilityFailureTimes = 0;
    /**
     * Should be first call when app is launched
     */
    [AylaSystemUtils loadSavedSettings];
    
    //xxxx
    [AylaSystemUtils serviceType:[NSNumber numberWithInteger:AML_DEVELOPMENT_SERVICE]];
    [AylaSystemUtils loggingLevel:AylaSystemLoggingAll];
    
    
    /**
     * Set lanModeState to Enabled if LAN MODE support is required
     */
    [AylaSystemUtils lanModeState: ENABLED];
    
    /**
     * This method needs to be called before any Lan Mode Enabled operations
     */
    [AylaLanMode enable];
    
    /**
     * For testing, no necessary
     */
    //[AylaCache clearAll];
    
    [self start];
    //semaphore = dispatch_semaphore_create(0);
    //mainThread_semaphore = dispatch_semaphore_create(0);
    //threadWait = true;
    //testThread = [[NSThread alloc] initWithTarget:self selector:@selector(threadStart:) object:nil];
    //[testThread start];	// Do any additional setup after loading the view.
}

- (IBAction)buttonPress:(id)sender {
    AylaDatapoint *dataPoint = [AylaDatapoint new];
    static int value = 0;
    value = 1-value;
    dataPoint.nValue = [NSNumber numberWithBool:value];
    
    /*
    [AylaReachability determineDeviceReachabilityWithBlock:^(int reachable) {
        if(reachable != AML_REACHABILITY_REACHABLE){
            NSLog(@"ERROR reachability %d", reachable);
        }
        
        [property1 createDatapoint:dataPoint success:^(AylaResponse *resp, AylaDatapoint *datapointCreated) {
            gblTestsPassed++;
            saveToLog(@"%@, %@, %@:%@, %@", @"P", @"LanMode", @"datapoint", @"created",  @"createDatapoint_boolean");
            [self pushToScreen: [NSString stringWithFormat:@"%@, %@, %@:%@, %@\n", @"P", @"LanMode", @"datapoint", @"created",  @"createDatapoint.boolean"]];
        } failure:^(AylaError *err) {
            saveToLog(@"%@, %@, %@:%d, %@", @"F", @"LanMode", @"errHttpStatusCode", err.httpStatusCode,  @"createDatapoint_boolean");
            [self pushToScreen: [NSString stringWithFormat:@"%@, %@, %@:%d, %@\n", @"F", @"LanMode", @"errHttpStatusCode", err.httpStatusCode,  @"createDatapoint_boolean"]];
        }];
    }];
     */
}

- (void)declareInitLanDeviceTests
{
    testSequencer =
    [TestSequencer testSequencerWithTestSuite:
     [NSArray arrayWithObjects:
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestLanUserLogin:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestLanDetermineServiceReachabilityWithBlock:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestLanGetDevices:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestLanDetermineServiceReachabilityWithBlock:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestLanDetermineDeviceReachabilityWithBlock:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self kickOffTestLoopThread:successBlock failure:failureBlock]; } copy],
      nil]];
}

- (void)declareLoopLanDeviceTests
{
    testSequencer =
    [TestSequencer testSequencerWithTestSuite:
     [NSArray arrayWithObjects:
      [^(void (^successBlock)(void), void (^failureBlock)(void)){
         gblAmlPropertyName = gblBooleanTypePropertyName1;
         [self amlTestLanGetProperties:successBlock failure:failureBlock];
       } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestLanDetermineDeviceReachabilityWithBlock:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestLanCreateBooleanDatapoint:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){
         gblAmlPropertyName = gblIntegerTypePropertyName1;
         [self amlTestLanGetProperties:successBlock failure:failureBlock];
       } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestLanCreateIntegerDatapoint:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){
         gblAmlPropertyName = gblStringTypePropertyName1;
         [self amlTestLanGetProperties:successBlock failure:failureBlock];
       } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestLanCreateStringDatapoint:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestGetDatapoints:successBlock failure:failureBlock]; } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){
         gblAmlPropertyName = gblDecimalTypePropertyName1;
         [self amlTestLanGetProperties:successBlock failure:failureBlock];
       } copy],
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestLanCreateDecimalDatapoint:successBlock failure:failureBlock]; } copy],
      /* TODO: test float type */
      nil]];
}

- (void)declareLoopLanFinalTests
{
    testSequencer =
    [TestSequencer testSequencerWithTestSuite:
     [NSArray arrayWithObjects:
      [^(void (^successBlock)(void), void (^failureBlock)(void)){ [self amlTestLanLogout:successBlock failure:failureBlock]; } copy],
      nil]];
}

-(void) start{
    
    [self declareInitLanDeviceTests];
    
    [self pushToScreen:@">>Start\n"];
    [testSequencer executeNextTest:^{} failure:^{[self setFailure];}];        // amlTestLanUserLogin
}

- (void)myRegisterReachabilityHandle
{
    /**
     * Lan Mode Support method. register your Rechabillity handler to library.
     * msg format: {  "device" : 1,
     "connectivity" : 1
     }
     *   Possbile values: AML_REACHABLE             0
     AML_UNREACHABLE          -1
     AML_LAN_MODE_DISABLED    -2
     AML_REACHABILITY_UNKNOWN -3
     */
    [AylaLanMode registerReachabilityHandle:^(NSDictionary *msg) {
        
        NSNumber *deviceReachability = [msg valueForKey:@"device"];
        NSNumber *serviceReachability = [msg valueForKey:@"connectivity"];
        saveToLog(@"%@, %@, %@:%@, %@:%@, %@", @"I", @"LanMode", @"devReachability", deviceReachability, @"serviceReachability", serviceReachability, @"ReachabilityHandle");
    }];
}

- (void)myRegisterNotifyHandle
{
    /**
     * Lan Mode Support method. register your Notify handler to library.
     * msg format: {  "statusCode" : 200,
     }
     * Possbile values: HTTP status code, 200 ~ 299 means Lan Mode works correctly with current LME device.
     When any property value is updated, this handler would also be called. So please call method "getProperties"
     to get latest property values when statusCode is returned as 200 ~ 299
     400 ~ 499 error happened during Lan Mode communication, A polling thread is required if app still wants to keep tra
     * Note: For testing, a sperate testing thread would be called here.
     */
    [AylaLanMode registerNotifyHandle:^(NSDictionary *msg) {
        int status =[[msg objectForKey:@"statusCode"] intValue];
        
        if(status >= 200 && status < 300  ){
            // [self getDeviceProperties];
            //dispatch_semaphore_signal(semaphore);

            if ([[msg objectForKey:@"type"] isEqualToString:@"session"]) {
                PFLogOnly(@"I", @"%@", @"session notification");
                if(newStart){
                    newStart = false;

                    [testSequencer executeNextTest:^{} failure:^{[self setFailure];}];     // amlTestLanDetermineDeviceReachabilityWithBlock
                    /*
                     [gblAmlTestDevice getProperties:nil success:^(AylaResponse *response, NSArray *properties) {
                     ;
                     } failure:^(AylaError *err) {
                     ;
                     }];
                     */
                }
                else{
                    
                    /*
                     NSArray *props = [[NSArray alloc] initWithObjects:@"Blue_LED", @"Green_LED", @"Blue_button", nil];
                     NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:props, @"names", nil];
                     [gblAmlTestDevice getProperties:params
                     success:^(NSArray *properties) {
                     saveToLog(@"%@, %@, %@:%d, %@", @"I", @"LanMode", @"count", [properties count],  @"getProperties");
                     } failure:^(aylaError *err) {
                     saveToLog(@"%@, %@, %@:%d, %@", @"E", @"LanMode", @"errHttpStatusCode", err->httpStatusCode,  @"getProperties");
                     }];
                     */
                }
            } else if([[msg objectForKey:@"type"] isEqualToString:@"property"]) {
                NSArray *props = [msg objectForKey:@"properties"];
                NSString *prop = [props objectAtIndex:0];
                
                [gblAmlTestDevice getProperties:@{@"names": @[prop]} success:^(AylaResponse *response, NSArray *properties) {
                    PFLogOnly(@"I", @"%@:%@", @"property update", [[properties objectAtIndex:0] name]);
                 } failure:^(AylaError *err) {
                    PFLog(@"E", @"%@, %@:%d, %@:%d", @"getProperties", @"errcode", err.errorCode , @"httpCode", err.httpStatusCode);
                 }];
            } else {
                PFLog(@"E", @"%@:%@", @"unexpected notify type", [msg objectForKey:@"type"]);
            }
        }
        else{
            PFLogOnly(@"E", @"%@:%d", @"statusCode", status);
        }
    }];
}

- (void)kickOffTestLoopThread:
/*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock
{
    //run a thread for testing. This only happens at first time this handler is called
    testThread = [[NSThread alloc] initWithTarget:self selector:@selector(threadStart:) object:nil];
    [testThread start];
}

- (void)amlTestLanUserLogin:
/*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock
{
    /**
     * Login to service
     */
    [AylaUser login:AML_TEST_USER_NAME password:AML_TEST_PASSWORD appId:AML_TEST_APP_ID appSecret:AML_TEST_APP_SECRET success:^(AylaResponse *resp, AylaUser *user){
        PFLog(@"P");
        
        [testSequencer executeNextTest:successBlock failure:failureBlock];
    } failure:^(AylaError *err) {
        PFLog(@"F", @"%@:%d, %@:%d", @"errcode", err.errorCode , @"httpCode", err.httpStatusCode);
        failureBlock();
    }];
}

- (void)amlTestLanGetDevices:
/*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock

{
    /**
     * Get user registered devices
     */
    [AylaDevice getDevices:nil success:^(AylaResponse *resp, NSArray *devices) {
        gblAmlTestDevice = nil;
        
        /**
         * Note this test only uses one device which matches @gblAmlProductName
         */
        if([devices count] >= 1){
            
            for(AylaDevice *tmp in devices){
                if([tmp.dsn isEqualToString:gblAmlProductName]){
                    gblAmlTestDevice = tmp;
                    break;
                }
            }
            
            if(gblAmlTestDevice != nil){
                PFLog(@"P", @"%@:%d", @"devCount", [devices count]);
            } else {
                PFLog(@"F", @"%@", @"Could not find device. Please check if gblAmlProductName is correct. No device assigned");
                failureBlock();
                return;
            }
        }
        else {
            PFLog(@"F", @"%@, %@", @"Please have at least 1 device to run test", @"Abort");
            failureBlock();
            return;
        }
        
        saveToLog(@"startLanModeEnableForDevice %@", gblAmlTestDevice.dsn);
        [self myRegisterReachabilityHandle];
        
        [self myRegisterNotifyHandle];
        
        /**
         * Enable Lan Mode communication to gblAmlTestDevice
         */
        [gblAmlTestDevice lanModeEnable];
        
    } failure:^(AylaError *err) {
        PFLog(@"F", @"%@:%d, %@:%d", @"errcode", err.errorCode , @"httpCode", err.httpStatusCode);
        failureBlock();
    }];
}

- (void)amlTestLanDetermineDeviceReachabilityWithBlock:
/*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock

{
    [AylaReachability determineDeviceReachabilityWithBlock:^(int reachablity) {
        if (reachablity == AML_REACHABILITY_REACHABLE) {
            PFLog(@"P");
            [testSequencer executeNextTest:successBlock failure:failureBlock];
        } else {
            PFLog(@"F", @"%@:%d", @"deviceReachability", reachablity);
            failureBlock();
        }
    }];
}

- (void)amlTestLanDetermineServiceReachabilityWithBlock:
/*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock

{
    [AylaReachability determineServiceReachabilityWithBlock:^(int reachablity) {
        if (reachablity == AML_REACHABILITY_REACHABLE) {
            PFLog(@"P");
            [testSequencer executeNextTest:successBlock failure:failureBlock];
        } else {
            PFLog(@"F", @"%@:%d", @"serviceReachability", reachablity);
            failureBlock();
        }
    }];
}

- (void)amlTestLanGetProperties:
/*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock

{
    NSArray *props = [[NSArray alloc] initWithObjects: gblAmlPropertyName,
                      @"log", @"decimal_out", @"output", nil];      // Adding the corresponding properties the module will send, so that the library does not log an Error (should be Warning)
    NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:props, @"names", nil];
    
    [gblAmlTestDevice getProperties:params
        success:^(AylaResponse *resp, NSArray *properties) {
            passNum++;
            gblAmlTestProperty = nil;
            for(AylaProperty *p in properties){
                if([p.name isEqualToString:gblAmlPropertyName]){
                    gblAmlTestProperty = p;
                }
            }
            
            if(gblAmlTestProperty!=nil){
                PFLog(@"P", @"%@:%@, %@:%@", @"property", [gblAmlTestProperty name], @"value", gblAmlTestProperty.datapoint.sValue);
                [testSequencer executeNextTest:successBlock failure:failureBlock];        // amlTestLanCreateDatapoint
            }
            else{
                PFLog(@"F", @"%@:%@, %@", gblAmlPropertyName, @"cannot find from property list",  @"getProperties - not assigned");
                failureBlock();
            }
            
        } failure:^(AylaError *err) {
            PFLog(@"F", @"%@:%d, %@", @"errHttpStatusCode", err.httpStatusCode,  @"getProperties");
            failureBlock();
        }];
}

- (void)amlTestLanCreateBooleanDatapoint:
    /*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock

{
    AylaDatapoint *dataPoint = [AylaDatapoint new];
    static int value = 0;
    value = 1-value;
    dataPoint.nValue = [NSNumber numberWithBool:value];
    
    [self amlTestLanCreateDatapoint: dataPoint success:successBlock failure:failureBlock];
}

- (void)amlTestLanCreateStringDatapoint:
/*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock

{
    AylaDatapoint *dataPoint = [AylaDatapoint new];
    gblTestString = @"Hello - ";
    
    srand(time(NULL));
    while (gblTestString.length < TEST_STRING_LEN) {
        char ch = (char) ((rand() % 95) + 32);
        NSString *chStr = [NSString stringWithFormat:@"%c" , ch];
        gblTestString = [gblTestString stringByAppendingString:chStr];
    }
    gblTestString = [gblTestString stringByReplacingOccurrencesOfString:@"%" withString: @"%%"];     // Escape % so it is not interpreted as a format character when printed
    
    dataPoint.sValue = gblTestString;
    
    [self amlTestLanCreateDatapoint: dataPoint success:successBlock failure:failureBlock];
}

- (void)amlTestLanCreateIntegerDatapoint:
/*success:*/(void (^)(void))successBlock
                                failure:(void (^)(void))failureBlock

{
    AylaDatapoint *dataPoint = [AylaDatapoint new];
    static int value = 333;
    dataPoint.nValue = [NSNumber numberWithInt:value++];
    
    [self amlTestLanCreateDatapoint: dataPoint success:successBlock failure:failureBlock];
}

- (void)amlTestLanCreateDecimalDatapoint:
/*success:*/(void (^)(void))successBlock
                               failure:(void (^)(void))failureBlock

{
    AylaDatapoint *dataPoint = [AylaDatapoint new];
    static double fvalue = 12345.67;
    fvalue++;
    dataPoint.nValue = [NSNumber numberWithFloat:fvalue];
    
    [self amlTestLanCreateDatapoint: dataPoint success:successBlock failure:failureBlock];
}

- (void)amlTestLanCreateFloatDatapoint:
/*success:*/(void (^)(void))successBlock
                                 failure:(void (^)(void))failureBlock

{
    AylaDatapoint *dataPoint = [AylaDatapoint new];
    static double fvalue = 88888.89;
    fvalue++;
    dataPoint.nValue = [NSNumber numberWithFloat:fvalue];
    
    [self amlTestLanCreateDatapoint: dataPoint success:successBlock failure:failureBlock];
}

- (void)amlTestLanCreateDatapoint:
    (AylaDatapoint *)dataPoint
    success:(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock

{
    datapointTried++;
    [gblAmlTestProperty createDatapoint:dataPoint success:^(AylaResponse *resp, AylaDatapoint *datapointCreated) {
        PFLog(@"P", @"%@", gblAmlPropertyName);
        succeededDatapointCounter++;
        [testSequencer executeNextTest:successBlock failure:failureBlock];
    } failure:^(AylaError *err) {
        PFLog(@"F", @"%@:%d, %@", @"errHttpStatusCode", err.httpStatusCode, gblAmlPropertyName);
        failureBlock();
    }];
}

- (void)amlTestGetDatapoints:
/*success:*/(void (^)(void))successBlock
                     failure:(void (^)(void))failureBlock
{
    NSMutableDictionary *callParams = [NSMutableDictionary new];
    [callParams setObject:[NSNumber numberWithInt:1] forKey:@"count"];
    
    /**
     * Test to get Datapoints by activity
     */
    [gblAmlTestProperty getDatapointsByActivity:callParams
                                        success:^(AylaResponse *resp, NSArray *datapoints)
     {
         int i;
         NSString *baseType = gblAmlTestProperty.baseType;
         NSString *valueStr;
         
         // Check to see if the last string created matches the one we started with
         if ((datapoints.count > 0) && ([gblAmlTestProperty.baseType isEqualToString:@"string"])) {
             AylaDatapoint *datapoint = [datapoints objectAtIndex:datapoints.count - 1];
             if ([datapoint.sValue isEqualToString:gblTestString]) {
                 PFLog(@"P");
             } else {
                 PFLog(@"F", @"%@!=%@", datapoint.sValue, gblTestString);
             }
         } else {
             PFLog(@"P");
         }
         [testSequencer executeNextTest:successBlock failure:failureBlock];
     }
     failure:^(AylaError *err)
     {
         NSError * nsError = err.nativeErrorInfo;
         NSString *description = [AylaSystemUtils shortErrorFromError:nsError];
         
         PFLog(@"F", @"%@:%@", @"Description:", description);
         // Execute next test, even on failure
         [testSequencer executeNextTest:successBlock failure:failureBlock];
     }];
}

- (void)amlTestLanLogout:
/*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock

{
    [AylaUser logoutWithParams:@{kAylaUserLogoutClearCache: @(YES)} success:^(AylaResponse *response) {
        PFLog(@"P");
        [testSequencer executeNextTest:successBlock failure:failureBlock];
    } failure:^(AylaError *err) {
        PFLog(@"F", @"%@:%@", @"logout", @"success");
        failureBlock();
    }];
}

- (void)tempThreadTest
{
   
}
- (IBAction)oneTimeRunTest:(id)sender {
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSArray *props = [[NSArray alloc] initWithObjects:@"log", nil];
        NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:props, @"names", nil];
        [gblAmlTestDevice getProperties:params success:^(AylaResponse *response, NSArray *properties) {
            NSLog(@"DONE");
        } failure:^(AylaError *err) {
            ;
        }];
    });
}

static BOOL isReady = true;
static int datapointTried = 0;
static int succeededDatapointCounter = 0;
static int passNum = 0;
static int failTimes = 0;
static int times = 0; // Loop times

- (void)threadStart:(id)params
{
    int threadTestLoops = LAN_MODE_PROPERTY_DATAPOINT_LOOPS;
    gblAmlTestProperty = nil;
    times  = 0;
    
    // main loop
    while(times< threadTestLoops){

        passNum = 0;
        sleep(2);
        
        if(isReady && times< threadTestLoops){
            [self declareLoopLanDeviceTests];
            
            /**
             * Included APIs in one loop:
             * [getProperties] [createDatapoint]
             */
            isReady = false;
            [testSequencer executeNextTest:^{ isReady = true; times++; }
                                   failure:^{ isReady = true; times++; failTimes++; }];        // amlTestLanGetProperties

        }
    }

    double delayInSeconds = 4.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSLog(@"failTimes: %d", failTimes);
        NSLog(@"TotalTimes: %d", gblNumberOfTests);
        NSLog(@"TotalPassTimes %d", gblTestsPassed);
        NSLog(@"Iterations: %d", currentLoopNum + 1);
        NSLog(@"DatapointTried: %d", datapointTried);
        NSLog(@"Datapoint succeeded: %d",succeededDatapointCounter);
        [self lanModeDisableTest];;
    });
    
}



- (void)lanModeDisableTest
{    
    /**
     * Call method lanModeDisable to stop LAN Mode connection to device
     */
    [gblAmlTestDevice lanModeDisable];
    
    /**
     * Call stop [AylaLanMode disble] to stop LAN mode service
     */
    [AylaLanMode disable];
    
    double delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    if(++currentLoopNum<totalLanModeLoops)
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            newStart = true;
            [AylaLanMode enable];
            [self start];
        });
    else{
        [self declareLoopLanFinalTests];
        [testSequencer executeNextTest:^{[self setSuccess];} failure:^{[self setFailure];}];        // amlTestLanLogout
    }
}



- (void)pushToLogAndScreen:(NSString *)msg
{
    [_textView setText:[NSString stringWithFormat:@"%@%@", _textView.text,msg]];
    saveToLog(@"%@", msg);
}

- (void)pushToScreen:(NSString *)msg
{
    [_textView setText:[NSString stringWithFormat:@"%@%@", _textView.text,msg]];
}

- (void)setSuccess
{
    [self complete];
    if([_statusField.text compare:@"working"]==0)
        [_statusField setText:@"success"];
    [_statusField setEnabled:true];
}

- (void)setFailure
{
    [self abortMsg];
    if([_statusField.text compare:@"working"]==0)
        [_statusField setText:@"failure"];
    [_statusField setEnabled:true];
}

- (void)complete
{    
    NSString *aMsg = [NSString stringWithFormat:@"%@, %@, %@:%d, %@:%d, %@:%d\n", @"Completed", @"amlLanModeDeviceTest", @"TotalTestsExecuted&Running", gblNumberOfTests, @"Passed", gblTestsPassed, @"Failed", gblNumberOfTests-gblTestsPassed, nil];
    saveToLog(@"%@, %@, %@:%d, %@:%d, %@:%d", @"Completed", @"amlLanModeDeviceTest", @"TotalTestsExecutedOrRunning", gblNumberOfTests, @"Passed", gblTestsPassed, @"Failed", gblTestsFailed);
    [_textView setText:[NSString stringWithFormat:@"%@%@", _textView.text,aMsg]];
    _statusField.text = @"success";
    
    if(runAllInALoop){
        outputMsgs = [NSString stringWithFormat:@"%@\n%@", outputMsgs, _textView.text];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
}

- (void)abortMsg
{    
    NSString *aMsg = [NSString stringWithFormat:@"%@, %@, %@:%d, %@:%d, %@:%d, %@\n", @"F", @"amlLanModeDeviceTest", @"TotalTestsExecuted&Running", gblNumberOfTests, @"Failed", gblTestsFailed, @"Passed", gblTestsPassed, @"Abort", nil];
    saveToLog(@"%@, %@, %@:%d, %@:%d, %@:%d, %@\n", @"F", @"amlLanModeDeviceTest", @"TotalTestsExecutedOrRunning", gblNumberOfTests, @"Failed", gblTestsFailed, @"Passed", gblTestsPassed, @"Abort");
    [_textView setText:[NSString stringWithFormat:@"%@%@", _textView.text,aMsg]];
    _statusField.text= @"failure";
    if(runAllInALoop){
        outputMsgs = [NSString stringWithFormat:@"%@\n%@", outputMsgs, _textView.text];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    if(gblAmlTestDevice!=nil)
        [gblAmlTestDevice lanModeDisable];
    [AylaLanMode disable];
    
    if([self.navigationController.viewControllers indexOfObject:self] == NSNotFound){
        runAllInALoop = NO;
    }
    [super viewWillDisappear:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
