//
//  AMLDeviceLanModeViewController.h
//  AML IOS
//
//  Created by Yipei Wang on 3/21/13.
//  Copyright (c) 2013 Ayla Networks. All rights reserved.
//

#import <UIKit/UIKit.h>
#define LAN_MODE_PROPERTY_DATAPOINT_LOOPS 1  //How many times createDatapoints would be called in a loop

@interface AMLDeviceLanModeViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UITextField *statusField;
@property (weak, nonatomic) IBOutlet UIButton *button;

@end


