//
//  AylaGrantSupport.h
//  iOS_AylaLibrary
//
//  Created by Yipei Wang on 6/20/14.
//  Copyright (c) 2015 Ayla Networks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AylaGrant(Support)

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
