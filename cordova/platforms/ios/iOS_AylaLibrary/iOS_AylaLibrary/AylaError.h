//
//  AylaError.h
//  Ayla Mobile Library
//
//  Created by Yipei Wang on 8/9/13.
//  Copyright (c) 2015 Ayla Networks. All rights reserved.
//

typedef enum {
    AylaErrorParameterErrorCode = 1000,
    AylaErrorParameterNativeErrorInfo,
    AylaErrorParameterErrorInfo
} AylaErrorParameter;

#import <Foundation/Foundation.h>
#import "AylaResponse.h"
@interface AylaError :AylaResponse<NSCopying>
@property (nonatomic, readonly) id errorInfo;
@property (nonatomic, readonly) id nativeErrorInfo; //Note: nativeErrorInfo: NSError on iOS, {API name & error code} on Android
@property (nonatomic, readonly) NSInteger errorCode;

- (id)initWithAylaResponse:(AylaResponse *)reponse;

@end
