//
//  iOS_AylaLibraryTests.m
//  iOS_AylaLibraryTests
//
//  Created by Yipei Wang on 9/20/13.
//  Copyright (c) 2015 Ayla Networks. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface iOS_AylaLibraryTests : XCTestCase

@end

@implementation iOS_AylaLibraryTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
