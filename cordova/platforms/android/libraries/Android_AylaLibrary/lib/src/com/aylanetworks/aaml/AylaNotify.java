//
//  AylaNotify.java
//  Ayla Mobile Library
//
//  Created by Daniel Myers on 11/25/12.
//  Copyright (c) 2012 Ayla Networks. All rights reserved.
//

package com.aylanetworks.aaml;

import com.google.gson.annotations.Expose;

import android.os.Handler;

public class AylaNotify {

	@Expose
	public String type; 	// Notification is about a "session" or a "property" change
	@Expose
	public String dsn; 		// The Device Serial Number of the device the property change notification references
	@Expose
	public String description; // The response description for a property change notification
	@Expose
	public String names[]; 	// Array of property names for a property change notification
	
	static public Handler notifierHandle = null;
	static AylaRestService rsNotifier = null;

	static Boolean notifyOutstanding = false;
	
	public static Boolean getNotifyOutstanding () {
		return notifyOutstanding;
	}
	static void setNotifyOutstanding(Boolean flag) {
		notifyOutstanding = flag;
	}
	
	/**
	 * Reset the notifyOutstanding flag so next getProperties will access the service or device
	 */
	public static void notifyAcknowledge() {
		notifyOutstanding = false;
	}

	/**
	 * register callback handle from main activity
	 */
	public static void register(Handler handle) {
		if (handle != null) {
			notifierHandle = handle;
			rsNotifier = new AylaRestService(notifierHandle, "notify_register", AylaRestService.PROPERTY_CHANGE_NOTIFIER);
			notifyOutstanding = false;
		}
	}

	//TODO: Put it in a common utils class 
	public static void returnToMainActivity(AylaRestService rs, String thisJsonResults, int thisResponseCode, int thisSubTaskId, Boolean fromDevice) {
		if (notifierHandle == null) {
			return;
		}
		if (rs == null) {
			rs = rsNotifier;
		}

		rs.jsonResults = thisJsonResults;
		rs.responseCode = thisResponseCode;
		rs.subTaskFailed = thisSubTaskId;
		if (fromDevice) { 
			notifyOutstanding = true;
		}
		
		rs.execute(); // call notifier handler in main activity
		return;
	}

}
