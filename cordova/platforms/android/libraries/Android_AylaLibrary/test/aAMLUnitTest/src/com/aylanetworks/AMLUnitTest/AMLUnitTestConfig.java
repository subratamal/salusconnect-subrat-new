/*
 * AMLUnitTestConfig.java
 * Ayla Mobile Library
 *
 * Created by Di Wang on 05/04/2015.
 * Copyright (c) 2015 Ayla Networks Inc. All rights reserved.
 * */

package com.aylanetworks.AMLUnitTest;

public class AMLUnitTestConfig {

	//------------------- UNIT TEST CONFIGURATION ------------------------------
		// Use your developer account email address and password from https://developer.aylanetworks.com
		public static final String userName = "me@company.com";
		public static final String password = "myPassword";

		// Replace the last couple of digits with the serial number of your EVB.
		public static final String gblAmlTestEVBDsn = "AC000W000XXXXXX";


		// Replace the last couple of digits with the serial number of your Gateway.
		public static final String gblAmlTestGatewayDsn = "AC000W000XXXXXX";
		// Replace the final digits with the serial number of your node dsn
		public static final String gblAmlTestSmartPlugNodeDsn = "VR00ZN000XXXXXX";			// Smart plug
		public static final String gblAmlTestLightSocketNodeDsn = "VR00ZN000XXXXXX";		// Light socket
		public static final String gblAmlTestRemoteControlNodeDsn = "VR00ZN000XXXXXX";		// Remote control

		// Replace the last couple of digits with the serial number of your generic gateway.
		public static final String gblAmlTestGenericGatewayDsn = "AC000W000XXXXXX";
		// Replace the final digits with the serial number of your generic node dsn
		public static final String gblAmlTestGenericNodeDsn = "VR00ZN000XXXXXX";
		public static final String gblAmlTestGenericNode2Dsn = "VR00ZN000XXXXXX";
		public static final String gblAmlTestGenericNode3Dsn = "VR00ZN000XXXXXX";


		// Replace with regular expression string matching your company's product
		public static final String gblAmlDeviceSsidRegex = "^Ayla-[0-9A-Fa-f]{12}"; // Regex string for matching Ayla EVB/Demo Kit SSID

		// Replace with the EVB/Demo Kit SSID used for Setup testing
		public static final String gblAmlModuleSsid = "Ayla-cc52afXXXXXX"; // Used for Setup testing

	    // Replace with your WLAN AP SSID providing internet access
		public static final String gblAmlLanSsid = "aSSID"; // The SSID of WLAN AP with internet access
		public static final String gblAmlLanSsidPassword = "ssidPassword"; // The password of the WLAN AP with internet access

		// Replace these with the application ID and secret issued to your company by Ayla Networks
		public static final String appId = "myApp-id";				// Ayla Control appId = "aMCA-id"
		public static final String appSecret = "myApp-secret";		// Ayla Control appSecret = "aMCA-9097620"

		// Select which test suite to use
		public static final testSuite amlTestSuite
//			= testSuite.GATEWAY_SERVICE_ONLY;
//			= testSuite.GATEWAY_LAN_MODE_ONLY;
//			= testSuite.DEVICE_SERVICE_ONLY;
//			= testSuite.LAN_MODE_ONLY;
//			= testSuite.SETUP_ONLY;
			= testSuite.GENERIC_GATEWAY_SERVICE_ONLY;
//			= testSuite.GENERIC_GATEWAY_LAN_MODE_ONLY;

		public enum testSuite {
			SETUP_ONLY
			, SETUP_AND_REGISTRATION_ONLY
			, DEVICE_SERVICE_ONLY
			, GATEWAY_SERVICE_ONLY
			, SETUP_REGISTRATION_AND_DEVICE_SERVICE
			, LAN_MODE_ONLY
			, GATEWAY_LAN_MODE_ONLY
			, GENERIC_GATEWAY_SERVICE_ONLY
			, GENERIC_GATEWAY_LAN_MODE_ONLY // Not applicable for now.
		}

		// This maps to modules like AMLBlobUnitTest, AMLContactsUnitTest.  NOT_APPLICABLE means to check test suite.
		public static final testModule amlTestModule
//			= testModule.AML_BLOB;
			  = testModule.NOT_APPLICABLE;
//			= testModule.AML_CONTACT;
		public enum testModule {
			NOT_APPLICABLE
			, AML_BLOB
			, AML_CONTACT
		}

	    // Select how to start: tap on the screen or start automatically on launch
		public static final runMode amlRunMode = runMode.TAP_HERE_TO_BEGIN;
		public enum runMode {
			AUTO
			, TAP_HERE_TO_BEGIN
		}

		// Replace with values used by triggers & apps
		public static final String gblAmlCountryCode = "1";
		public static final String gblAmlPhoneNumber = "8005551212"; // sms trigger alerts are sent here
		public static final String gblAmlEmailAddress = userName; 	 // email trigger alerts are sent here

		public static String gblAmlContactID = null;

		// Set this to your push notification server, if not using Ayla's
		public static final String senderId = "103052998040";

		// Schedules - Ensure the device is using host template version "demo_dp 1.0",and the service is using the "Ayla_LED_EVB demo_schedules" template
		public static String gblScheduleName = null; // evb:"schedule_in", smart plug:"sched1", or null to skip


		// Share a named resource.
		// Share testing skipped unless default email address are changed
		public static final String shareWithEmailAddress = 	 	 "shareWith@email.com"; 			// share resource with this registered user, default:shareWith@email.com
		public static final String shareReceiveFromEmailAddress = "shareReceiveFrom@email.com";  	// resource shared (received from) this registered user, default:shareReceivedFrp,@email.com
		public static final String shareReceiveFromDSN = "AC000W0000XXXXX";  							// resource shared (received from) DSN, optional, used for filtering by resourceId

		public static final String resendConfirmationEmailAddress = "resendConfirmationEmail.com";		// Replace with a signed up, but not confirmed e-mail address
		public static final String resetPasswordEmailAddress = "resetPasswordEmail.com";				// Replace with an e-mail address used to test reset password

		// Controls whether the asynchronous or synchronous versions of amlTestUserLogin2(), amlTestCreateDatapoint(), and amlTestGetProperties() tests will be run
		public static final boolean gblAmlAsyncMode = true;

		// Number of test suite iterations to run
		public static final int testIterations = 1;
		//------------ END UNIT TEST CONFIGURATION ------------------------------



}// end of AMLUnitTestConfig class
