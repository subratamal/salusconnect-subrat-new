/*
 * AMLGenericGatewayUnitTestCaseServiceOnly.java
 * Ayla Mobile Library
 * 
 * Created by Di Wang on 07/08/2015.
 * Copyright (c) 2015 Ayla Networks Inc. All rights reserved.
 * */


package com.aylanetworks.AMLUnitTest.gatewayTest;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.annotation.SuppressLint;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;

import com.aylanetworks.AMLUnitTest.AMLUnitTestConfig;
import com.aylanetworks.AMLUnitTest.PushNotification;
import com.aylanetworks.AMLUnitTest.facility.Test;
import com.aylanetworks.AMLUnitTest.facility.TestSequencer;
import com.aylanetworks.AMLUnitTest.interfaces.DefaultScreen;
import com.aylanetworks.AMLUnitTest.interfaces.ITestResultDisplayable;
import com.aylanetworks.AMLUnitTest.interfaces.IUnitTestable;
import com.aylanetworks.aaml.AylaAppNotification;
import com.aylanetworks.aaml.AylaDatapoint;
import com.aylanetworks.aaml.AylaDevice;
import com.aylanetworks.aaml.AylaDeviceGateway;
import com.aylanetworks.aaml.AylaDeviceNode;
import com.aylanetworks.aaml.AylaDeviceNotification;
import com.aylanetworks.aaml.AylaHost;
import com.aylanetworks.aaml.AylaHostNetworkConnection;
import com.aylanetworks.aaml.AylaLanMode;
import com.aylanetworks.aaml.AylaNetworks;
import com.aylanetworks.aaml.AylaNotify;
import com.aylanetworks.aaml.AylaProperty;
import com.aylanetworks.aaml.AylaReachability;
import com.aylanetworks.aaml.AylaRestService;
import com.aylanetworks.aaml.AylaSystemUtils;
import com.aylanetworks.aaml.AylaUser;
import com.aylanetworks.aaml.AylaNetworks.lanMode;



@SuppressLint({ "HandlerLeak", "DefaultLocale" })
public class AMLGenericGatewayUnitTestCaseServiceOnly 
implements IUnitTestable
{

	private static final String tag 
		= AMLGenericGatewayUnitTestCaseServiceOnly .class.getSimpleName();
	
	private ITestResultDisplayable mScreen = null;
	private TestSequencer testSequencer = null;
	private Runnable mEndHandler = null;
	
	

	Message msg = new Message();
	AylaUser gblAmlTestUser = null;
	AylaDevice gblAmlTestDevice = null;
	AylaDeviceGateway gblAmlTestGateway = null;
	
	
	AylaDeviceNode gblAmlTestGenericNode = null;
	AylaDeviceNode gblAmlTestGenericNode2 = null;
	AylaDeviceNode gblAmlTestGenericNode3 = null;
	
	AylaRestService restService4 = null;
	
	private AylaProperty gblAmlTestGenericOnCmdProperty = null;
	private AylaProperty gblAmlTestGenericOffCmdProperty = null;
	private AylaProperty gblAmlTestGenericOnOffStatusProperty = null;
	
	private final String gblAmlTestOnCmdPropertyName 
//		= "On_Cmd";
		= "01:0006_S:01";
	private final String gblAmlTestOffCmdPropertyName 
//		= "Off_Cmd";
		= "01:0006_S:00";
	private final String gblAmlTestOnOffStatusPropertyName 
//		= "OnOff_Status";
		= "01:0006_S:0000";
	
	AylaDeviceNotification gblAmlTestDeviceNotification = null;
	AylaAppNotification gblAmlTestAppNotification = null;
	
//	int gblAmlTestSmartPlugNodePropertyValue = 0;			// Smart plug "in" property value
//	int gblAmlTestLightSocketNodePropertyValue = 0;			// Light socket "in" property value
	
	private int gblAmlTestOnCmdPropertyValue = 0;
	private int gblAmlTestOffCmdPropertyValue = 0;
	
	boolean delayExecution = false;
	boolean isWifi = false;
	boolean isGateway = false;
	boolean isNode = false;
	boolean gblAmlTestLanModeEnabled = false;

	
	

	
	public AMLGenericGatewayUnitTestCaseServiceOnly() {
		mScreen = new DefaultScreen();
	}
	
	public AMLGenericGatewayUnitTestCaseServiceOnly(final ITestResultDisplayable screen) {
		mScreen = screen;
	}
	
	@Override
	public void onResume() {
		if (mScreen == null) {
			mScreen = new DefaultScreen();
		}
		mScreen.init();
		
		initTestSuite();
		
		AylaSystemUtils.lanModeState = lanMode.DISABLED; // disable lan mode, only access cloud service
		
//		AylaNetworks.onResume();
	}// end of setup

	
	@Override
	public void start() {
		if (testSequencer == null) {
			onResume();
		}
		testSequencer.nextTest().execute();
	}// end of start           
	

	@Override
	public void onPause() {
		if (testSequencer == null) {
			onResume();
		}
		
		AylaNetworks.onPause(false);
	}// end of tearDown     
	
	
	@Override
	public void onDestroy() {
		AylaNetworks.onDestroy();
		
//		gblAmlTestZigbeeGateway = null;
		//TODO: GC work done here.
		testSequencer = null;
	}// end of onDestroy    

	
	@Override
	public void onEnd() {
		if (mEndHandler != null) {
			mEndHandler.run();
		}
	}


	@Override
	public void setEndHandler(Runnable r) {
		mEndHandler = r;
	}
	
	
	@Override
	public void setResultScreen(ITestResultDisplayable screen) {
		if (screen == null) {
			mScreen = new DefaultScreen();
		} else {
			mScreen = screen;
		}
	}// end of setResultScreen           
	
	
	@Override
	public boolean isRunning() {
		if (testSequencer != null) {
			return testSequencer.isTestSuiteRunning();
		}
		return false;
	}
	
	
	private void initTestSuite() {
		List<Test> zigbeeServiceTestList = new ArrayList<Test>(settingsTestList);
		zigbeeServiceTestList.addAll(gatewayServiceStartTestList);
		zigbeeServiceTestList.addAll(endList);
		
		testSequencer = new TestSequencer(zigbeeServiceTestList);
	}// end of initTEstSuite    
	
	private List<Test> endList = new ArrayList<Test>(Arrays.asList(
			new Test() { public void execute() { onEnd(); } }
			));
	
	private List<Test> settingsTestList = new ArrayList<Test>(Arrays.asList(
			new Test() { public void execute() { amlTestNothing(); }
	}));
	
	
	private List<Test> gatewayServiceStartTestList = new ArrayList<Test>(Arrays.asList(
			new Test() { public void execute() { amlTestReturnHostWiFiState2(); } },
			new Test() { public void execute() { amlTestUserLogin2(); } },
			// gateways
			new Test() { public void execute() { amlTestGetDevices(); } },
//			new Test() { public void execute() { amlTestOpenGatewayJoinWindow(); } },
//			new Test() { public void execute() { amlTestGetGatewayCandidates(); } },
//			new Test() { public void execute() { amlTestRegisterGatewayCandidate(); } },
			new Test() { public void execute() { amlTestGetGatewayNodes(); } },
			
			new Test() { public void execute() { amlTestGetGenericNodeProperties(); } },
//			new Test() { public void execute() { amlTestGetLightSocketProperties(); } },
//			new Test() { public void execute() { amlTestGetRemoteControlProperties(); } },
//			new Test() { public void execute() { amlTestIdentify(); } },
			
			new Test() { public void execute() { amlTestGenericNodeDatapoint(true); } },
//			new Test() { public void execute() { amlTestGetSmartPlugDatapointsByActivity(1); } }
			
			new Test() { public void execute() { amlTestGenericNodeDatapoint(false); } }
//			, new Test() { public void execute() { amlTestDeleteAllGroups(); } },
//			new Test() { public void execute() { amlTestCreateGroup(); } },	
//			new Test() { public void execute() { amlTestUpdateGroup(); } },	
//			new Test() { public void execute() { amlTestGetGroups(); } },
//			new Test() { public void execute() { amlTestGetGroup(); } },
//			new Test() { public void execute() { amlTestDeleteAllScenes(); } },
//			new Test() { public void execute() { amlTestDeleteAllScenes(); } },
//			new Test() { public void execute() { amlTestCreateNodeScene(); } },	
//			new Test() { public void execute() { amlTestUpdateNodeScene(); } },	
//			new Test() { public void execute() { amlTestGetScenes(); } },
//			new Test() { public void execute() { amlTestRecallNodeScene(); } },
//			new Test() { public void execute() { amlTestDeleteNodeScene(); } },
//			new Test() { public void execute() { amlTestDeleteNodeScene(); } },
//			new Test() { public void execute() { amlTestCreateGroupScene(); } },	
//			new Test() { public void execute() { amlTestGetScene(); } },
//			new Test() { public void execute() { amlTestRecallGroupScene(); } },
//			new Test() { public void execute() { amlTestDeleteGroupScene(); } },
//			new Test() { public void execute() { amlTestDeleteGroupScene(); } },
//
//			new Test() { public void execute() { amlTestDeleteAllBindings(); } },
//			new Test() { public void execute() { amlTestCreateBindingToProperty(); } },
//			new Test() { public void execute() { amlTestUpdateBinding(); } },
//			new Test() { public void execute() { amlTestGetBinding(); } },
//			new Test() { public void execute() { amlTestDeletePropertyBinding(); } },
//			new Test() { public void execute() { amlTestCreateBindingToGroup(); } },
//			new Test() { public void execute() { amlTestGetBindings(); } },
//			new Test() { public void execute() { amlTestDeleteGroupBinding(); } },			
//			
//			new Test() { public void execute() { amlTestDeleteGroup(); } }			// Used by binding and scene tests
//
//			, new Test() { public void execute() { amlTestCreateDeviceNotification(); } },
//			new Test() { public void execute() { amlTestUpdateDeviceNotification(); } },
//			new Test() { public void execute() { amlTestGetDeviceNotifications(); } },
//			new Test() { public void execute() { amlTestCreateAppNotification(); } },
//			new Test() { public void execute() { amlTestUpdateAppNotification(); } },
//			new Test() { public void execute() { amlTestGetAppNotifications(); } },
//			new Test() { public void execute() { amlTestDeleteAppNotification(); } },
//			new Test() { public void execute() { amlTestDeleteDeviceNotification(); } }
			));
	
	
	// FIX: Temporarily take out settings tests, since they interfere with
		// cached values (BUG AML-40)
		private void amlTestNothing() {
			String passMsg = String.format("%s, %s", "P", "TestNothing");
			amlTestStatusMsg("Pass", "TestNothing", passMsg);
			testSequencer.nextTest().execute();
		}
	
	
	/**
	 * Get WiFi state from host
	 */
	private void amlTestReturnHostWiFiState2() {
		
		if (AMLUnitTestConfig.amlTestSuite == AMLUnitTestConfig.testSuite.LAN_MODE_ONLY 
				|| AMLUnitTestConfig.amlTestSuite == AMLUnitTestConfig.testSuite.GATEWAY_LAN_MODE_ONLY) {
			
			if ( AylaReachability.isCloudServiceAvailable()) {
				if (AylaLanMode.lanModeState != lanMode.RUNNING) {
					AylaLanMode.resume();	// start the Lan Mode service. Only required for iterative testing
				}
				AylaHost.returnHostWiFiState(returnHostWiFiState2);
			} else {
				String failMsg = String.format("%s, %s, %s:%s, %s", "F", "amlTest", "device", "not reachable", "returnHostWiFiState2");
				amlTestStatusMsg("Fail", "returnHostWiFiState2.reachability", failMsg);	
			}
		} else {
			AylaHost.returnHostWiFiState(returnHostWiFiState2);
		}
	}// end of amlTestReturnHostWiFiState2
	
	private final Handler returnHostWiFiState2 = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaHostNetworkConnection state = AylaSystemUtils.gson.fromJson(jsonResults,  AylaHostNetworkConnection.class);
				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "state", state.state, "returnHostWiFiState2");
				amlTestStatusMsg("Pass", "returnHostWiFiState2", passMsg);	

				testSequencer.nextTest().execute();
//				amlTestUserLogin2();
			} else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "");
				amlTestStatusMsg("Fail", "returnHostWiFiState", errMsg);
			}
		}		
	};
	
	
	private void amlTestUserLogin2() {
		if (AMLUnitTestConfig.gblAmlAsyncMode) {
			AylaUser.login(login2, 
					AMLUnitTestConfig.userName, 
					AMLUnitTestConfig.password, 
					AMLUnitTestConfig.appId, 
					AMLUnitTestConfig.appSecret);
		} else {
			// sync
			msg.arg1 = AylaNetworks.AML_ERROR_FAIL; // assume failure;

			Thread thread = new Thread(new Runnable() {
				public void run()
				{
					AylaRestService rs = AylaUser.login(
							AMLUnitTestConfig.userName, 
							AMLUnitTestConfig.password, 
							AMLUnitTestConfig.appId, 
							AMLUnitTestConfig.appSecret);
					msg =  rs.execute();
				}
			});
			thread.start();
			try {
				thread.join();		// Wait for thread to finish
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			doLogin2(msg);
		}
	}

	private final Handler login2 = new Handler() {
		public void handleMessage(Message msg) {
			doLogin2(msg);
		}		
	};
	
	void doLogin2(Message msg) {
		String jsonResults = (String)msg.obj;
		if (msg.what == AylaNetworks.AML_ERROR_OK) {
	
			AylaUser aylaUser = AylaSystemUtils.gson.fromJson(jsonResults,  AylaUser.class);
			aylaUser = AylaUser.setCurrent(aylaUser);
			gblAmlTestUser = aylaUser;		// save for unit test
			
			String passMsg = String.format("%s, %s, %s:%d, %s", "P", "amlTest", "secondsToExpiry", aylaUser.accessTokenSecondsToExpiry(), "login2");
			amlTestStatusMsg("Pass", "login2", passMsg);
	
			testSequencer.nextTest().execute();
		} else {
			String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "login2");
			amlTestStatusMsg("Fail", "" +
					"", errMsg);
		}
	}
	
	
	private void amlTestGetDevices() {
		AylaDevice.getDevices(getDevices);
	} 

	private final Handler getDevices = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaDevice[] devices = AylaSystemUtils.gson.fromJson(jsonResults,  AylaDevice[].class);
				gblAmlTestDevice = null;
				for (AylaDevice device : devices) {
					if ( TextUtils.equals(device.dsn, AMLUnitTestConfig.gblAmlTestGenericGatewayDsn) ) {
						
						mScreen.displayProgressInfo(device.toString());
						gblAmlTestLanModeEnabled = false;
						gblAmlTestDevice = device; // assign device under test
//						gblAmlTestDevice.lanModeEnable(); //Start a lan mode session with this device
						//String model = device.model;
						//String dsn = device.dsn;
						//String oemMode = device.oemModel;
						//String connectedAt = device.connectedAt;
						//String mac = device.mac;
						//String lanIp = device.lanIp;
						//String token = device.token;
						// check device class type
						isWifi = device.isWifi(); 
						isGateway = device.isGateway(); 
						isNode = device.isNode();
						
						if (isGateway && (AMLUnitTestConfig.amlTestSuite != AMLUnitTestConfig.testSuite.GENERIC_GATEWAY_SERVICE_ONLY))
						{
							String errMsg = String.format("%s, %s, %s:%s, %s", "F", "amlTest", "device class & suite", "mismatch", "getDevices - class & suite must match");
							amlTestStatusMsg("Fail", "getDevices", errMsg);
							return ;
						}

						if (isGateway) {// Assuming only one gateway is available at this time.
							gblAmlTestGateway = (AylaDeviceGateway) device;

						}// 
					}
					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s:%s, %s", "I", "amlTest", "DSN", device.dsn, "amOwner", device.amOwner(), "getDevices");
				}// end of devices. 

				// TODO: make sure gblAmlTestZigbeeGateway is null before each time this test suite starts. 
				if (gblAmlTestGateway != null)
//				if ( gblAmlTestZigbeeGateway!= null ) 
				{
					String passMsg = String.format("%s, %s, %s:%d, %s", "P", "amlTest", "count", devices.length, "getDevices");
					amlTestStatusMsg("Pass", "getDevices", passMsg);
					testSequencer.nextTest().execute();
				} else { // no gateway is found.
					String errMsg = String.format("%s, %s, %s:%s, %s", "F", "amlTest", "gblAmlTestDevice", "null", "getDevices - no generic gateway assigned.");
					amlTestStatusMsg("Fail", "getDevices", errMsg);
				}
			} else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "getDevices");
				amlTestStatusMsg("Fail", "getDevices", errMsg);
			}
		}		
	};

	// TODO: Generic gateway uses property update manner to do join_window, need to redesign this.
	private void amlTestOpenGatewayJoinWindow() {
		final String joinWindowOpenTime = "60";	// optional window open duration, defaults to 200 seconds
		Map<String, String> callParams = new HashMap<String, String>();
		
		callParams.put(AylaDevice.kAylaDeviceJoinWindowDuration, joinWindowOpenTime);
		gblAmlTestGateway.openRegistrationJoinWindow(openGatewayJoinWindow, callParams);
	}
	
	private final Handler openGatewayJoinWindow = new Handler() {
		public void handleMessage(Message msg) {
			//String jsonResults = (String)msg.obj;	// success = 204
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				String passMsg = String.format("%s, %s, %s", "P", "amlTest", "openGatewayJoinWindow");
				amlTestStatusMsg("Pass", "openGatewayJoinWindow", passMsg);	
				testSequencer.nextTest().execute();
			} else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "openGatewayJoinWindow");
				amlTestStatusMsg("Fail", "openGatewayJoinWindow", errMsg);
			}
		}		
	};
	
	
	private void amlTestGetGatewayCandidates() {
		Map<String, String> callParams = null;
		callParams = new HashMap<String, String>();
		
		gblAmlTestGateway.getRegistrationCandidates(getGatewayCandidates, callParams);
		//gblAmlTestGateway.getRegistrationCandidates(getGatewayCandidates, callParams);
	}
	
	private final Handler getGatewayCandidates = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaDeviceNode[] nodes = AylaSystemUtils.gson.fromJson(jsonResults,  AylaDeviceNode[].class);
				gblAmlTestGenericNode = null;
				for (AylaDeviceNode node : nodes) {
					if (node.dsn.equals(AMLUnitTestConfig.gblAmlTestGenericNodeDsn)) {
						mScreen.displayProgressInfo(node.toString());
//						gblAmlTestLanModeEnabled = false;
						gblAmlTestGenericNode = node; // assign device under test
						//gblAmlTestSmartPlugNode.lanModeEnable(); //Start a lan mode session with this device
						//String model = device.model;
						//String dsn = device.dsn;
						//String oemMode = device.oemModel;
						//String connectedAt = device.connectedAt;
						//String mac = device.mac;
						//String lanIp = device.lanIp;
						//String token = device.token;
					}
					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s:%s, %s", "I", "amlTest", "DSN", node.dsn, "amOwner", node.amOwner(), "getGatewayCandidates");
				}

				if (gblAmlTestGenericNode != null) { // simulate user selection of a device
					String passMsg = String.format("%s, %s, %s:%d, %s", "P", "amlTest", "count", nodes.length, "getGatewayCandidates");
					amlTestStatusMsg("Pass", "getCandidates", passMsg);	

//					if (!(amlTestSuite == testSuite.LAN_MODE_ONLY) && !(amlTestSuite == testSuite.GATEWAY_LAN_MODE_ONLY)) {
						testSequencer.nextTest().execute();
//					} 
				} else {
					String errMsg = String.format("%s, %s, %s:%s, %s", "F", "amlTest", "gblAmlTestDevice", "null", "getGatewayCandidates - no node assigned");
					amlTestStatusMsg("Fail", "getDevices", errMsg);
				}
			} else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "getGatewayCandidates");
				amlTestStatusMsg("Fail", "getDevices", errMsg);
			}
		}		
	};
	
	
	private void amlTestRegisterGatewayCandidate() {
		gblAmlTestGateway.registerCandidate(registerGatewayCandidate, gblAmlTestGenericNode);
		//gblAmlTestGateway.registerCandidate(registerGatewayCandidate, gblAmlTestSmartPlugNode);
	}

	private final Handler registerGatewayCandidate = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				gblAmlTestGenericNode = AylaSystemUtils.gson.fromJson(jsonResults,  AylaDeviceNode.class);
				String productName = gblAmlTestGenericNode.getProductName();

				mScreen.displayProgressInfo(gblAmlTestGenericNode.toString());
				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "productName", productName, "registerGatewayCandidate");
				amlTestStatusMsg("Pass", "registerGatewayCandidate", passMsg);

				testSequencer.nextTest().execute();
//				if ( (amlTestSuite == testSuite.SETUP_ONLY) || (amlTestSuite == testSuite.SETUP_AND_REGISTRATION_ONLY) ) {
//					amlTestDeleteDeviceWifiProfile(); // delete the wifi profile from the device
//				} else {
//					amlTestUserLogout(); // leave the device connected and registered
//				}
			} else {
				String subMethodFailure = "";
     			if (msg.arg2 == AylaNetworks.AML_REGISTER_NEW_DEVICE) {
    				subMethodFailure = "getRegisterNewDevice";
    				if (msg.arg1 == AylaNetworks.AML_ERROR_NOT_FOUND) {
    					// Unsupported registration type
    				} else if (msg.arg1 == AylaNetworks.AML_NO_ITEMS_FOUND) {
    					// Setup token not found for AP-Mode registration
    				}
    			} else
    			if (msg.arg2 == AylaNetworks.AML_GET_REGISTRATION_CANDIDATE) {
    				subMethodFailure = "getRegistrationCandidate";
    				// No registration candidates were found
    			} else if (msg.arg2 == AylaNetworks.AML_GET_MODULE_REGISTRATION_TOKEN) {
    				subMethodFailure = "getModuleRegistrationCandidate";
    				// Failed to connect to new device for registration

    			}  else if (msg.arg2 == AylaNetworks.AML_REGISTER_DEVICE) {
    				subMethodFailure = "registerDevice";
    				// Failed to register the new device
    			}
				String errMsg = String.format("%s, %s, %s:%d, %s, %s.%s", "F", "amlTest", "error", msg.arg1, msg.obj, "registerGatewayCandidate",  subMethodFailure);
				amlTestStatusMsg("Fail", "registerGatewayCandidate", errMsg);
			}
		}				
	};
	
	
	private void amlTestGetGatewayNodes() {
		//((AylaDeviceGateway)gblAmlTestDevice).getNodes(getNodes, null);
		gblAmlTestGateway.getNodes(getNodes, null);
	} 

	private final Handler getNodes = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaDeviceNode[] nodes = AylaSystemUtils.gson.fromJson(jsonResults,  AylaDeviceNode[].class);
				gblAmlTestGenericNode = null;
				gblAmlTestGenericNode2 = null;
				gblAmlTestGenericNode3 = null;
				for (AylaDeviceNode node : nodes) {
					if ( AMLUnitTestConfig.gblAmlTestGenericNodeDsn.equalsIgnoreCase(node.dsn) ) {
						
						mScreen.displayProgressInfo(node.toString());
//						gblAmlTestLanModeEnabled = false;
						gblAmlTestGenericNode = node; // assign device under test
						//gblAmlTestSmartPlugNode.lanModeEnable(); //Start a lan mode session with this device
						//String model = device.model;
						//String dsn = device.dsn;
						//String oemMode = device.oemModel;
						//String connectedAt = device.connectedAt;
						//String mac = device.mac;
						//String lanIp = device.lanIp;
						//String token = device.token;
					} else if ( AMLUnitTestConfig.gblAmlTestGenericNode2Dsn.equalsIgnoreCase(node.dsn) ) {
						mScreen.displayProgressInfo(node.toString()); 
						gblAmlTestGenericNode2 = node; // assign device under test
					} else if ( AMLUnitTestConfig.gblAmlTestGenericNode3Dsn.equalsIgnoreCase(node.dsn) ) {
						gblAmlTestGenericNode3 = node; // assign device under test
					}
					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s:%s, %s", "I", "amlTest", "DSN", node.dsn, "amOwner", node.amOwner(), "getNodes");
				}

				if (gblAmlTestGenericNode != null) { // simulate user selection of a device
					String passMsg = String.format("%s, %s, %s:%d, %s", "P", "amlTest", "count", nodes.length, "getNodes");
					amlTestStatusMsg("Pass", "getNodes", passMsg);	
//					if (!(amlTestSuite == testSuite.LAN_MODE_ONLY) && !(amlTestSuite == testSuite.GATEWAY_LAN_MODE_ONLY)) {
						testSequencer.nextTest().execute();
//					} 
				} else {
					String errMsg = String.format("%s, %s, %s:%s, %s", "F", "amlTest", "amlTestGetGatewayNodes", "null", "getNodes - no nodes assigned");
					amlTestStatusMsg("Fail", "getNodes", errMsg);
				}
			} else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "getNodes");
				amlTestStatusMsg("Fail", "getDevices", errMsg);
			}
		}		
	};
	
	
	private void amlTestGetGenericNodeProperties() {
		gblAmlTestGenericNode.getProperties(getGenericNodeProperties);
	}

	private final Handler getGenericNodeProperties = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				gblAmlTestGenericNode.properties = AylaSystemUtils.gson.fromJson(jsonResults,AylaProperty[].class);
				String name;
				for (AylaProperty property : gblAmlTestGenericNode.properties) {
					name = property.name();
					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s:%s, %s", "P", "amlTest", "name", name, "value", property.value, "getGenericNodeProperties");
//					if (name.equals(gblAmlTestGenericNodePropertyName)) { // simulate user selection of a property
//						gblAmlTestSmartPlugNodeProperty = property; // assign property under test
//					}
					if ( gblAmlTestOnCmdPropertyName.equalsIgnoreCase(name) ) {
						gblAmlTestGenericOnCmdProperty = property;
						gblAmlTestOnCmdPropertyValue 
//							= (Integer)property.datapoint.nValue();           
							= Integer.parseInt(property.datapoint.value());
						continue;
					}
					if ( gblAmlTestOffCmdPropertyName.equalsIgnoreCase(name) ) {
						gblAmlTestGenericOffCmdProperty = property;
						gblAmlTestOffCmdPropertyValue 
//							= (Integer)property.datapoint.nValue();    
							= Integer.parseInt(property.datapoint.value());
						continue;
					}
					if ( gblAmlTestOnOffStatusPropertyName.equalsIgnoreCase(name)) {
						gblAmlTestGenericOnOffStatusProperty = property;
						continue;
					}
				}

				if (gblAmlTestOnCmdPropertyName != null && gblAmlTestOffCmdPropertyName != null) { // As we gonna use these two guys for later testing.
					String passMsg = String.format("%s, %s, %s:%d, %s", "P", "amlTest", "count", gblAmlTestGenericNode.properties.length, "getGenericNodeProperties");
					amlTestStatusMsg("Pass", "getGenericNodeProperties", passMsg);

					testSequencer.nextTest().execute();
				} else {
					String errMsg = String.format("%s, %s, %s:%s, %s", "F", "amlTest", "amlTestGetGenericNodeProperties", "null", "getGenericNodeProperties - no property assigned");
					amlTestStatusMsg("Fail", "getGenericNodeProperties", errMsg);
				}
			}  else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "getGenericNodeProperties");
				amlTestStatusMsg("Fail", "getGenericNodeProperties", errMsg);
			}		
		};
	};

//	private void amlTestGetLightSocketProperties() {
//		gblAmlTestGenericNode2.getProperties(getLightSocketProperties);
//	}
//
//	private final Handler getLightSocketProperties = new Handler() {
//		public void handleMessage(Message msg) {
//			String jsonResults = (String)msg.obj;
//			if (msg.what == AylaNetworks.AML_ERROR_OK) {
//				gblAmlTestGenericNode2.properties = AylaSystemUtils.gson.fromJson(jsonResults,AylaProperty[].class);
//				String name;
//				for (AylaProperty property : gblAmlTestGenericNode2.properties) {
//					name = property.name();
//					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s:%s, %s", "P", "amlTest", "name", name, "value", property.value, "getLightSocketProperties");
//					if (name.equals(gblAmlTestGenericNode2PropertyName)) { // simulate user selection of a property
//						gblAmlTestLightSocketNodeProperty = property; // assign property under test
//					}
//				}
//
//				if (gblAmlTestLightSocketNodeProperty != null) {
//					String passMsg = String.format("%s, %s, %s:%d, %s", "P", "amlTest", "count", gblAmlTestGenericNode.properties.length, "getLightSocketProperties");
//					amlTestStatusMsg("Pass", "getLightSocketProperties", passMsg);
//
//					testSequencer.nextTest().execute();
//				} else {
//					String errMsg = String.format("%s, %s, %s:%s, %s", "F", "amlTest", "gblAmlTestProperty", "null", "getLightSocketProperties - no property assigned");
//					amlTestStatusMsg("Fail", "getLightSocketProperties", errMsg);
//				}
//			}  else {
//				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "getLightSocketProperties");
//				amlTestStatusMsg("Fail", "getLightSocketProperties", errMsg);
//			}		
//		};
//	};

//	private void amlTestGetRemoteControlProperties() {
//		gblAmlTestGenericNode3.getProperties(getRemoteControlProperties);
//	}
//
//	private final Handler getRemoteControlProperties = new Handler() {
//		public void handleMessage(Message msg) {
//			String jsonResults = (String)msg.obj;
//			if (msg.what == AylaNetworks.AML_ERROR_OK) {
//				gblAmlTestGenericNode3.properties = AylaSystemUtils.gson.fromJson(jsonResults,AylaProperty[].class);
//				String name;
//				for (AylaProperty property : gblAmlTestGenericNode3.properties) {
//					name = property.name();
//					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s:%s, %s", "P", "amlTest", "name", name, "value", property.value, "getRemoteControlProperties");
//					if (name.equals(gblAmlTestGenericNode3PropertyName)) { // simulate user selection of a property
//						gblAmlTestRemoteControlNodeProperty = property; // assign property under test
//					}
//				}
//
//				if (gblAmlTestRemoteControlNodeProperty != null) {
//					String passMsg = String.format("%s, %s, %s:%d, %s", "P", "amlTest", "count", gblAmlTestGenericNode.properties.length, "getRemoteControlProperties");
//					amlTestStatusMsg("Pass", "getRemoteControlProperties", passMsg);
//
//					testSequencer.nextTest().execute();
//				} else {
//					String errMsg = String.format("%s, %s, %s:%s, %s", "F", "amlTest", "gblAmlTestProperty", "null", "getRemoteControlProperties - no property assigned");
//					amlTestStatusMsg("Fail", "getRemoteControlProperties", errMsg);
//				}
//			}  else {
//				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "getRemoteControlProperties");
//				amlTestStatusMsg("Fail", "getRemoteControlProperties", errMsg);
//			}		
//		};
//	};
	
	
	/*
	 * Used to identify a node by blinking a light, making a sound, vibrating, etc
	 * Must be supported by node host FW
	 * callParams
	 *   	{"value":"On","time":"55"}
	 *      	the key "value" may be "On" or "Off"
     *      	 The key "time" has a corresponding value from 0 to 255 in seconds, passed as a string
     *       
     * Returns
     *     {"id":"12345"} for the "On" and "Off" option, or 
     *     {"id":"on_0x123456789abc","status":"success"} for the "Result" option
     *     
     * Errors
     * 		401 - Unauthorized
     * 		404 - Node not found
     * 		405 - Not supported for this node
	 */
	private void amlTestIdentify() {
		Map<String, String> callParams = new HashMap<String, String>();
		
		callParams.put(AylaDeviceNode.kAylaNodeParamIdentifyValue, AylaDeviceNode.kAylaNodeParamIdentifyOn);
		//callParams.put(AylaDeviceNode.kAylaNodeParamIdentifyValue, AylaDeviceNode.kAylaNodeParamIdentifyOff);
		//callParams.put(AylaDeviceNode.kAylaNodeParamIdentifyValue, AylaDeviceNode.kAylaNodeParamIdentifyResult);
		callParams.put(AylaDeviceNode.kAylaNodeParamIdentifyTime, "10");
		gblAmlTestGenericNode.identify(identifyNodeHandler, callParams);
	}
	
	private final Handler identifyNodeHandler = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "jsonResults", jsonResults, "identifyNode_Handler");
				amlTestStatusMsg("Pass", "identifyNode", passMsg);
				// Blinking is something last for some time, 
				// need to delay the next test for couple of seconds in order to make it work.
				new CountDownTimer(30000, 10000) {

					@Override
					public void onFinish() {
						testSequencer.nextTest().execute();
					}

					@Override
					public void onTick(long millisUntilFinished) {
					}
				}.start();
				
			}  else {	
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "identifyNode_Handler");
				amlTestStatusMsg("Fail", "identifyNode", errMsg);
			}		
		}
	};
	
	
	private void amlTestGenericNodeDatapoint(final boolean isOn) {
		AylaProperty property = null;
		int propertyValue = 0;
		Handler h = null;
		if ( isOn ) {
			property = gblAmlTestGenericOnCmdProperty;
			propertyValue = gblAmlTestOnCmdPropertyValue;
			h = createOnCmdDatapoint;
		} else {
			property = gblAmlTestGenericOffCmdProperty;
			propertyValue = gblAmlTestOffCmdPropertyValue;
			h = createOffCmdDatapoint;
		}
		propertyValue = 1 - propertyValue;		// toggle boolean value

		final AylaDatapoint datapoint = new AylaDatapoint();
		datapoint.nValue(propertyValue);
		
		gblAmlTestGateway.createNodeDatapoint(h, datapoint, gblAmlTestGenericNode, property, delayExecution);
	}

	
	private final Handler createOffCmdDatapoint = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				final AylaDatapoint datapoint = AylaSystemUtils.gson.fromJson(jsonResults,AylaDatapoint.class);
				
				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "value", jsonResults, "createOffCmdDatapoint");
				amlTestStatusMsg("Pass", "createOffCmdDatapoint", passMsg);
				
				gblAmlTestGenericOffCmdProperty.datapoint = datapoint;
				gblAmlTestOffCmdPropertyValue 
//					= (Integer) datapoint.nValue();
					= Integer.parseInt(datapoint.value());
				testSequencer.nextTest().execute();
				
				
//				new CountDownTimer(5000000, 100) {
//					@Override
//					public void onFinish() {
//					}
//					@Override
//					public void onTick(long millisUntilFinished) {
//						if (AylaNotify.getNotifyOutstanding()) {
//							gblAmlTestGenericOffCmdProperty.datapoint = datapoint;
//							gblAmlTestOffCmdPropertyValue 
//								= Integer.parseInt(datapoint.value());
//							testSequencer.nextTest().execute();
//							this.cancel();
//						}
//					}// onTick
//				}.start();
				
//				if (AylaLanMode.lanModeSession.UP != AylaLanMode.getSessionState()) {
//					final Handler handler = new Handler();
//				    handler.postDelayed(new Runnable() {
//				        @Override
//				        public void run() {
//							testSequencer.nextTest().execute();
//				        }
//				    }, 1000);	// Delay 1 second
//				} else {
//					
//				}
			}  else {	
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "createOffCmdDatapoint");
				amlTestStatusMsg("Fail", "createOffCmdDatapoint", errMsg);
			}		
		}
	};
	
	
	private final Handler createOnCmdDatapoint = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				final AylaDatapoint datapoint = AylaSystemUtils.gson.fromJson(jsonResults,AylaDatapoint.class);

				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "Server Response", jsonResults, "createOnCmdDatapoint");
				amlTestStatusMsg("Pass", "createOnCmdDatapoint", passMsg);

				gblAmlTestGenericOnCmdProperty.datapoint = datapoint;
				gblAmlTestOnCmdPropertyValue 
					= Integer.parseInt(datapoint.value());
				testSequencer.nextTest().execute();
				
//				new CountDownTimer(5000000, 100) {
//					@Override
//					public void onFinish() {
//					}
//					@Override
//					public void onTick(long millisUntilFinished) {
//						if (AylaNotify.getNotifyOutstanding()) {
//							gblAmlTestGenericOnCmdProperty.datapoint = datapoint;
//							gblAmlTestOnCmdPropertyValue 
//								= Integer.parseInt(datapoint.value());
//							testSequencer.nextTest().execute();
//							this.cancel();
//						}
//					}// onTick
//				}.start();
//				if (AylaLanMode.lanModeSession.UP != AylaLanMode.getSessionState()) {
//					final Handler handler = new Handler();
//				    handler.postDelayed(new Runnable() {
//				        @Override
//				        public void run() {
//							testSequencer.nextTest().execute();
//				        }
//				    }, 1000);	// Delay 1 second
//				} else {
//					
//				}
			}  else {	
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "createOnCmdDatapoint");
				amlTestStatusMsg("Fail", "createOnCmdDatapoint", errMsg);
			}		
		}
	};
	
	
//	private void amlTestGetSmartPlugDatapointsByActivity(int count) {
//		Map<String, String> callParams = new HashMap<String, String>();
//		callParams.put("count", Integer.toString(count));
//		gblAmlTestGateway.getNodeDatapointsByActivity(getSmartPlugDatapointsByActivity, callParams, gblAmlTestGenericNode, gblAmlTestSmartPlugNodeProperty, delayExecution);
//	}
//
//	private final Handler getSmartPlugDatapointsByActivity = new Handler() {
//		public void handleMessage(Message msg) {
//			String jsonResults = (String)msg.obj;
//			if (msg.what == AylaNetworks.AML_ERROR_OK) {
//				gblAmlTestSmartPlugNodeProperty.datapoints = AylaSystemUtils.gson.fromJson(jsonResults, AylaDatapoint[].class);
//				String baseType = gblAmlTestSmartPlugNodeProperty.baseType();
//				if (TextUtils.equals("boolean", baseType)) {					
//					// Print all of the datapoints
//					for (AylaDatapoint datapoint : gblAmlTestSmartPlugNodeProperty.datapoints) {
//						String sValue = datapoint.sValueFormatted(baseType);
//						Number nValue = datapoint.nValueFormatted(baseType);
//	
//						String nValueStr = nValue.toString();
//						AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s:%s, %s", "P", "amlTest", "nValue", nValueStr, "sValue", sValue, "getSmartPlugDatapointsByActivity");
//					}
//					// Check most recent value
//					int mostRecent = gblAmlTestSmartPlugNodeProperty.datapoints.length - 1;
//					Number nValue = gblAmlTestSmartPlugNodeProperty.datapoints[mostRecent].nValueFormatted(baseType);
//					int nValueInt = nValue.intValue();
//					if (nValueInt == gblAmlTestSmartPlugNodePropertyValue) {
//						String passMsg = String.format("%s, %s, %s:%d, %s", "P", "amlTest", "nValue", nValueInt, "getSmartPlugDatapointsByActivity");
//						amlTestStatusMsg("Pass", "getSmartPlugDatapointsByActivity", passMsg);
//
//						testSequencer.nextTest().execute();
//					} else {
//						String errMsg = String.format("%s, %s, %s:%s, %d!=%d, %s", "F", "amlTest", "error", "nValueMismatch", nValueInt, gblAmlTestSmartPlugNodePropertyValue, "getSmartPlugDatapointsByActivity");
//						amlTestStatusMsg("Fail", "getSmartPlugDatapointsByActivity", errMsg);							
//					}
//				} else {
//					String errMsg = String.format("%s, %s, %s:%s, %s!=%s, %s", "F", "amlTest", "error", "baseTypeMismatch", baseType, AMLUnitTestConfig.gblAmlBaseType, "getSmartPlugDatapointsByActivity");
//					amlTestStatusMsg("Fail", "getSmartPlugDatapointsByActivity", errMsg);					
//				}
//			} else {
//				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, jsonResults, "getSmartPlugDatapointsByActivity");
//				amlTestStatusMsg("Fail", "getSmartPlugDatapointsByActivity", errMsg);
//			}		
//		}
//	};
	
	
	
		// getAllGroups and then delete them
//		private void amlTestDeleteAllGroups() {
//			gblAmlTestZigbeeGateway.getGroups(deleteAllGroups, null);
//		}

//		private final Handler deleteAllGroups = new Handler() {
//			public void handleMessage(Message msg) {
//				String jsonResults = (String)msg.obj;
//				if (msg.what == AylaNetworks.AML_ERROR_OK) {
//					AylaGroupZigbee[] groups = AylaSystemUtils.gson.fromJson(jsonResults, AylaGroupZigbee[].class);
//					int len = groups.length;
//					for (int i = 0; i < len; i++) {
//						gblAmlTestZigbeeGateway.deleteGroup(deleteOneGroup, groups[i], null);
//					}
//					new CountDownTimer(3000, 1000) {
//						@Override
//						public void onFinish() {
//							testSequencer.nextTest().execute();
//						}
//						@Override
//						public void onTick(long millisUntilFinished) {
//						}
//					}.start();
//				} else {
//					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "deleteAllGroups");
//					amlTestStatusMsg("Fail", "deleteAllGroups", errMsg);
//				}
//			}
//		};
//		
//		private final Handler deleteOneGroup = new Handler() {
//			public void handleMessage(Message msg) {
//				if (msg.what == AylaNetworks.AML_ERROR_OK) {
//					String passMsg = String.format("%s, %s, %s", "P", "amlTest", "deleteGroup");
//					amlTestStatusMsg("Pass", "deleteGroup", passMsg);
//				}  else {	
//					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "deleteGroup");
//					amlTestStatusMsg("Fail", "deleteGroup", errMsg);
//				}		
//			}
//		};


//		private void amlTestCreateGroup() {
//			AylaGroupZigbee group = new AylaGroupZigbee();
//			group.groupName = "SmartPlugGroup";
//			group.nodeDsns = new String[1];
//			group.nodeDsns[0] = AMLUnitTestConfig.gblAmlTestSmartPlugNodeDsn;
//
//			gblAmlTestZigbeeGateway.createGroup(createGroup, group, null, delayExecution);
//		}
//
//		private void amlTestUpdateGroup() {
//			gblAmlTestSmartPlugLightSocketGroup.groupName = "SmartPlugLightSocketGroup";
//			gblAmlTestSmartPlugLightSocketGroup.nodeDsns = new String[2];
//			gblAmlTestSmartPlugLightSocketGroup.nodeDsns[0] = AMLUnitTestConfig.gblAmlTestSmartPlugNodeDsn;
//			gblAmlTestSmartPlugLightSocketGroup.nodeDsns[1] = AMLUnitTestConfig.gblAmlTestLightSocketNodeDsn;
//			
//			gblAmlTestZigbeeGateway.updateGroup(createGroup, gblAmlTestSmartPlugLightSocketGroup, null, delayExecution);
//		}
//
//		private final Handler createGroup = new Handler() {
//			public void handleMessage(Message msg) {
//				String jsonResults = (String)msg.obj;
//				if (msg.what == AylaNetworks.AML_ERROR_OK) {
//					gblAmlTestSmartPlugLightSocketGroup = AylaSystemUtils.gson.fromJson(jsonResults, AylaGroupZigbee.class);
//
//					String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "name", gblAmlTestSmartPlugLightSocketGroup.groupName, "createOrUpdateGroup");
//					amlTestStatusMsg("Pass", "createOrUpdateGroup", passMsg);
//
//					testSequencer.nextTest().execute();
//				}  else {	
//					if (msg.arg1 == 422) { // allow recovery from previous failure
//						String passMsg = String.format("%s, %s, %s:%s, %s", "W", "amlTest", "warning", "group already exists", "createOrUpdateGroup");
//						amlTestStatusMsg("Pass", "createOrUpdateGroup", passMsg);
//
//						testSequencer.nextTest().execute();
//					} else {
//						String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "createOrUpdateGroup");
//						amlTestStatusMsg("Fail", "createOrUpdateGroup", errMsg);
//					}
//				}		
//			}
//		};

//		private void amlTestGetGroups() {
//			// optional call parameters
//			Map<String, Object> callParams = new HashMap<String, Object>();
//			callParams.put(AylaNetworksZigbee.kAylaZigbeeNodeParamDetail, "false");		// default is "false"
//			
//			gblAmlTestZigbeeGateway.getGroups(getGroups, callParams, delayExecution);
//		}
//
//		private final Handler getGroups = new Handler() {
//			public void handleMessage(Message msg) {
//				String jsonResults = (String)msg.obj;
//				if (msg.what == AylaNetworks.AML_ERROR_OK) {
//					AylaGroupZigbee[] newGroups = AylaSystemUtils.gson.fromJson(jsonResults, AylaGroupZigbee[].class);
//
//					if (newGroups.length == 1) {
//						// Minimal check to see if the groupName matches
//						if (newGroups[0].groupName.equals(gblAmlTestSmartPlugLightSocketGroup.groupName)) {
//							String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "name", newGroups[0].groupName, "getGroups");
//							amlTestStatusMsg("Pass", "getGroups", passMsg);
//		
//							testSequencer.nextTest().execute();
//						} else {
//							String errMsg = String.format("%s, %s, %s:%s, %s", "F", "amlTest", "unexpected groupName", newGroups[0].groupName, "getGroups");
//							amlTestStatusMsg("Fail", "getGroups", errMsg);					
//						}
//					} else {
//						String errMsg = String.format("%s, %s, %s:%d, %s", "F", "amlTest", "unexpected number of groups", newGroups.length, "getGroups");
//						amlTestStatusMsg("Fail", "getGroups", errMsg);										
//					}
//				}  else {	
//					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "getGroups");
//					amlTestStatusMsg("Fail", "getGroups", errMsg);
//				}	
//			}
//		};

//		private void amlTestGetGroup() {
//			gblAmlTestZigbeeGateway.getGroup(getGroup, gblAmlTestSmartPlugLightSocketGroup, null, delayExecution);
//		}
//
//		private final Handler getGroup = new Handler() {
//			public void handleMessage(Message msg) {
//				String jsonResults = (String)msg.obj;
//				if (msg.what == AylaNetworks.AML_ERROR_OK) {
//					AylaGroupZigbee newGroup = AylaSystemUtils.gson.fromJson(jsonResults, AylaGroupZigbee.class);
//					
//					// Minimal check to see if the groupName matches
//					if (newGroup.groupName.equals(gblAmlTestSmartPlugLightSocketGroup.groupName)) {
//						String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "name", gblAmlTestSmartPlugLightSocketGroup.groupName, "getGroup");
//						amlTestStatusMsg("Pass", "getGroup", passMsg);
//
//						testSequencer.nextTest().execute();
//					} else {
//						String errMsg = String.format("%s, %s, %s:%s, %s", "F", "amlTest", "unexpected groupName", newGroup.groupName, "getGroup");
//						amlTestStatusMsg("Fail", "getGroup", errMsg);					
//					}
//				}  else {	
//					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "getGroup");
//					amlTestStatusMsg("Fail", "getGroup", errMsg);
//				}		
//			}
//		};
		

	// getAllScenes and then delete them
//		private void amlTestDeleteAllScenes() {
//			gblAmlTestZigbeeGateway.getScenes(deleteAllScenes, null);
//		}

//		private final Handler deleteAllScenes = new Handler() {
//			public void handleMessage(Message msg) {
//				String jsonResults = (String)msg.obj;
//				if (msg.what == AylaNetworks.AML_ERROR_OK) {
//					AylaSceneZigbee[] scenes = AylaSystemUtils.gson.fromJson(jsonResults, AylaSceneZigbee[].class);
//					int len = scenes.length;
//					for (int i = 0; i < len; i++) {
//						gblAmlTestZigbeeGateway.deleteScene(deleteOneScene, scenes[i], null);
//					}
//					
//					new CountDownTimer(3000, 1000) {
//						@Override
//						public void onFinish() {
//							testSequencer.nextTest().execute();
//						}
//						@Override
//						public void onTick(long millisUntilFinished) {
//						}
//					}.start();
//				} else {
//					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "deleteAllScenes");
//					amlTestStatusMsg("Fail", "deleteAllScenes", errMsg);
//				}
//			}
//		};
//
//		private final Handler deleteOneScene = new Handler() {
//			public void handleMessage(Message msg) {
//				if (msg.what == AylaNetworks.AML_ERROR_OK) {
//					String passMsg = String.format("%s, %s, %s", "P", "amlTest", "deleteScene");
//					amlTestStatusMsg("Pass", "deleteScene", passMsg);
//				}  else {	
//					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "deleteScene");
//					amlTestStatusMsg("Fail", "deleteScene", errMsg);
//				}		
//			}
//		};


//	    private void amlTestCreateNodeScene() {
//			AylaSceneZigbee scene = new AylaSceneZigbee();
//			scene.sceneName = "SmartPlugNodeScene";
//			scene.nodeDsns = new String[1];
//			scene.nodeDsns[0] = AMLUnitTestConfig.gblAmlTestSmartPlugNodeDsn;
//			
//			gblAmlTestZigbeeGateway.createScene(createScene, scene, null, delayExecution);
//		}
//
//		private void amlTestUpdateNodeScene() {
//			gblAmlTestSmartPlugLightSocketNodeScene.sceneName = "SmartPlugLightSocketNodeScene";
//			gblAmlTestSmartPlugLightSocketNodeScene.nodeDsns = new String[2];
//			gblAmlTestSmartPlugLightSocketNodeScene.nodeDsns[0] = AMLUnitTestConfig.gblAmlTestSmartPlugNodeDsn;
//			gblAmlTestSmartPlugLightSocketNodeScene.nodeDsns[1] = AMLUnitTestConfig.gblAmlTestLightSocketNodeDsn;
//			
//			gblAmlTestZigbeeGateway.updateScene(createScene, gblAmlTestSmartPlugLightSocketNodeScene, null, delayExecution);
//		}

//		private void amlTestCreateGroupScene() {
//			AylaSceneZigbee scene = new AylaSceneZigbee();
//			scene.sceneName = "SmartPlugLightSocketGroupScene";
//			scene.groups = new Integer[1];
//			scene.groups[0] = gblAmlTestSmartPlugLightSocketGroup.getId();
//			
//			gblAmlTestZigbeeGateway.createScene(createScene, scene, null, delayExecution);
//		}
//
//		private final Handler createScene = new Handler() {
//			public void handleMessage(Message msg) {
//				String jsonResults = (String)msg.obj;
//				if (msg.what == AylaNetworks.AML_ERROR_OK) {
//					AylaSceneZigbee scene = AylaSystemUtils.gson.fromJson(jsonResults, AylaSceneZigbee.class);
//					if (scene.sceneName.equals("SmartPlugNodeScene") || scene.sceneName.equals("SmartPlugLightSocketNodeScene")) {
//						gblAmlTestSmartPlugLightSocketNodeScene = scene;
//					} else if (scene.sceneName.equals("SmartPlugLightSocketGroupScene")) {
//						gblAmlTestSmartPlugLightSocketGroupScene = scene;
//					}
//
//					String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "name", scene.sceneName, "createOrUpdateScene");
//					amlTestStatusMsg("Pass", "createOrUpdateScene", passMsg);
//
//					testSequencer.nextTest().execute();
//				}  else {	
//					if (msg.arg1 == 422) { // allow recovery from previous failure
//						String passMsg = String.format("%s, %s, %s:%s, %s", "W", "amlTest", "warning", "scene already exists", "createOrUpdateScene");
//						amlTestStatusMsg("Pass", "createOrUpdateScene", passMsg);
//
//						testSequencer.nextTest().execute();
//					} else {
//						String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "createOrUpdateScene");
//						amlTestStatusMsg("Fail", "createOrUpdateScene", errMsg);
//					}
//				}		
//			}
//		};

//		private void amlTestGetScenes() {
//			// optional call parameters
//			Map<String, Object> callParams = new HashMap<String, Object>();
//			callParams.put(AylaNetworksZigbee.kAylaZigbeeNodeParamDetail, "false");		// default is "false"
//			gblAmlTestZigbeeGateway.getScenes(getScenes, callParams, delayExecution);
//		}
//
//		private final Handler getScenes = new Handler() {
//			public void handleMessage(Message msg) {
//				String jsonResults = (String)msg.obj;
//				if (msg.what == AylaNetworks.AML_ERROR_OK) {
//					AylaSceneZigbee[] newScenes = AylaSystemUtils.gson.fromJson(jsonResults, AylaSceneZigbee[].class);
//
//					if (newScenes.length == 1) {
//						// Minimal check to see if the sceneName matches
//						if (newScenes[0].sceneName.contains(gblAmlTestSmartPlugLightSocketNodeScene.sceneName)) {
//							String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "name", newScenes[0].sceneName, "getScenes");
//							amlTestStatusMsg("Pass", "getScenes", passMsg);
//		
//							testSequencer.nextTest().execute();
//						} else {
//							String errMsg = String.format("%s, %s, %s:%s, %s", "F", "amlTest", "unexpected sceneName", newScenes[0].sceneName, "getScenes");
//							amlTestStatusMsg("Fail", "getScenes", errMsg);					
//						}
//					} else {
//						String errMsg = String.format("%s, %s, %s:%d, %s", "F", "amlTest", "unexpected number of scenes", newScenes.length, "getScenes");
//						amlTestStatusMsg("Fail", "getScenes", errMsg);										
//					}
//				}  else {	
//					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "getScenes");
//					amlTestStatusMsg("Fail", "getScenes", errMsg);
//				}	
//			}
//		};

//		private void amlTestGetScene() {
//			gblAmlTestZigbeeGateway.getScene(getScene, gblAmlTestSmartPlugLightSocketGroupScene, null, delayExecution);
//		}
//
//		private final Handler getScene = new Handler() {
//			public void handleMessage(Message msg) {
//				String jsonResults = (String)msg.obj;
//				if (msg.what == AylaNetworks.AML_ERROR_OK) {
//					AylaSceneZigbee newScene = AylaSystemUtils.gson.fromJson(jsonResults, AylaSceneZigbee.class);
//					
//					// Minimal check to see if the sceneName matches
//					if (newScene.sceneName.equals(gblAmlTestSmartPlugLightSocketGroupScene.sceneName)) {
//						String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "name", gblAmlTestSmartPlugLightSocketGroupScene.sceneName, "getScene");
//						amlTestStatusMsg("Pass", "getScene", passMsg);
//
//						testSequencer.nextTest().execute();
//					} else {
//						String errMsg = String.format("%s, %s, %s:%s, %s", "F", "amlTest", "unexpected sceneName", newScene.sceneName, "getScene");
//						amlTestStatusMsg("Fail", "getScene", errMsg);					
//					}
//				}  else {	
//					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "getScene");
//					amlTestStatusMsg("Fail", "getScene", errMsg);
//				}		
//			}
//		};
		
//		private void amlTestDeleteNodeScene() {
//			gblAmlTestZigbeeGateway.deleteScene(deleteNodeScene, gblAmlTestSmartPlugLightSocketNodeScene, null, delayExecution);
//		}

//		private void amlTestDeleteGroupScene() {
//			gblAmlTestZigbeeGateway.deleteScene(deleteGroupScene, gblAmlTestSmartPlugLightSocketGroupScene, null, delayExecution);
//		}
//
//		private final Handler deleteNodeScene = new Handler() {
//			public void handleMessage(Message msg) {
//				if (msg.what == AylaNetworks.AML_ERROR_OK) {
//					String passMsg = String.format("%s, %s, %s", "P", "amlTest", "deleteScene");
//					amlTestStatusMsg("Pass", "deleteScene", passMsg);
//
//					testSequencer.nextTest().execute();
//				}  else {	
//					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "deleteScene");
//					amlTestStatusMsg("Fail", "deleteScene", errMsg);
//				}		
//			}
//		};
//		
//		private final Handler deleteGroupScene = new Handler() {
//			public void handleMessage(Message msg) {
//				if (msg.what == AylaNetworks.AML_ERROR_OK) {
//					String passMsg = String.format("%s, %s, %s", "P", "amlTest", "deleteScene");
//					amlTestStatusMsg("Pass", "deleteScene", passMsg);
//
//					testSequencer.nextTest().execute();
//				}  else {	
//					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "deleteScene");
//					amlTestStatusMsg("Fail", "deleteScene", errMsg);
//				}		
//			}
//		};
		
//		private void amlTestRecallNodeScene() {
//			gblAmlTestZigbeeGateway.recallScene(recallNodeScene, gblAmlTestSmartPlugLightSocketNodeScene, null, delayExecution);
//		}
//
//		private final Handler recallNodeScene = new Handler() {
//			public void handleMessage(Message msg) {
//				if (msg.what == AylaNetworks.AML_ERROR_OK) {
//					String passMsg = String.format("%s, %s, %s", "P", "amlTest", "recallScene");
//					amlTestStatusMsg("Pass", "recallScene", passMsg);
//
//					testSequencer.nextTest().execute();
//				}  else {	
//					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "recallScene");
//					amlTestStatusMsg("Fail", "recallScene", errMsg);
//				}		
//			}
//		};
		
//		private void amlTestRecallGroupScene() {
//			gblAmlTestZigbeeGateway.recallScene(recallGroupScene, gblAmlTestSmartPlugLightSocketGroupScene, null, delayExecution);
//		}
//
//		private final Handler recallGroupScene = new Handler() {
//			public void handleMessage(Message msg) {
//				if (msg.what == AylaNetworks.AML_ERROR_OK) {
//					String passMsg = String.format("%s, %s, %s", "P", "amlTest", "recallScene");
//					amlTestStatusMsg("Pass", "recallScene", passMsg);
//
//					testSequencer.nextTest().execute();
//				}  else {	
//					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "recallScene");
//					amlTestStatusMsg("Fail", "recallScene", errMsg);
//				}		
//			}
//		};
		
		// getAllGroups and then delete them
//		private void amlTestDeleteAllBindings() {
//			gblAmlTestZigbeeGateway.getBindings(deleteAllBindings, null);
//		}
//
//		private final Handler deleteAllBindings = new Handler() {
//			public void handleMessage(Message msg) {
//				String jsonResults = (String)msg.obj;
//				if (msg.what == AylaNetworks.AML_ERROR_OK) {
//					AylaBindingZigbee[] bindings = AylaSystemUtils.gson.fromJson(jsonResults, AylaBindingZigbee[].class);
//					int len = bindings.length;
//					for (int i = 0; i < len; i++) {
//						gblAmlTestZigbeeGateway.deleteBinding(deleteOneBinding, bindings[i], null);
//					}
//					
//					new CountDownTimer(3000, 1000) {
//						@Override
//						public void onFinish() {
//							testSequencer.nextTest().execute();
//						}
//						@Override
//						public void onTick(long millisUntilFinished) {
//						}
//					}.start();
//				} else {
//					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "deleteAllBindings");
//					amlTestStatusMsg("Fail", "deleteAllBindings", errMsg);
//				}
//			}
//		};
//		
//		private final Handler deleteOneBinding = new Handler() {
//			public void handleMessage(Message msg) {
//				if (msg.what == AylaNetworks.AML_ERROR_OK) {
//					String passMsg = String.format("%s, %s, %s", "P", "amlTest", "deleteBinding");
//					amlTestStatusMsg("Pass", "deleteBinding", passMsg);
//				}  else {	
//					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "deleteBinding");
//					amlTestStatusMsg("Fail", "deleteBinding", errMsg);
//				}		
//			}
//		};


//		private void amlTestCreateBindingToProperty() {
//			AylaBindingZigbee binding = new AylaBindingZigbee();
//			
//			binding.bindingName = "aRemoteControlToSmartPlugBindinga";
//			binding.fromId = gblAmlTestRemoteControlNode.dsn;
//			binding.fromName = gblAmlTestRemoteControlNodeProperty.name;
//			binding.toId = gblAmlTestSmartPlugNode.dsn;
//			binding.toName = gblAmlTestSmartPlugNodeProperty.name;
//			
//			gblAmlTestZigbeeGateway.createBinding(createBindingToProperty, binding, null, delayExecution);
//		}

//		private void amlTestUpdateBinding() {
//			AylaBindingZigbee binding = new AylaBindingZigbee();
//			
//			binding.bindingName = "RemoteControlToSmartPlugBinding";		// Only the name can be updated
//			binding.fromId = gblAmlTestPropertyBinding.fromId;
//			binding.fromName = gblAmlTestPropertyBinding.fromName;
//			binding.toId = gblAmlTestPropertyBinding.toId;
//			binding.toName = gblAmlTestPropertyBinding.toName;
//			binding.gatewayDsn = gblAmlTestPropertyBinding.gatewayDsn;
//			binding.id = gblAmlTestPropertyBinding.getId();
//
//			gblAmlTestZigbeeGateway.updateBinding(createBindingToProperty, binding, null, delayExecution);
//		}
//
//		private final Handler createBindingToProperty = new Handler() {
//			public void handleMessage(Message msg) {
//				String jsonResults = (String)msg.obj;
//				if (msg.what == AylaNetworks.AML_ERROR_OK) {
//					gblAmlTestPropertyBinding = AylaSystemUtils.gson.fromJson(jsonResults, AylaBindingZigbee.class);
//
//					String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "name", gblAmlTestPropertyBinding.bindingName, "createOrUpdateBindingToProperty");
//					amlTestStatusMsg("Pass", "createOrUpdateBindingToProperty", passMsg);
//
//					testSequencer.nextTest().execute();
//				}  else {	
//					if (msg.arg1 == 422) { // allow recovery from previous failure
//						String passMsg = String.format("%s, %s, %s:%s, %s", "W", "amlTest", "warning", "binding already exists", "createOrUpdateBindingToProperty");
//						amlTestStatusMsg("Pass", "createOrUpdateBindingToProperty", passMsg);
//
//						testSequencer.nextTest().execute();
//					} else {
//						String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "createOrUpdateBindingToProperty");
//						amlTestStatusMsg("Fail", "createOrUpdateBindingToProperty", errMsg);
//					}
//				}		
//			}
//		};

//		private void amlTestCreateBindingToGroup() {
//			AylaBindingZigbee binding = new AylaBindingZigbee();
//			
//			binding.bindingName = "RemoteControlToSmartPlugLightSocketBinding";
//			binding.fromId = gblAmlTestRemoteControlNode.dsn;
//			binding.fromName = gblAmlTestRemoteControlNodeProperty.name;
//			binding.toId = gblAmlTestSmartPlugLightSocketGroup.getId().toString();
//			binding.isGroup = true;
//			gblAmlTestZigbeeGateway.createBinding(createBindingToGroup, binding, null, delayExecution);
//		}
//
//		private final Handler createBindingToGroup = new Handler() {
//			public void handleMessage(Message msg) {
//				String jsonResults = (String)msg.obj;
//				if (msg.what == AylaNetworks.AML_ERROR_OK) {
//					gblAmlTestGroupBinding = AylaSystemUtils.gson.fromJson(jsonResults, AylaBindingZigbee.class);
//
//					String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "name", gblAmlTestGroupBinding.bindingName, "createBindingToGroup");
//					amlTestStatusMsg("Pass", "createBindingToGroup", passMsg);
//
//					testSequencer.nextTest().execute();
//				}  else {	
//					if (msg.arg1 == 422) { // allow recovery from previous failure
//						String passMsg = String.format("%s, %s, %s:%s, %s", "W", "amlTest", "warning", "binding already exists", "createBindingToGroup");
//						amlTestStatusMsg("Pass", "createBindingToGroup", passMsg);
//
//						testSequencer.nextTest().execute();
//					} else {
//						String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "createBindingToGroup");
//						amlTestStatusMsg("Fail", "createBindingToGroup", errMsg);
//					}
//				}		
//			}
//		};

//		private void amlTestGetBindings() {
//			gblAmlTestZigbeeGateway.getBindings(getBindings, null, delayExecution);
//		}
//
//		private final Handler getBindings = new Handler() {
//			public void handleMessage(Message msg) {
//				String jsonResults = (String)msg.obj;
//				if (msg.what == AylaNetworks.AML_ERROR_OK) {
//					AylaBindingZigbee[] newBindings = AylaSystemUtils.gson.fromJson(jsonResults, AylaBindingZigbee[].class);
//
//					if (newBindings.length == 1) {
//						// Minimal check to see if the bindingName matches
//						if (newBindings[0].bindingName.equals(gblAmlTestGroupBinding.bindingName)) {
//							String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "bindingName", newBindings[0].bindingName, "getBindings");
//							amlTestStatusMsg("Pass", "getBindings", passMsg);
//		
//							testSequencer.nextTest().execute();
//						} else {
//							String errMsg = String.format("%s, %s, %s:%s, %s", "F", "amlTest", "unexpected bindingName", newBindings[0].bindingName, "getBindings");
//							amlTestStatusMsg("Fail", "getBindings", errMsg);					
//						}
//					} else {
//						String errMsg = String.format("%s, %s, %s:%d, %s", "F", "amlTest", "unexpected number of bindings", newBindings.length, "getBindings");
//						amlTestStatusMsg("Fail", "getBindings", errMsg);										
//					}
//				}  else {	
//					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "getBindings");
//					amlTestStatusMsg("Fail", "getBindings", errMsg);
//				}	
//			}
//		};

//		private void amlTestGetBinding() {
//			
//			gblAmlTestZigbeeGateway.getBinding(getBinding, gblAmlTestPropertyBinding, null, delayExecution);
//		}
//
//		private final Handler getBinding = new Handler() {
//			public void handleMessage(Message msg) {
//				String jsonResults = (String)msg.obj;
//				if (msg.what == AylaNetworks.AML_ERROR_OK) {
//					AylaBindingZigbee newBinding = AylaSystemUtils.gson.fromJson(jsonResults, AylaBindingZigbee.class);
//					
//					// Minimal check to see if the bindingName matches
//					if (newBinding.bindingName.equals(gblAmlTestPropertyBinding.bindingName)) {
//						String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "name", gblAmlTestPropertyBinding.bindingName, "getBinding");
//						amlTestStatusMsg("Pass", "getBinding", passMsg);
//
//						testSequencer.nextTest().execute();
//					} else {
//						String errMsg = String.format("%s, %s, %s:%s, %s", "F", "amlTest", "unexpected bindingName", newBinding.bindingName, "getBinding");
//						amlTestStatusMsg("Fail", "getBinding", errMsg);					
//					}
//				}  else {	
//					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "getBinding");
//					amlTestStatusMsg("Fail", "getBinding", errMsg);
//				}		
//			}
//		};
		
//		private void amlTestDeletePropertyBinding() {
//			// <---- Set BREAK POINT here so the user has time to test the new property binding before it is deleted
//			gblAmlTestZigbeeGateway.deleteBinding(deletePropertyBinding, gblAmlTestPropertyBinding, null, delayExecution);
//		}
//
//		private void amlTestDeleteGroupBinding() {
//			// <---- Set BREAK POINT here so the user has time to test the new group binding before it is deleted
//			gblAmlTestZigbeeGateway.deleteBinding(deleteGroupBinding, gblAmlTestGroupBinding, null, delayExecution);
//		}
//
//		private final Handler deletePropertyBinding = new Handler() {
//			public void handleMessage(Message msg) {
//				if (msg.what == AylaNetworks.AML_ERROR_OK) {
//					String passMsg = String.format("%s, %s, %s", "P", "amlTest", "deleteBinding");
//					amlTestStatusMsg("Pass", "deleteBinding", passMsg);
//
//					testSequencer.nextTest().execute();
//				}  else {	
//					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "deleteBinding");
//					amlTestStatusMsg("Fail", "deleteBinding", errMsg);
//				}		
//			}
//		};
//		
//		private final Handler deleteGroupBinding = new Handler() {
//			public void handleMessage(Message msg) {
//				if (msg.what == AylaNetworks.AML_ERROR_OK) {
//					String passMsg = String.format("%s, %s, %s", "P", "amlTest", "deleteBinding");
//					amlTestStatusMsg("Pass", "deleteBinding", passMsg);
//
//					testSequencer.nextTest().execute();
//				}  else {	
//					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "deleteBinding");
//					amlTestStatusMsg("Fail", "deleteBinding", errMsg);
//				}		
//			}
//		};
		
		
//		private void amlTestDeleteGroup() {
//			gblAmlTestZigbeeGateway.deleteGroup(deleteGroup, gblAmlTestSmartPlugLightSocketGroup, null, delayExecution);
//		}
//
//		private final Handler deleteGroup = new Handler() {
//			public void handleMessage(Message msg) {
//				if (msg.what == AylaNetworks.AML_ERROR_OK) {
//					String passMsg = String.format("%s, %s, %s", "P", "amlTest", "deleteGroup");
//					amlTestStatusMsg("Pass", "deleteGroup", passMsg);
//
//					testSequencer.nextTest().execute();
//				}  else {	
//					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "deleteGroup");
//					amlTestStatusMsg("Fail", "deleteGroup", errMsg);
//				}		
//			}
//		};
		
		// ----------------------------------- Device Notifications --------------------------
		private void amlTestCreateDeviceNotification() {
			AylaDeviceNotification deviceNotification = new AylaDeviceNotification();
			deviceNotification.notificationType = AylaDeviceNotification.aylaDeviceNotificationTypeOnConnectionLost;
			deviceNotification.message = "WiFi connection dropped";
			deviceNotification.threshold = 600;
			deviceNotification.deviceNickname = "deviceNotificaitonNickname";
			//Random random = new Random(); Integer anInt = random.nextInt();

			restService4 = gblAmlTestDevice.createNotification(createOrUpdateDeviceNotification, deviceNotification);
		}

		private final Handler createOrUpdateDeviceNotification = new Handler() {
			public void handleMessage(Message msg) {
				String jsonResults = (String)msg.obj;
				if (msg.what == AylaNetworks.AML_ERROR_OK) {
					gblAmlTestDevice.deviceNotification = AylaSystemUtils.gson.fromJson(jsonResults, AylaDeviceNotification.class);
					AylaDeviceNotification deviceNotification = gblAmlTestDevice.deviceNotification;

					String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "threshold", deviceNotification.threshold, "createOrUpdateDeviceNotification");
					amlTestStatusMsg("Pass", "createDeviceNotification", passMsg);
					testSequencer.nextTest().execute();
				}  else {	
					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "createOrUpdateDeviceNotification");
					amlTestStatusMsg("Fail", "createDeviceNotification", errMsg);
				}		
			}
		};
		
		private void amlTestUpdateDeviceNotification() {
			gblAmlTestDevice.deviceNotification.threshold = 3600;
			
			//gblAmlTestDevice.updateNotification(createOrUpdateDeviceNotification, gblAmlTestDevice.deviceNotification);
			gblAmlTestDevice.deviceNotification.updateNotification(createOrUpdateDeviceNotification);
		}
		
		private void amlTestGetDeviceNotifications() {
			gblAmlTestDevice.getNotifications(getDeviceNotifications, null);
			//gblAmlTestDevice.deviceNotification.getNotifications(getDeviceNotifications, null);
		}
		
		private final Handler getDeviceNotifications = new Handler() {
			public void handleMessage(Message msg) {
				String jsonResults = (String)msg.obj;
				if (msg.what == AylaNetworks.AML_ERROR_OK) {
					gblAmlTestDevice.deviceNotifications = AylaSystemUtils.gson.fromJson(jsonResults, AylaDeviceNotification[].class);
					//AylaDeviceNotification deviceNotification = gblAmlTestDevice.deviceNotification;

					if (gblAmlTestDevice.deviceNotifications.length > 0) {
						gblAmlTestDeviceNotification = gblAmlTestDevice.deviceNotifications[0];
						String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "threshold", gblAmlTestDeviceNotification.threshold, "getDeviceNotifications");
						amlTestStatusMsg("Pass", "getDeviceNotifications", passMsg);
						testSequencer.nextTest().execute();
					} else {	
						String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "getDeviceNotifications_err1");
						amlTestStatusMsg("Fail", "createDeviceNotification", errMsg);
					}
				}  else {	
					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "getDeviceNotifications_err2");
					amlTestStatusMsg("Fail", "getDeviceNotifications", errMsg);
				}		
			}
		};
		
		private void amlTestDeleteDeviceNotification() {
			gblAmlTestDevice.destroyNotification(destroyDeviceNotification, gblAmlTestDevice.deviceNotification);
			// gblAmlTestDevice.deviceNotification.destroyNotification(destroyDeviceNotification);
		}
		
		private final Handler destroyDeviceNotification = new Handler() {
			public void handleMessage(Message msg) {
				if (msg.what == AylaNetworks.AML_ERROR_OK) {		
					String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "return", "success", "destroyDeviceNotification");
					amlTestStatusMsg("Pass", "destroyDeviceNotification", passMsg);

					testSequencer.nextTest().execute();
					
				}  else {	
					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "destroyDeviceNotification");
					amlTestStatusMsg("Fail", "destroyDeviceNotification", errMsg);
				}		
			}
		};
		
		// ------------------------------------- Device Application Notifications -------------------------------------------------
		private void amlTestCreateAppNotification() {
			AylaAppNotification appNotification = new AylaAppNotification();
			
			// pick an appType
			appNotification.appType = AylaAppNotification.aylaAppNotificationTypeSms;
			//appNotification.appType = AylaAppNotification.aylaAppNotificationTypeEmail;
			//appNotification.appType = AylaAppNotification.aylaAppNotificationTypePush;
			
			appNotification.notificationAppParameters.username = "yourNameHere";
			
			// Params for the appType picked
			if (appNotification.appType.equals(AylaAppNotification.aylaAppNotificationTypeSms)) {
				appNotification.notificationAppParameters.countryCode = AMLUnitTestConfig.gblAmlCountryCode;
				appNotification.notificationAppParameters.phoneNumber = AMLUnitTestConfig.gblAmlPhoneNumber;
				appNotification.notificationAppParameters.contactId = AMLUnitTestConfig.gblAmlContactID;
				appNotification.notificationAppParameters.message = "Testing create device notification sms application";
			} else if (appNotification.appType.equals(AylaAppNotification.aylaAppNotificationTypeEmail)) {
				appNotification.notificationAppParameters.email = AMLUnitTestConfig.gblAmlEmailAddress;
				appNotification.notificationAppParameters.message = "Testing create device notification email application";
			} else if (appNotification.appType.equals(AylaAppNotification.aylaAppNotificationTypePush)) {
				appNotification.notificationAppParameters.registrationId = PushNotification.registrationId;
				appNotification.notificationAppParameters.message = "Testing create device notification push application";
				appNotification.notificationAppParameters.pushSound = "default";
				appNotification.notificationAppParameters.pushMdata = "my meta data";
			}
			
			gblAmlTestDevice.deviceNotification.createApp(createOrUpdateAppNotification, appNotification);
		}

		private final Handler createOrUpdateAppNotification = new Handler() {
			public void handleMessage(Message msg) {
				String jsonResults = (String)msg.obj;
				if (msg.what == AylaNetworks.AML_ERROR_OK) {
					AylaAppNotification appNotification = AylaSystemUtils.gson.fromJson(jsonResults,AylaAppNotification.class);
					gblAmlTestDevice.deviceNotification.appNotification = appNotification;

					String countryCode = appNotification.notificationAppParameters.countryCode;
					String phoneNumber = appNotification.notificationAppParameters.phoneNumber;
					String message = appNotification.notificationAppParameters.message;

					String passMsg = String.format("%s, %s, %s,:%s, %s:%s, %s", "P", "amlTest", "phoneNumber", phoneNumber, "message", message, "createOrUpdateAppNotification_handler");
					amlTestStatusMsg("Pass", "createOrUpdateAppNotification", passMsg);

					testSequencer.nextTest().execute();
				}  else {
					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "createOrUpdateAppNotification_handler");
					amlTestStatusMsg("Fail", "createOrUpdateAppNotification", errMsg);
				}
			}
		};
		
		private void amlTestUpdateAppNotification() {
			AylaAppNotification appNotification = gblAmlTestDevice.deviceNotification.appNotification;
			
			// pick an appType
			appNotification.appType = AylaAppNotification.aylaAppNotificationTypeSms;
			//appNotification.appType = AylaAppNotification.aylaAppNotificationTypeEmail;
			//appNotification.appType = AylaAppNotification.aylaAppNotificationTypePush;
			
			// Params for the appType picked
			if (appNotification.appType.equals(AylaAppNotification.aylaAppNotificationTypeSms)) {
				appNotification.notificationAppParameters.countryCode = AMLUnitTestConfig.gblAmlCountryCode;
				appNotification.notificationAppParameters.phoneNumber = AMLUnitTestConfig.gblAmlPhoneNumber;
				appNotification.notificationAppParameters.message = "Testing update device notification sms application";
			} else
			if (appNotification.appType.equals(AylaAppNotification.aylaAppNotificationTypeEmail)) {
				appNotification.notificationAppParameters.username = "yourNameHere";
				appNotification.notificationAppParameters.email = AMLUnitTestConfig.gblAmlEmailAddress;
				appNotification.notificationAppParameters.message = "Testing update device notification email application";
			} else
			if (appNotification.appType.equals(AylaAppNotification.aylaAppNotificationTypePush)) {
				appNotification.notificationAppParameters.registrationId = PushNotification.registrationId;
				appNotification.notificationAppParameters.message = "Testing update device notification push application";
				appNotification.notificationAppParameters.pushSound = "default";
				appNotification.notificationAppParameters.pushMdata = "my meta data";
			}
			
			gblAmlTestDevice.deviceNotification.updateApp(createOrUpdateAppNotification, appNotification);
		}

		private void amlTestGetAppNotifications() {
			//Map<String, String> s = new HashMap<String, String>(); TBD
			//callParams.put("count", "3");
			//callParams.put("test", "def");
			Map<String, String> callParams = null; // cappParams is unused at this time
			gblAmlTestDevice.deviceNotification.getApps(getAppNotifications, callParams);
		}

		private final Handler getAppNotifications = new Handler() {
			public void handleMessage(Message msg) {
				String jsonResults = (String)msg.obj;
				if (msg.what == AylaNetworks.AML_ERROR_OK) {
					gblAmlTestDevice.deviceNotification.appNotifications = AylaSystemUtils.gson.fromJson(jsonResults, AylaAppNotification[].class);

					for (AylaAppNotification appNotification : gblAmlTestDevice.deviceNotification.appNotifications) {
						AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "P", "amlTest", "message", appNotification.notificationAppParameters.message, "getAppNotifications");
						gblAmlTestAppNotification = appNotification;
					}

					String passMsg = String.format("%s, %s, %s:%d, %s", "P", "amlTest", "count", gblAmlTestDevice.deviceNotification.appNotifications.length, "getAppNotifications");
					amlTestStatusMsg("Pass", "getAppNotifications", passMsg);

					testSequencer.nextTest().execute();
//					amlTestDestroyApplicationTrigger();
				}  else {
					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, jsonResults, "getAppNotifications");
					amlTestStatusMsg("Fail", "getAppNotifications", errMsg);
				}
			}
		};

		private void amlTestDeleteAppNotification() {
			gblAmlTestDeviceNotification.destroyApp(destroyAppNotification, gblAmlTestAppNotification);
			//gblAmlTestDeviceNotification.appNotification.destroy(destroyAppNotification); 
		}

		private final Handler destroyAppNotification = new Handler() {
			public void handleMessage(Message msg) {
				if (msg.what == AylaNetworks.AML_ERROR_OK) {		
					String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "return", "success", "destroyAppNotification");
					amlTestStatusMsg("Pass", "destroyApplicationTrigger", passMsg);

					testSequencer.nextTest().execute();
				}  else {	
					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "destroyAppNotification");
					amlTestStatusMsg("Fail", "destroyAppNotification", errMsg);
				}		
			}
		};

		
		
	// TODO: extract as a common utility function
		private void amlTestStatusMsg(String passFail, String methodName,
				String methodMsg) {
			String testMsg = "";
			if (passFail != null && passFail.contains("Pass")) {
				AylaSystemUtils.saveToLog("\n%s", methodMsg);
				testMsg = String.format("%s %s", methodName, "passed");

				if (!passFail.contains("Sync")) {
					mScreen.displayFailMessage(testMsg + "\n\n");
				} else {
					Log.i(tag, testMsg);
				}
			} else if (passFail != null && passFail.contains("Fail")) {
				AylaSystemUtils.saveToLog("\n%s", methodMsg);
				testMsg = String.format("%s %s.\n%s", methodName, "failed", methodMsg);
				if (!passFail.contains("Sync")) {
					mScreen.displayPassMessage(testMsg + "\n\n");
				} else {
					Log.e(tag, testMsg);
				}
			} else {
				// TODO: passFail null or no match.
			}
			AylaSystemUtils.saveToLog("%s", testMsg);

		}// end of amlTestStatusMsg 

}// end of AMLZigbeeUnitTestCaseServiceOnly class               






