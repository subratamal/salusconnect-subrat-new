/*
 * Test.java
 * Ayla Mobile Library
 * 
 * Created by Di Wang on 05/04/2015.
 * Copyright (c) 2015 Ayla Networks Inc. All rights reserved.
 * */

package com.aylanetworks.AMLUnitTest.facility;

/**
 * Generic test function call (used by ArrayList of tests, aka test suites)
 * */
public class Test {
	  public void execute() {}
	  public void execute(Object arg) {}
	  public void execute(Object arg1, Object arg2) {}
	  public void execute(Object arg1, Object arg2, Object arg3) {}
	  public void execute(Object arg1, Object arg2, Object arg3, Object arg4) {}
	  public void execute(Object arg1, Object arg2, Object arg3, Object arg4, Object arg5) {}
}// end of Test class               









