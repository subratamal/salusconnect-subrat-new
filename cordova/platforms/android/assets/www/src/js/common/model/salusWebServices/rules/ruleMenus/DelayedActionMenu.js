"use strict";

define([
	"app",
	"common/constants",
	"common/model/salusWebServices/rules/Rule.model"
], function (App, constants) {
	App.module("Models", function (Models, App, B) {
		Models.DelayedActionMenu = B.Model.extend({
			defaults: {
				menu: null,
				equipmentArray: null
			},
			initialize: function (equipmentArray) {
				this.equipmentArray = equipmentArray;
				this._buildMenu();
			},
			_buildMenu: function (){
				this.set("menu", {
					menuModel: new B.Model({
						uniqueView: false,
						type: constants.oneTouchMenuTypes.delayedAction,
						isLeafMenu: false
					}),
					childMenuCollection: new B.Collection([
						new Models.RuleMenuItemModel({
							displayTextKey: "equipment.oneTouch.menus.thenDoThis.root.stateOfEquipment"
						}, {childMenu: this._stateOfEquipment()})
					])
				});
			},
			_stateOfEquipment: function () {
				return  {
					menuModel: new B.Model({
						uniqueView: false,
						type: constants.oneTouchMenuTypes.delayedAction,
						isLeafMenu: false
					}),
					childMenuCollection: new B.Collection(this.equipmentArray.getEquipmentMenuForRule(constants.oneTouchMenuTypes.delayedAction))
				};
			},
			_undoAction: function () {
				return  {
					menuModel: new B.Model({
						uniqueView: "undo-action-select",
						type: constants.oneTouchMenuTypes.delayedAction,
						isLeafMenu: false
					}),
					uniqueViewType: "undo-action-select",
					childMenuCollection: new B.Collection([this._timerView()])
				};
			},
			_timerView: function () {
				return  {
					menuModel: new B.Model({
						uniqueView: "timer",
						type: constants.oneTouchMenuTypes.delayedAction,
						isLeafMenu: true
					}),
					uniqueViewType: "timer"
				};
			}
		});
	});

	return App.Models.DelayedActionMenu;
});