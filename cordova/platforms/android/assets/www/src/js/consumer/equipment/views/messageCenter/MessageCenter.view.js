"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusPage",
	"consumer/views/SalusTabbedSection.view",
	"consumer/equipment/views/messageCenter/AlertTable.view",
	"consumer/equipment/views/NotificationsTable.view"
], function (App, templates, SalusPageMixin, SalusTabbedView) {

	App.module("Consumer.Equipment.Views", function (Views, App, B, Mn, $, _) {
		Views.MessageCenterView = Mn.LayoutView.extend({
			className: "message-center-page container-fluid",
			template: templates['equipment/messageCenter/messageCenter'],
			tabSubHeadMap: {
				"bb-alerts-tab": "messageCenter.alerts.subHeader",
				"bb-notifications-tab": "messageCenter.notifications.subHeader"
			},
			regions: {
				"tabViewRegion": "#bb-tab-view-region"
			},
			bindings: {
				"#bb-sub-header": {
					observe: "subHeadKey",
					onGet: function (value) {
						return App.translate(value);
					}
				}
			},
			initialize: function () {
				_.bindAll(this, "_onTabChange");

				this.alertView = new Views.AlertTableView();
				this.notificationsView = new Views.NotificationsTableView();
				this.model = new B.Model({"subHeadKey": this.tabSubHeadMap["bb-alerts-tab"]});
			},
			onRender: function () {
				var options = {
					className: "nav-tabs-centered",
					tabs: [
						{
							key: "bb-alerts-tab",
							classes: "alerts-tab",
							i18n: "messageCenter.alerts.title",
							view: this.alertView
						},
						{
							key: "bb-notifications-tab",
							classes: "notifications-tab",
							i18n: "messageCenter.notifications.title",
							view: this.notificationsView
						}
					],
					onTabChangeCb: this._onTabChange
				};

				this.tabViewRegion.show(new SalusTabbedView(options));
			},
			_onTabChange: function (evt, key) {
				this.model.set("subHeadKey", this.tabSubHeadMap[key]);
			}
		}).mixin([SalusPageMixin], {
			analyticsSection: "equipment",
			analyticsPage: "messageCenter"
		});
	});

	return App.Consumer.Equipment.Views.MessageCenterView;
});