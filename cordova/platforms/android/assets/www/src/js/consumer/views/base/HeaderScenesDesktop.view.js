"use strict";

define([
	"app",
	"bluebird",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"common/util/VendorfyCssForJs"
], function (App, P, constants, consumerTemplates, SalusView, VendorfyCssForJs) {

	App.module("Consumer.Views", function (Views, App, B, Mn, $, _) {

		/**
		 * TODO: all things named 'Scenes' need to be changed to MyStatus
		 */

		Views.SceneItemDesktopView = Mn.ItemView.extend({
			template: consumerTemplates["base/headerSceneItem"],
			tagName: "li",
			className: "scene-item-header-desktop",
			attributes: {
				role: "button"
			},
			ui: {
				name: ".bb-scene-name",
				selected: ".bb-scene-selected",
				gradientWithIcon: ".bb-scene-background"
			},
			bindings: {
				".bb-scene-name": {
					observe: "name"
				},
				".bb-scene-selected": {
					observe: "selected",
					update: function ($el, selected) {
						if (App.hasCurrentGateway()) {
							$el.toggleClass("selected", selected).toggleClass("deselected", !selected);
						} else {
							$el.addClass("invalid").removeClass("selected").removeClass("deselected");
						}
					}
				}
			},
			events: {
				"click": "_setMyStatus"
			},
			initialize: function () {
				_.bindAll(this, "_setMyStatus");
				this.clicking = false;
			},
			onRender: function () {
				var iconPaths = constants.myStatusIconPaths,
						colors = constants.getStatusIconBackgroundColor(this.model.get("icon"));

				if (this.model.get("icon")) {
					VendorfyCssForJs.formatBgGradientWithImage(
							this.ui.gradientWithIcon,
							"center center/60% 60%",
							"no-repeat",
							App.rootPath(iconPaths[this.model.get("icon")]),
							"145deg",
							colors.topColor,
							"0%",
							colors.botColor,
							"100%",
							"/images/icons/myStatus/icon_question_mark.svg"
					);
				}
			},
			_setMyStatus: function () {
				var that = this, myStatusProp, gatewayNode = App.getCurrentGateway().getGatewayNode(),myStatusKey;
                
                if(that.model.get("selected")){
                    myStatusKey="NULL";
                }else{
                    myStatusKey=that.model.get("key");
                }

				if (!this.clicking && gatewayNode) {
					this.clicking = true;
					this.showSpinner({textKey: "none"});

					gatewayNode.getDevicePropertiesPromise().then(function () {
						if ((myStatusProp = gatewayNode.get("MyStatus"))) {
							that.listenToOnce(myStatusProp, "propertiesSynced", that.hideSpinner);
							that.listenToOnce(myStatusProp, "propertiesFailedSync", that.hideSpinner);

							App.salusConnector.setMyStatus(myStatusKey).finally(function () {
								that.clicking = false;
							});
						} else {
							that.clicking = false;
							that.hideSpinner();

						}
					}).catch(function (err) {
						that.clicking = false;
						that.hideSpinner();

						return P.reject(err);
					});
				}
			}
		}).mixin([SalusView]);

		Views.HeaderScenesDesktopView = Mn.CompositeView.extend({
			tagName: "li",
			template: consumerTemplates["base/headerDropdown"],
			childViewContainer: "ul",
			childView: Views.SceneItemDesktopView,
			initialize: function () {
				this.collection = App.salusConnector.getRuleGroupCollection() || new B.Collection();

				this.myStatusButton = new Views.MyStatusButtonView();
			},

			onRender: function () {
				this.$('ul').prepend(this.myStatusButton.render().$el);
			},

			onDestroy: function () {
				this.myStatusButton.destroy();
			}
		}).mixin([SalusView]);

		Views.MyStatusButtonView = Mn.ItemView.extend({
			tagName: "li",
			template: consumerTemplates["myStatus/myStatusLink"],
			className: "myStatus-link",
			events: {
				"click": "_goToMyStatus",
				"mousedown": "_downChangeColor",
				"mouseup": "_upChangeColor"
			},
			ui: {
				"myStatusButton": ".bb-myStatus-link-button"
			},
			initialize: function () {
				_.bindAll(this, "_goToMyStatus", "_downChangeColor", "_upChangeColor");
			},
			_downChangeColor: function () {
				this.ui.myStatusButton.addClass("down");
				this.ui.myStatusButton.removeClass("up");
			},
			_upChangeColor: function () {
				this.ui.myStatusButton.addClass("up");
				this.ui.myStatusButton.removeClass("down");
			},
			_goToMyStatus: function () {
				App.navigate("myStatus");
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.Views.HeaderScenesDesktopView;
});