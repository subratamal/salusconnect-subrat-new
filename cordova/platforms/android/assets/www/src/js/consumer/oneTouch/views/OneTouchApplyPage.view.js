"use strict";

define([
	"app",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusPage",
	"consumer/views/mixins/mixin.salusView",
	"common/util/VendorfyCssForJs",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/myStatus/views/MyStatusGroup.view"
], function (App, constants, consumerTemplates, SalusPage, SalusView, VendorfyCss) {

	App.module("Consumer.OneTouch.Views", function (Views, App, B, Mn, $, _) {
		Views.OneTouchApplyPageView = Mn.LayoutView.extend({
			id: "one-touch-apply-page",
			className: "container",
			template: consumerTemplates["oneTouch/oneTouchApplyPage"],
			regions: {
				statusCollectionRegion: ".bb-status-collection-region",
				skipButtonRegion: ".bb-skip-button",
				cancelButtonRegion: ".bb-cancel-button"
			},
			initialize: function () {
				_.bindAll(this, "handleSkipClicked", "handleCancelClicked");

				this.currentRule = App.Consumer.OneTouch.ruleMakerManager.getCurrentRule();
               
				this.skipButton = new App.Consumer.Views.SalusButtonPrimaryView({
					buttonTextKey: "common.labels.next",
					className: "width100 btn btn-primary",
					clickedDelegate: this.handleSkipClicked
				});

				this.cancelButton = new App.Consumer.Views.SalusButtonPrimaryView({
					buttonTextKey: "common.labels.cancel",
					className: "width100 btn btn-default",
					clickedDelegate: this.handleCancelClicked
				});
			},

			onRender: function () {
				var that = this;

				App.salusConnector.getDataLoadPromise(["rules", "ruleGroups"]).then(function () {
					if (!that.currentRule) {
						return App.navigate("oneTouch");
					}

					that.statusCollectionView = new App.Consumer.OneTouch.Views.StatusCollectionView({
						collection: App.salusConnector.getRuleGroupCollection(),
						rule: that.currentRule
					});

					that.statusCollectionRegion.show(that.statusCollectionView);

					that.skipButtonRegion.show(that.skipButton);
					that.cancelButtonRegion.show(that.cancelButton);
				});
			},

			handleSkipClicked: function () {
				var path = "oneTouch/" + App.getCurrentGatewayDSN() + "/pin";

				App.navigate(path);
			},

			handleCancelClicked: function () {
				App.navigate("oneTouch");
			}
		}).mixin([SalusPage], {
			analyticsSection: "oneTouch",
			analyticsPage: "apply"
		});

		Views.StatusApplyTileView = Mn.ItemView.extend({
			className: "status-apply-tile col-xs-4",
			template: consumerTemplates["myStatus/myStatusTile"],
			ui: {
				tileBackground: ".bb-myStatus-tile-background",
				selected: ".bb-myStatus-selected"
			},
			bindings: {
				".bb-title-text": {
					observe: "name"
				}
			},
			attributes: {
				role: "button"
			},
            triggers: {
                "click": "click:item"
            },
			initialize: function (/*options*/) {
				this.rule = this.options.rule;
			},
			onRender: function () {
				var iconPaths = constants.myStatusIconPaths,
						colors = constants.getStatusIconBackgroundColor(this.model.get("icon"));

				VendorfyCss.formatBgGradientWithImage(
						this.ui.tileBackground,
						"center center/48% 48%",
						"no-repeat",
						App.rootPath(iconPaths[this.model.get("icon")]),
						"145deg",
						colors.topColor, //top color
						"0%",
						colors.botColor, //bottom color
						"100%"
				);

				this.setSelectedUI(this.model.hasRule(this.rule));
			},
			setSelectedUI: function (toggleBool) {
				this.ui.selected.toggleClass("selected", toggleBool);
				this.ui.selected.toggleClass("deselected", !toggleBool);
			}
		}).mixin([SalusView]);

		Views.StatusCollectionView = Mn.CollectionView.extend({
			className: "row status-collection-row",
			childView: Views.StatusApplyTileView,
			childViewOptions: function (model) {
				return {
					model: model,
					rule: this.options.rule
				};
			},
            childEvents: {
                "click:item": "_handleItemClick"
            },
            _handleItemClick: function(childView) {
                var that = this;
                var model = childView.model, addRemovePromise = [];
                var collection = this.collection;
                
                _.each(collection.models, function(m) {
                    if(m.hasRule(childView.rule)) {
                        addRemovePromise.push(m.removeRule(childView.rule));
                    } else {
                        if(m.get("key") === model.get("key")) {
                            addRemovePromise.push(model.addRule(childView.rule));
                        }
                    }
                });
				childView.showSpinner();
                return P.all(addRemovePromise).then(function() {
                    _.each(that.children._views, function(view) {
                        view.setSelectedUI(false);
                    });
                    childView.hideSpinner();
					childView.setSelectedUI(model.hasRule(childView.rule));
                });
            }
		}).mixin([SalusView]);
	});

	return App.Consumer.OneTouch.Views.OneTouchApplyPageView;
});
