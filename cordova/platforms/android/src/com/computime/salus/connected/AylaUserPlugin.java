
package com.computime.salus.connected;

import android.os.Bundle;
import org.apache.cordova.*;
import org.apache.cordova.PluginResult.Status;
import org.json.*;
import android.util.Log;
import android.os.Handler;
import android.os.Message;
import com.aylanetworks.aaml.*;
import com.aylanetworks.aaml.AylaNetworks.lanMode;

import java.security.AlgorithmParameterGenerator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

public class AylaUserPlugin extends CordovaPlugin
{
    int serviceReachability;

    String userName;
    String password;
    String appId;
    String appSecret;

    String savedUserName;
    String savedPassword;
    ArrayList<AylaDevice> devices = null;

    static AylaUser aylaUser = null;

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {

        if(action.equals("login")) {
            login(args, callbackContext);
            return true;
        } else if(action.equals("logout")) {
            logout(args, callbackContext);
            return true;
        }  else if(action.equals("refreshAccessToken")) {
            refreshAccessToken(callbackContext, args);
            return true;
        }  else if (action.equals("createDatapoint")) {
            createDatapoint(callbackContext, args);
            return true;
        } else if (action.equals("getProperties")) {
            getProperties(callbackContext, args);
            return true;
        } else if (action.equals("getDevices")) {
            getDevices(callbackContext, args);
            return true;
        } else if (action.equals("getDatapointsByActivity")) {
            getDatapointsByActivity(callbackContext, args);
            return true;
        }

        // saw that these methods will have to implemented in the switch statement
//        else if (action.equals("havePinSet")) {
//            return true;
//        } else if (action.equals("setPin")) {
//            return true;
//        } else if (action.equals("checkPin")) {
//            return true;
//        } else if (action.equals("removePin")) {
//            return true;
//        }

        return false;
    }

    // helper method to call the callback method
    private void callCallbackContext (CallbackContext callback, PluginResult.Status status, String retMsg) {
        try {
            JSONObject retJson = new JSONObject(retMsg);
            PluginResult result = new PluginResult(status, retJson);
            result.setKeepCallback(true);
            callback.sendPluginResult(result);
        } catch (Exception e){
            //throw error
            callCallbackContextError(callback, Status.ERROR, "Error creating callback json");
        }
    }

    private void callCallbackContext (CallbackContext callback, PluginResult.Status status, JSONArray retJsonArray) {
        try {
            PluginResult result = new PluginResult(status, retJsonArray);
            result.setKeepCallback(true);
            callback.sendPluginResult(result);
        } catch (Exception e){
            //throw error
            callCallbackContextError(callback, Status.ERROR, "Error creating callback json");
        }
    }

    private void callCallbackContextError(CallbackContext callback, PluginResult.Status status, String msg) {
        PluginResult errorResult = new PluginResult(status , msg);
        errorResult.setKeepCallback(true);
        callback.sendPluginResult(errorResult);
    }

    // ---------- Start Login Methods ----------

    // entry point method to log in a user
    private void login (JSONArray args, CallbackContext callback) {

        try {
            userName = args.getString(0);
            password = args.getString(1);
            appId = args.getString(2);
            appSecret = args.getString(3);

            // check to see if username and password are present
            if (userName.isEmpty() || password.isEmpty()) {
                // error if either is missing, throw error
                callCallbackContextError(callback, Status.ERROR, "Error: missing user name or password");
                return;
            }

            // determine the reachability of the ayla service
            // the AylaUser.login method does these availability/reachability checks but does not return a fine grain error handling.
            // so we do these checks here to determine the exact cause of the issue before making the login call
            boolean waitForResults = true;
            AylaReachability.determineReachability(waitForResults);

            // can take some time to determine if the service is reachable
            serviceReachability = AylaReachability.getConnectivity();

            // if the service is not reachable, try to login using lan mode
            if ( serviceReachability != AylaNetworks.AML_REACHABILITY_REACHABLE ) {
                // if the lan mode is enabled
                if ( AylaSystemUtils.lanModeState != lanMode.DISABLED ) {
                    // double check to see if the entered userName and Password are the same as the
                    // one stored on the phone. if not, throw error
                    if (!userName.equals(savedUserName) || !password.equals(savedPassword)) {

                        callCallbackContextError(callback, Status.ERROR, "Error: saved information does not match");
                        return;
                    }
                } else {
                    // if ayla service is unreachable and the lan mode is disabled, throw an error because
                    // the device cannot communicate with the hub at all
                    callCallbackContextError(callback, Status.ERROR, "Error: Ayla service is unreachable and lan mode is disabled");
                    return;
                }
            }

            // the ayla service is reachable or lan mode is enabled, attempt to login
            userLogin(callback);

        } catch (JSONException e) {
            // do something here to show parsing error
            callCallbackContextError(callback, Status.ERROR, "Error: parsing arguments");
        }

    }

    // helper method used to login to the system
    private void userLogin(CallbackContext callback) {
        UserLoginHandler userLoginHandler = new UserLoginHandler(callback);
        // ayla service it reachable
        if (serviceReachability == AylaNetworks.AML_REACHABILITY_REACHABLE) {
            // Attempt to update the access token using the refresh token

            //TODO: look into how this user is storage into memory. is it secure?
            // load user auth from flash storage
            String jsonUser = AylaSystemUtils.loadSavedSetting("currentUser", "");

            // if we have a user already signed in, use it to log in
            if (!jsonUser.equals("")) {
                // if the username or password are different, normal login since user/pass must be the same to reuse auth info
                if (!userName.equals(savedUserName) || !password.equals(savedPassword)) {
                    AylaUser.login(userLoginHandler, userName, password, appId, appSecret);
                    return;
                }

                AylaUser aylaUser = AylaSystemUtils.gson.fromJson(jsonUser,  AylaUser.class);
                int secondsToExpiry = aylaUser.accessTokenSecondsToExpiry();
                if (secondsToExpiry < AylaNetworks.DEFAULT_ACCESS_TOKEN_REFRESH_THRRESHOLD) {	// 21600 less than six hours
                    AylaUser.login(userLoginHandler, userName, password, appId, appSecret); 			// normal login
                } else {
                    Message msg = new Message();
                    msg.what = AylaNetworks.AML_ERROR_OK;
                    msg.obj = jsonUser;
                    userLoginHandler.handleLoginResponse(msg);
                }
            } else {
                // we do not have a currently a user signed in, so use the provided credentials
                AylaUser.login(userLoginHandler, userName, password, appId, appSecret);
            }
        }
        else {
            // ayla service is not reachable, but lan is enabled
            // need to figure out how to do lan login with the gateway
            return;
        }
    }
    // ---------- End Login Methods ----------


    // ---------- Start Logout Methods ----------
    // entry point method to log out a user
    private void logout (JSONArray args, CallbackContext callback) {
        Map<String, String> callParams = new HashMap<String, String>();
        // get the currently logged in users access token
        callParams.put("access_token",  aylaUser.getAccessToken());
        // call the ayla logout method
        AylaUser.logout(new UserLogoutHandler(callback), callParams);
    }
    // ---------- End Logout Methods ----------


    // ---------- Start RefreshAccessToken Methods ----------
    private void refreshAccessToken (CallbackContext callback, JSONArray args) {

        try {
            String refreshToken = args.getString(0);
            AylaUser.refreshAccessToken(new RefreshAccessTokenHandler(callback), refreshToken);
        } catch (Exception e) {
            callCallbackContextError(callback, Status.ERROR, "Error while attempting to refresh access token : " + e.toString());
        }
    }
    // ---------- End RefreshAccessToken Methods ----------

    // ---------- Start getProperties  Methods ----------
    private void getProperties(CallbackContext callback, JSONArray args) {
        try {
            int deviceId = args.getInt(0);
            GetPropertiesHandler  getPropertiesHandler = new GetPropertiesHandler(deviceId, callback);

            if (this.devices != null) {
                // we already have the devices, get the properties
                getPropertiesHandler.sendDeviceProperites(this.devices);
            } else {
                // get devices and then send device properties
                AylaDevice.getDevices(getPropertiesHandler);
            }

        } catch (Exception e) {
            callCallbackContextError(callback, Status.ERROR, "Error while attempting to get properties : " + e.toString());
        }
    }
    // ---------- End getProperties Methods ----------

    // ---------- Start getDevices Methods ----------
    private void getDevices(CallbackContext callback, JSONArray args) {
        AylaDevice.getDevices(new GetDevicesHandler(callback));
    }
    // ---------- End getDevices Methods ----------

    // ---------- Start CreateDatapoint Methods ----------
    /**
     * Note: This method creates a temporary AylaProperty, Therefor the return property value will be set back on the temporary AylaProperty and then abandoned.
     */
    private void createDatapoint(CallbackContext callback, JSONArray args) {
        try {
            AylaDatapoint datapoint = new AylaDatapoint();

            Number propKey = (Number)args.get(1);
            String baseType = (String)args.get(2);
            AylaProperty property = new AylaProperty(propKey);
            property.baseType = baseType;

            if( args.get(3) instanceof String) {
                datapoint.sValue((String)args.get(3));
            } else {
                datapoint.nValue((Number)args.get(3));
            }

            datapoint.createDatapoint(new CreateDataPointHandler(callback), property);

        } catch (Exception e) {
            callCallbackContextError(callback, Status.ERROR, "Error while attempting to create data point : " + e.toString());
        }
    }
    // ---------- End CreateDatapoint Methods ----------

    // ---------- Start getDatapointsByActivity Methods ----------
    //NOTE:TODO Still under dev.
    private void getDatapointsByActivity(CallbackContext callback, JSONArray args) {
        callCallbackContextError(callback, Status.ERROR, "getDatapointsByActivity not implemented");
    }
    // ---------- End getDatapointsByActivity Methods ----------

    // ---------- Start havePinSet Methods ----------
    // ---------- End havePinSet Methods ----------

    // ---------- Start setPin Methods ----------
    // ---------- End setPin Methods ----------

    // ---------- Start checkPin Methods ----------
    // ---------- End checkPin Methods ----------

    // ---------- Start removePin Methods ----------
    // ---------- End removePin Methods ----------

    // ---------- Start Handler methods ---------------
    private class UserLoginHandler extends Handler{

        private CallbackContext callBack;

        public UserLoginHandler (CallbackContext callBack){
            this.callBack = callBack;
        }

        @Override
        public void handleMessage(Message msg) {
            handleLoginResponse(msg);
        }

        public void handleLoginResponse(Message msg) {
            String jsonResults = (String)msg.obj;

            if (msg.what == AylaNetworks.AML_ERROR_OK) {
                // save auth info of current user
                aylaUser = AylaSystemUtils.gson.fromJson(jsonResults, AylaUser.class);
                aylaUser = AylaUser.setCurrent(aylaUser);

                // persist authentication info
                jsonResults = AylaSystemUtils.gson.toJson(aylaUser,AylaUser.class);
                AylaSystemUtils.saveSetting("currentUser", jsonResults); 	// save refreshToken to flash storage

                //TODO: look into how secure this is
                aylaUser.password = password;

                // we are logged in, so we call the callback
                callCallbackContext(callBack, Status.OK, jsonResults);

            } else{

                if (msg.arg1 == AylaNetworks.AML_IO_EXCEPTION) {
                    //error during login
                    callCallbackContextError(callBack, Status.ERROR, "Error during login");

                } else if(msg.arg1 == AylaNetworks.AML_ERROR_UNREACHABLE) {
                    // No connectivity to WiFi or the cloud. Check your connection.
                    // throw error
                    callCallbackContextError(callBack, Status.ERROR, "Error: No connectivity to WiFi or the cloud. Check your connection");
                } else {
                    // msg.arg1 == 401 : invalid userName or password
                    //throw error
                    callCallbackContextError(callBack, Status.ERROR, "Error: invalid userName or password");
                }
            }
        }
    }


    // Handler method for Logout
    private class UserLogoutHandler extends Handler{
        private CallbackContext callBack;

        public UserLogoutHandler (CallbackContext callBack) {
            this.callBack = callBack;
        }

        @Override
        public void handleMessage(Message msg) {
            try {
                String jsonResultsStr = (String)msg.obj;
                JSONObject resultsJson = new JSONObject(jsonResultsStr);

                Boolean logoutStatus = (Boolean)resultsJson.get("logout");
                if(logoutStatus) {
                    // clear userInfo flash storage
                    AylaSystemUtils.saveSetting("currentUser", "");
                    AylaCache.clearAll();
                    devices = null;
                    callCallbackContext(callBack, Status.OK, jsonResultsStr);
                } else {
                    //throw error
                    callCallbackContextError(callBack, Status.ERROR, "Error: logging out");
                }
            } catch (Exception e){
                //throw error
                callCallbackContextError(callBack, Status.ERROR, "Error while attempting to logout : " + e.toString());
            }
        }
    }

    // Handler method for refreshAccessToken
    private class RefreshAccessTokenHandler extends Handler{
        private CallbackContext callBack;

        public RefreshAccessTokenHandler (CallbackContext callBack) {
            this.callBack = callBack;
        }

        @Override
        public void handleMessage(Message msg) {
            if (msg.what == AylaNetworks.AML_ERROR_OK) {
                try {
                    String jsonResultsStr = (String)msg.obj;
                    JSONObject resultsJson = new JSONObject(jsonResultsStr);

                    // update user saved in memory
                    String accessToken = (String)resultsJson.get("access_token");
                    String refreshToken = (String)resultsJson.get("refresh_token");
                    int expiresIn = (int)resultsJson.get("expires_in");

                    aylaUser.setAccessToken(accessToken);
                    aylaUser.setRefreshToken(refreshToken);
                    aylaUser.setExpiresIn(expiresIn);

                    // save updated user data to disk
                    String userJsonString = AylaSystemUtils.gson.toJson(aylaUser,AylaUser.class);
                    AylaSystemUtils.saveSetting("currentUser", userJsonString);

                    callCallbackContext(callBack, Status.OK, jsonResultsStr);
                } catch (Exception e) {
                    callCallbackContextError(callBack, Status.ERROR, "Error: trying to refresh access token : " + e.toString());
                }
            } else {
                callCallbackContextError(callBack, Status.ERROR, "Error: Refreshing access token");
            }
        }
    }

    // Handler method for getProperties
    private class GetPropertiesHandler extends Handler{

        private int deviceId;
        private CallbackContext callback;

        public GetPropertiesHandler (int deviceId, CallbackContext callback) {
            this.deviceId = deviceId;
            this.callback = callback;
        }

        @Override
        public void handleMessage(Message msg) {
            try{
                String msgString = (String)msg.obj;
                JSONArray retJsonArray = new JSONArray(msgString);

                if (retJsonArray.length() > 0) {
                    ArrayList<AylaDevice> deviceList = new ArrayList<AylaDevice>();
                    for (int i = 0; i < retJsonArray.length(); i++) {
                        AylaDevice aylaDevice = AylaSystemUtils.gson.fromJson(retJsonArray.get(i).toString(),  AylaDevice.class);
                        deviceList.add(aylaDevice);
                    }
                    devices = deviceList;
                    sendDeviceProperites(devices);
                } else {
                    // throw error
                }

            } catch (Exception e) {
                // throw error
                callCallbackContextError(callback, Status.ERROR, "Error: GetPropertiesHandler - parsing json");
            }
        }

        public void sendDeviceProperites(ArrayList<AylaDevice> devices ) {
            AylaDevice selectedDevice  = null;
            for (int i = 0; i < devices.size(); i ++) {
                AylaDevice device = devices.get(i);
                if(device.getKey().intValue() == deviceId) {
                    selectedDevice = device;
                    break;
                }
            }

            if (selectedDevice != null) {
                selectedDevice.getProperties(new SendDevicePropertiesHandler(callback));
            } else {
                // throw 404 error
                callCallbackContextError(callback, Status.ERROR, "Error: 404 - device not found");

            }
        }
    }

    private class SendDevicePropertiesHandler extends Handler{

        private CallbackContext callback;

        public SendDevicePropertiesHandler(CallbackContext callback) {
            this.callback = callback;
        }

        @Override
        public void handleMessage(Message msg) {
            try{
                String msgString = (String)msg.obj;
                JSONArray jsonArray = new JSONArray(msgString);
                JSONArray returnJsonArray = new JSONArray();

                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject prop = jsonArray.getJSONObject(i);
                    JSONObject propJson = new JSONObject();
                    JSONObject subJson = new JSONObject();
                    Boolean isStringType = prop.has("base_type") ? prop.get("base_type").equals("string") : false;

                    if(prop.has("value")) {
                        // if the type isnt a string and the value is a valid int
                        if (!isStringType && checkValueTypeInt((String) prop.get("value"))) {
                            // we have an int, so parse it and add it to the json
                            int value = Integer.parseInt((String)prop.get("value"));
                            propJson.put("value", value);
                        } else {
                            // we are not an int, so just add it to the json
                            propJson.put("value", prop.get("value"));
                        }

                    } else {
                        propJson.put("value", null);
                    }

                    // Property list taken from DeviceProperty.model.js
                    propJson.put("key", prop.has("key") ? prop.get("key") : null);
                    propJson.put("base_type", prop.has("base_type") ? prop.get("base_type") : null);
                    propJson.put("name", prop.has("name") ? prop.get("name") : null);
                    propJson.put("direction", prop.has("direction") ? prop.get("direction") : null);
                    propJson.put("data_updated_at", prop.has("data_updated_at") ? prop.get("data_updated_at") : null);
                    propJson.put("display_name", prop.has("display_name") ? prop.get("display_name") : null);
                    propJson.put("owner", prop.has("owner") ? prop.get("owner") : null);
                    propJson.put("read_only", prop.has("read_only") ? prop.get("read_only") : null);

                    subJson.put("property", propJson);
                    returnJsonArray.put(subJson);
                }

                callCallbackContext(callback, Status.OK, returnJsonArray);

            } catch (Exception e) {
                callCallbackContextError(callback, Status.ERROR, "Error: SendDevicePropertiesHandler - parsing json");

            }
        }
    }

    // helper method to check if a strings value is a
    // a string, or just an int.
    private boolean checkValueTypeInt (String s) {
        if(s.isEmpty()) return false;
        for(int i = 0; i < s.length(); i++) {
            if(i == 0 && s.charAt(i) == '-') {
                if(s.length() == 1) {
                    return false;
                } else continue;
            }
            //base 10
            if(Character.digit(s.charAt(i),10) < 0) {
                return false;
            }
        }
        return true;
    };

    //handler method for the getDevicesHandler call
    private class GetDevicesHandler extends Handler {

        private CallbackContext callBack;

        public GetDevicesHandler (CallbackContext callBack) {
            this.callBack = callBack;
        }

        @Override
        public void handleMessage(Message msg) {
            try{

                if (msg.obj != null) {
                    String msgString = (String) msg.obj;
                    JSONArray parsedJsonArray = new JSONArray(msgString);
                    JSONArray retJsonArray = new JSONArray();

                    ArrayList<AylaDevice> deviceList = new ArrayList<AylaDevice>();
                    for (int i = 0; i < parsedJsonArray.length(); i++) {

                        // get the json string for this device
                        JSONObject deviceJson = parsedJsonArray.getJSONObject(i);

                        // use ayla lib to convert json in to the correct object
                        AylaDevice device = AylaSystemUtils.gson.fromJson(deviceJson.toString(), AylaDevice.class);
                        deviceList.add(device);

                        // convert the device jso to a form we use in the app
                        JSONObject retDeviceJson = parsedJsonArray.getJSONObject(i);
                        retDeviceJson.put("key", device.getKey());
                        retDeviceJson.put("product_name", device.getProductName());
                        retDeviceJson.put("model", device.getModel());
                        retDeviceJson.put("dsn", device.dsn);
                        retDeviceJson.put("oem_model", device.oemModel);
                        retDeviceJson.put("device_type", getDeviceType(device));
                        retDeviceJson.put("connected_at", device.connectedAt);
                        retDeviceJson.put("mac", device.mac);
                        retDeviceJson.put("lan_ip", device.lanIp);
                        retDeviceJson.put("swVersion", device.swVersion);
                        retDeviceJson.put("ssid", device.ssid);
                        retDeviceJson.put("product_class", device.productClass);
                        retDeviceJson.put("has_properties", device.hasProperties);
                        retDeviceJson.put("ip", device.ip);
                        retDeviceJson.put("lan_enabled", device.lanEnabled);
                        retDeviceJson.put("connection_status", device.connectionStatus);
                        retDeviceJson.put("templateId", device.templateId);
                        retDeviceJson.put("lat", device.lat);
                        retDeviceJson.put("lng", device.lng);
                        retDeviceJson.put("userId", device.userID);
                        retDeviceJson.put("moduleUpdatedAt", device.moduleUpdatedAt);
                        retDeviceJson.put("registrationType", device.registrationType);
                        retDeviceJson.put("registrationToken", device.registrationToken);
                        retDeviceJson.put("setupToken", device.setupToken);
                        retDeviceJson.put("gateway_dsn", device instanceof AylaDeviceNode ? ((AylaDeviceNode) device).gatewayDsn : "");

                        retJsonArray.put(retDeviceJson);
                    }

                    devices = deviceList;

                    callCallbackContext(callBack, Status.OK, retJsonArray);
                } else {
                    callCallbackContextError(callBack, Status.ERROR, "");
                }

            } catch (Exception e) {
                // throw error
                callCallbackContextError(callBack, Status.ERROR, "Error: GetDevicesHandler - parsing json");
            }
        }

        private String getDeviceType(AylaDevice device) {
            if (device.isGateway()) {
                return "Gateway";
            }
            if (device.isWifi()){
                return "Wifi";
            }

            if (device.isNode()) {
                return "Node";
            }

            return null;
        }
    }

    //handler method for createDataPoint
    private class CreateDataPointHandler extends Handler {
        private CallbackContext callback;

        public CreateDataPointHandler (CallbackContext callback)
        {
            this.callback = callback;
        }

        @Override
        public void handleMessage(Message msg) {
            try {
                JSONObject retJson = new JSONObject();
                JSONObject parsedMsgJson = new JSONObject();

                String msgString = (String)msg.obj;
                JSONObject msgJson = new JSONObject(msgString);


                if(msgJson.has("value")) {
                    if (checkValueTypeInt((String)msgJson.get("value"))) {
                        // we have an int, so parse it and add it to the json
                        int value = Integer.parseInt((String)msgJson.get("value"));
                        parsedMsgJson.put("value", value);
                    } else {
                        // we are not an int, so just add it to the json
                        parsedMsgJson.put("value", msgJson.get("value"));
                    }

                } else {
                    parsedMsgJson.put("value", null);
                }

                parsedMsgJson.put("created_at", msgJson.has("created_at") ? msgJson.get("created_at") : null);
                parsedMsgJson.put("updated_at", msgJson.has("updated_at") ? msgJson.get("updated_at") : null);
                parsedMsgJson.put("retrieved_at", msgJson.has("retrieved_at") ? msgJson.get("retrieved_at") : null);

                retJson.put("datapoint", parsedMsgJson);
                callCallbackContext(callback, Status.OK, retJson.toString());

            } catch (Exception e) {
                callCallbackContextError(callback, Status.ERROR, "Error: CreateDataPointHandler - parsing json");
            }
        }
    }
    // ---------- End Handler methods ---------------
}