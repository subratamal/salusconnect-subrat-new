/**
 * By specifying that this is node based and setting "use strict" up here, we remove ~106 JSLint errors
 * https://coderwall.com/p/-h1h1w/how-to-use-jslint-in-node-js-projects-properly
 */
/*jslint node: true */
'use strict';

var gulp = require('gulp');
var plumber = require('gulp-plumber');

var debug = require('gulp-debug'); //Usage: .pipe(debug({verbose: false}))
var util = require('gulp-util');
var fs = require('fs');
var nodeUtil = require('util');

var debugOutputMsg = false; //Use this flag to turn on more debugging logging statements when using debugLog method.

var ejsConfig = {
	index: {
		fileName: "index",
		basePath: ""
	},
	officeIndex: {
		fileName: "index",
		basePath: "office"
	},
	officeDetail: {
		fileName: "detail",
		basePath: "office"
	},
	specRunner: {
		fileName: "SpecRunner",
		basePath: "",
		isTestFile: true
	},
	consumerDeployment: {
		fileName: "consumer",
		basePath: ""
	},
	smashedMobile: {
		fileName: "smashedMobile",
		basePath: ""
	}
};

var templateFileBuilderConfig = {
	header: "// this file is auto-generated DO NOT EDIT!\ndefine([], function () {\n\treturn { \n",
	footer: "\t};\n});\n"
};

var templateJSLocations = [
	"web/src/js/consumer/consumerTemplates.js",
	"web/src/js/common/commonTemplates.js",
	"web/src/js/office/officeTemplates.js"
];

// configuration to build out the requireConfig.js file. Used by the generateRequireConfig task
var requireFileBuilderConfig = [
	"// This file is auto-generated, please edit requireConfig.json\n// jshint ignore: start\n",
	"(function () { \n",
	"window.requireConfig = ",
	"<<JSON_FILE>>", // Key to bring in the requireConfig.js file, THIS CANNOT BE THE FIRST LINE IN THE FILE
	";\n",
	"})();\n"
];

// specific require config items for the generation of the smashed consumer.js
// this will be merged with the base config grabbed from web/src/js/requireCongig.json
var smashSpecificRequireConfig = {
	baseUrl: "./web/src/js/lib",
	include: ["require.js", "../requireConfig.js", "../require_main.js"],
	out: "web/js/consumer.js",
	optimize: "uglify2",
	uglify2: {
		mangle: true
	}
};

var paths = {
	requireConfig: 'web/src/js/requireConfig',
	sass: 'web/src/sass/**/*.scss',
	ejs: {
		path: 'web/src/ejs/',
		subpath: {
			config: "config/",
			templates: "templates/"
		}
	},
	tpl: {
		consumer: 'web/src/tpl/consumer/**/*.html',
		office: 'web/src/tpl/office/**/*.html',
		common: 'web/src/tpl/common/**/*.html',
		base: 'web/src/tpl/'
	},
	js: 'web/src/js/',
	destination: {
		webRoot: '/',
		css: 'css',
		js: 'js',
		templates: 'src/js/',
		html: "", // root of the target project web folder
		translations: 'translations'
	},
	buildTypeBasePaths: {
		cordova: "cordova/www/", //This is a base directory which can be used to populate the other two, keep it updated.
		android: "cordova/platforms/android/assets/www/",
		ios: "cordova/platforms/ios/www/",
		web: "web/"
	},
	buildTypeTestPath: "web/tests/",
	translations: 'web/src/translations/**/*.json',
	copyWebWhiteList: ['web/fonts/**', 'web/images/**', 'web/js/**', 'web/src/js/**', '!web/src/js/lib/bootstrap/tests/**/*'],
	webDeploymentWhiteList: ['web/fonts/**/*', 'web/images/**/*', 'web/js/**/*', 'web/translations/**/*', "web/consumer.html", "web/favicon.ico"],
	webOutputFolder: 'web/output/',
	webCSSFiles: 'web/css/*.css',
	cssOutputDirectory: 'web/output/css/',
	jshintOutputName: '/tools/jshint-output'
};

var debugLog = function debugLog(msg) {
	if (debugOutputMsg) {
		util.log(msg);
	}
};

/**
 * Display errors in a more usable way
 * Usage: add ".on('error', errorHandler)" to the end of a .pipe line where you're seeing a generic error and want more details
 */
var errorHandler = function (error) {
	console.log(error.toString());
	this.emit('end');
};

/**
 * grabs a JSON file from the file system and parses it into a js object
 * @param filePath the path to the json file that will be loaded
 * @param if present will return if file not found.
 */
var loadJSON = function (filePath, defaultJson) {
	var path = require('path'),
		fullPath = path.join(__dirname, filePath);

	debugLog(fullPath);


	if (defaultJson) {
		debugLog(defaultJson);

		if (!fs.existsSync(fullPath)) {
			return defaultJson;
		}
	}
	return JSON.parse(fs.readFileSync(fullPath));
};

/**
 * configure output pipes to write to one or more output locations as necessary. By default, this will write to the web, iOS,
 * and android output folders
 * @param pipe the processing pipe to be added to
 * @param destinationType the destination type: this should be a key in paths.destination
 * @param relativePathSuffix if there is a secondary path required within the paths.destination path within the project path
 * @param webOutputOnly only output to the web project.
 * @param testOutputOnly used to show only the output of this path
 * @returns {*} the updated pipe
 */
var configureOutputPipes = function (pipe, destinationType, relativePathSuffix, webOutputOnly, testOutputOnly) {
	var path, relPath, pipeObj, buildPathType,
		buildPaths = paths.buildTypeBasePaths;

	if (webOutputOnly) {
		debugLog("ConfigureOutputPipes web output only :" + webOutputOnly);
		buildPaths = [paths.buildTypeBasePaths.web]; // this is a special case
	}

	debugLog("ConfigureOutputPipes buildPath count:" + Object.keys(buildPaths).length);

	if (testOutputOnly) {
		buildPaths = [paths.buildTypeTestPath];
	}

	relPath = paths.destination[destinationType];
	pipeObj = pipe;

	for (buildPathType in buildPaths) {
		if (buildPaths.hasOwnProperty(buildPathType)) {
			path = buildPaths[buildPathType] + relPath + relativePathSuffix;
			debugLog("ConfigureOutputPipes adding dest: " + path);
			pipeObj = pipeObj.pipe(gulp.dest(path));
		}
	}

	return pipeObj;
};

var templateFilesToJson = function (filename, namespace) {
	var order = require('gulp-order'),
		changed = require('gulp-changed'),
		toJson = require("gulp-to-json");

	var destination = paths.tpl.base + namespace;

	return gulp.src(paths.tpl[namespace])
		.pipe(order([]))
		.pipe(toJson({
			filename: paths.tpl.base + namespace + "/" + filename,
			relative: true
		}))
		.pipe(changed(destination))
		.pipe(gulp.dest(destination))
};

/**
 * compile HTML written Underscore templates into js snippets for use by client side js
 * @param userType corresponds to items in the paths.tpl, which specifies the template src path
 * @param namespace the namespace to write the js snippets too, they will live at window[namespace]
 */
var compileUnderscoreTemplates = function (jsonFileName, namespace) {
	var _ = require("underscore");
	var templateFiles = loadJSON(paths.tpl.base + namespace + "/" + jsonFileName),
		destinationPath = paths.destination.templates + namespace + "/",
		outFilePath = paths.buildTypeBasePaths.web + destinationPath + namespace + "Templates.js";


		fs.writeFileSync(outFilePath, templateFileBuilderConfig.header);

		templateFiles.forEach(function (path, index) {
			var templateString = fs.readFileSync(paths.tpl.base + namespace + "/" + path, {encoding: 'utf-8'}),
				templateFunc = _.template(templateString).source.replace(/[\r\n\t]+/gm, ''), // this replace removes new lines so function lives on one line
				fileLine = '\t\t"' + path.replace(".html", "") + '": ' + templateFunc;
				if (index + 1 !== templateFiles.length) {
					fileLine += ",";
				}

				fileLine += "\n";
				fs.appendFileSync(outFilePath, fileLine);
		});

		fs.appendFileSync(outFilePath, templateFileBuilderConfig.footer);

		//var minifyHTML = require('gulp-minify-html');
		// leaving this commented out until i have a chance to talk to brian normington
		// pipes = gulp.src(path)
		// 	.pipe(order([]))
		// 	.pipe(template(templateSettings).on('error', errorHandler))
		// 	.pipe(concat(namespace + "Templates.js"))
		// 	.pipe(minifyHTML(opts))
		// 	.pipe(gulp.dest(paths.buildTypeBasePaths.web + destinationPath))
		// 	.pipe(replace(/src=([\"]?)\//g, 'src=$1./'))
		// 	.pipe(gulp.dest(paths.buildTypeBasePaths.ios + destinationPath))
		// 	.pipe(gulp.dest(paths.buildTypeBasePaths.android + destinationPath));
};

var templateAppProcessing = function (destinationPath) {
	var replace = require('gulp-replace');
	return gulp.src(templateJSLocations)
		.pipe(replace(/src=([\"]?)\//g, 'src=$1./'))
		.pipe(gulp.dest(paths.buildTypeBasePaths.ios + destinationPath))
		.pipe(gulp.dest(paths.buildTypeBasePaths.android + destinationPath))
		.pipe(gulp.dest(paths.buildTypeBasePaths.cordova + destinationPath));
};

var compileConsumerTemplates = function () {
	compileUnderscoreTemplates("consumerTemplates.json", 'consumer');
	return templateAppProcessing("src/js/consumer/");
};

gulp.task('consumerTemplateCompile', ["consumerTemplateFilesToJson"], function () {
	return compileConsumerTemplates();
});

gulp.task('officeTemplateCompile', ["officeTemplateFilesToJson"], function () {
	compileUnderscoreTemplates("officeTemplates.json", 'office');
	return templateAppProcessing("src/js/consumer/");
});

gulp.task('commonTemplateCompile', ["commonTemplateFilesToJson"], function () {
	compileUnderscoreTemplates("commonTemplates.json", 'common');
	return templateAppProcessing("src/js/common/");
});

gulp.task('consumerTemplateFilesToJson', function () {
	return templateFilesToJson("consumerTemplates.json", 'consumer');
});

gulp.task('officeTemplateFilesToJson', function () {
	return templateFilesToJson("officeTemplates.json", 'office');
});

gulp.task('commonTemplateFilesToJson', function () {
	return templateFilesToJson("commonTemplates.json", 'common');
});

// Compile Our Sass
gulp.task('sass', function () {
	var sass = require('gulp-sass'),
		replace = require('gulp-replace'),
		pipes = gulp.src(paths.sass)
			.pipe(plumber())
			.pipe(sass({"errLogToConsole": true}))
			.pipe(gulp.dest(paths.buildTypeBasePaths.web + paths.destination.css))
			.pipe(replace(/url\(\"\//g, 'url("../'))
			.pipe(gulp.dest(paths.buildTypeBasePaths.ios + paths.destination.css))
			.pipe(gulp.dest(paths.buildTypeBasePaths.android + paths.destination.css))
			.pipe(gulp.dest(paths.buildTypeBasePaths.cordova + paths.destination.css));

	return pipes;
});

gulp.task("minifyCss", function () {
	var minifyCSS = require("gulp-clean-css"),

	pipes = gulp.src(paths.webCSSFiles)
		.pipe(plumber())
		.pipe(minifyCSS())
		.pipe(gulp.dest(paths.cssOutputDirectory))
})

var copyTranslationFiles = function (srcPipe) {
	var changed = require('gulp-changed'),
		jsonlint = require('gulp-jsonlint'),
		jsonminify = require('gulp-jsonminify'),
		webDestinationPath = paths.buildTypeBasePaths.web + paths.destination.translations + "/",
		pipes = srcPipe
			.pipe(plumber())
			.pipe(changed(webDestinationPath))
			.pipe(jsonlint())
			.pipe(jsonlint.reporter())
			.pipe(jsonminify());

	return configureOutputPipes(pipes, "translations", "", false, false);
}

gulp.task('copyTranslations', function () {
	return copyTranslationFiles(gulp.src(paths.translations));
});

gulp.task('copyCordovaIos', [], function () {
	var changed = require('gulp-changed');

	return gulp.src("cordova/platforms/ios/platform_www/**/*")
		.pipe(plumber())
		.pipe(changed(paths.buildTypeBasePaths.ios))
		.pipe(gulp.dest(paths.buildTypeBasePaths.ios));
});

gulp.task('copyCordovaAndroid', [], function () {
	var changed = require('gulp-changed');

	return gulp.src("cordova/platforms/android/platform_www/**/*")
			.pipe(plumber())
			.pipe(changed(paths.buildTypeBasePaths.android))
			.pipe(gulp.dest(paths.buildTypeBasePaths.android));
});

var copyWebFiles = function (glob) {
	var changed = require('gulp-changed');

	return gulp.src(glob, {base: 'web/', buffer: false})
		.pipe(plumber())
		//Using ios as the reference directory, android should be the same.
		.pipe(changed(paths.buildTypeBasePaths.ios))
		.pipe(gulp.dest(paths.buildTypeBasePaths.ios))
		.pipe(gulp.dest(paths.buildTypeBasePaths.android))
		.pipe(gulp.dest(paths.buildTypeBasePaths.cordova));
};

gulp.task('copyWebFiles', ["copyCordovaIos", "copyCordovaAndroid"], function () {
	return copyWebFiles(paths.copyWebWhiteList);
});

gulp.task('cleanTranslations', function (cb) {
	var del = require('del'),
		path = [paths.buildTypeBasePaths.web + paths.destination.translations + '/**/*'];

	// TODO: do ios and android
	del(path, cb);
});

gulp.task('cleanApps', function (cb) {
	var del = require('del'),
		path = [
			paths.buildTypeBasePaths.android + '/**/*',
			"!" + paths.buildTypeBasePaths.android + '/cordova*',
			paths.buildTypeBasePaths.ios + '/**/*',
			"!" + paths.buildTypeBasePaths.ios + '/cordova*',
			paths.buildTypeBasePaths.cordova + '/**/*',
			"!" + paths.buildTypeBasePaths.cordova + '/cordova*'
		];

	del(path, cb);
});

gulp.task('cleanWeb', function (cb) {
	var del = require('del'),
		path = [
			"web/translations/*",
			"web/css/*",
			"web/js/*",
			"web/src/js/*/*Templates.js",
			"web/index.html",
			"web/office/*.html"
		];

	del(path, cb);
});

gulp.task('cleanJshint', [], function (cb) {
	var del = require('del'),
		path = [ "." + paths.jshintOutputName + "*"];

	del(path, cb);
});

gulp.task('clean', ['cleanWeb', 'cleanApps', 'cleanTranslations']);

gulp.task('default', ['watch']);


/**
 *To set the build number run
 * gulp --versionNumber=0.0.2 --buildNumber=23232 --releaseDate="2/18/2016"  --environment=dev ejsCompile
 */
gulp.task('ejsCompile', function () {
	var key, target, basePath, jsonPath, contentJson, json, ejsPath, pipes, destPath, isWebOnly,
		ejs = require('gulp-ejs'),
		replace = require('gulp-replace'),
		globalJson = loadJSON(paths.ejs.path + paths.ejs.subpath.config + '_globals.json'),

		versionNumber = util.env.versionNumber || "0.0.1",
		buildNumber =  util.env.buildNumber || "dev",
		buildDate = (new Date()).toUTCString(),
		releaseDate = util.env.releaseDate ? (new Date(util.env.releaseDate)).toUTCString() : null,
		build = {
			versionNumber: versionNumber,
			buildNumber: buildNumber,
			buildDate: buildDate,
			releaseDate: releaseDate
		},
		buildJson = JSON.stringify(build),
		environment = util.env.environment || "local",
		cordovaReplacment = '<script src=\"./cordova.js\"></script> ' +
			'<script type=\"application/javascript\">window.requireConfig.baseUrl=\"./src/js/lib\";</script>',
		inlineJsReplacement ='<script type=\"application/javascript\">window.environment=\"' + environment + '\";window.build=' + buildJson + ';</script>',
		minifyHTML = require('gulp-minify-html');

	console.log("Building for: " + environment + " = " + buildJson);

	for (key in ejsConfig) {
		if (ejsConfig.hasOwnProperty(key)) {
			debugLog("EjsComile using key: " + key);

			target = ejsConfig[key];
			basePath = target.basePath ? target.basePath + "/" : "";
			jsonPath = paths.ejs.path + paths.ejs.subpath.config + basePath + target.fileName + ".json";
			contentJson = loadJSON(jsonPath);
			json = nodeUtil._extend(globalJson, contentJson);
			ejsPath = paths.ejs.path + paths.ejs.subpath.templates + basePath + target.fileName + ".ejs";
			destPath = paths.destination.html + basePath;
			pipes = gulp.src(ejsPath)
				.pipe(plumber())
				.pipe(ejs(json));

			if (target.isTestFile) {
				pipes.pipe(gulp.dest(paths.buildTypeTestPath + destPath));
			} else {
				pipes.pipe(replace(/<!-- inline-js -->/g, inlineJsReplacement));
				isWebOnly = key.indexOf("consumer") !== -1 || key.indexOf("office") !== -1;
				if (key.indexOf("index") >= 0) {
					pipes.pipe(minifyHTML({comments: true}))
						.pipe(gulp.dest(paths.buildTypeBasePaths.web + destPath))
						.pipe(replace(/(src|href)=([\"]?)\//g, '$1=$2./'))
						.pipe(replace(/<!--cordova files-->/g, cordovaReplacment))
						.pipe(gulp.dest(paths.buildTypeBasePaths.ios + destPath))
						.pipe(gulp.dest(paths.buildTypeBasePaths.android + destPath))
						.pipe(gulp.dest(paths.buildTypeBasePaths.cordova + destPath));
				} else if (key.indexOf("consumer") >= 0) {
					pipes.pipe(minifyHTML())
						.pipe(gulp.dest(paths.buildTypeBasePaths.web + destPath));
				} else if (key.indexOf("smashedMobile") >= 0) {
					pipes.pipe(replace(/(src|href)=([\"]?)\//g, '$1=$2./'))
						.pipe(replace(/<!--cordova files-->/g, cordovaReplacment))
						.pipe(minifyHTML())
						.pipe(gulp.dest(paths.buildTypeBasePaths.ios + destPath))
						.pipe(gulp.dest(paths.buildTypeBasePaths.android + destPath))
						.pipe(gulp.dest(paths.buildTypeBasePaths.cordova + destPath));
				}
			}
			//configureOutputPipes(pipes, "html", basePath, isWebOnly, target.isTestFile);
		}
	}
});

gulp.task('cleanOutput', function (cb) {
	var del = require('del'),
		path = paths.webOutputFolder + "*";

	del(path, cb);
});

gulp.task('prepareForDeployment', ['cleanOutput'], function () {
	gulp.start("minifyCss")
	gulp.src(paths.webDeploymentWhiteList, { base: "web" })
		.pipe(plumber())
		.pipe(gulp.dest(paths.webOutputFolder));
});

gulp.task('generateRequireConfig', function () {
	var config = loadJSON(paths.requireConfig + ".json");

	requireFileBuilderConfig.forEach(function (line, index) {
		if (index === 0) {
			fs.writeFileSync(paths.requireConfig + ".js", line);
		} else {
			if (line === "<<JSON_FILE>>") {
				line = JSON.stringify(config);
			}

			fs.appendFileSync(paths.requireConfig + ".js", line);
		}
	});
});

gulp.task('requireSmash', ['generateRequireConfig', 'consumerTemplateCompile'], function () {
	var requireJS = require("requirejs"),
		requireConfig = loadJSON(paths.requireConfig + ".json"), // default requireConfig
		config = nodeUtil._extend(requireConfig, smashSpecificRequireConfig); // merge specific require config with default config

	// smashes js into one consumer.js file.
	requireJS.optimize(config);
});

gulp.task('buildDev', ["clean"], function () {
	gulp.start("ejsCompile", "sass", 'consumerTemplateCompile', 'commonTemplateCompile',
		'copyTranslations', "copyWebFiles");
});

gulp.task('buildForDeployment', function () {
	gulp.start('requireSmash');
});

// JS Lint Task
gulp.task('lint', ['cleanJshint'], function () {
	var jshint = require('gulp-jshint'),
		jsintlogPath = __dirname + paths.jshintOutputName,
		now = new Date(),
		extension = "-" + now.getTime() + ".txt";  //Need a unique name for each run so the log file gets rotated.

//	var pathArray = paths.javaScripts.slice(0); //Copy the array.
//	pathArray.push("!public/src/javascripts/templates.js"); //ignore the generated templates file.

	return gulp.src(['web/src/js/**', '!web/src/js/lib/**', '!web/src/js/**/*Templates.js', 'translations/**.js'])
		.pipe(plumber())
		.pipe(jshint())
		.pipe(jshint.reporter('default'))
		.pipe(jshint.reporter('gulp-jshint-file-reporter',
			{ filename: jsintlogPath + extension    }));
});

gulp.task("watch", ["buildDev", "generateRequireConfig", "lint", "consumerTemplateFilesToJson"], function () {
	var jshint = require('gulp-jshint');

	gulp.watch(paths.ejs.path + "**/*.ejs", ["ejsCompile"]);
	gulp.watch([paths.sass], function () {
		gulp.start("sass");
	});

	gulp.watch(paths.tpl.consumer, function (obj) {
		util.log(obj.path);
		if (obj.type === 'changed') {
			compileConsumerTemplates();
		} else {
			util.log(obj.type);
		}
	});

	//gulp.watch(paths.tpl.office, ['officeTemplateCompile']); we have no office templates or folders at the moment
	gulp.watch(paths.tpl.common, ['commonTemplateCompile']);
	gulp.watch(paths.translations, ['copyTranslations']);
	gulp.watch(paths.requireConfig + ".json", ['generateRequireConfig']);

	//watch web files like images, etc....
	gulp.watch(paths.copyWebWhiteList, function (obj) {
		if (obj.type === 'changed') {
			copyWebFiles(obj.path);
		}
	});

	gulp.watch(['web/src/js/**', '!web/src/js/lib/**', '!web/src/js/**/*Templates.js'], function (obj) {
		//Single file check
		gulp.src(obj.path)
			.pipe(plumber())
			.pipe(jshint())
			.pipe(jshint.reporter('default'));

		//Check everything
		gulp.start("lint");
	});
});


gulp.task('fixupLanguageFiles', [], function () {
	var stringify = require('json-stable-stringify');
	var modify = require('gulp-modify');
	
	gulp.src('web/src/translations/**/consumer.json')
		.pipe(modify({
		    fileModifier: function(file, contents) {
				var contentAsObject = JSON.parse(contents);					
				return stringify(contentAsObject, {space: '\t' });
			}
		}))
		.pipe(gulp.dest('web/src/translations'));	
});


var ejsEmailConfig = {
	paths: {
		ejsSrc: "email/src/ejs/",
		output: "email/output/"
	},
	aylaEnvironments: ["dev", "eu"],
	devEnvironments: ["local", "int", "staging"],
	languages: ["en", "en-uk", "nl", "ro", "ru", "fr", "sv", "da", "de", "pl", "no"],
	files: {
		guest_signup_confirmation: {
			fileName: "guest_signup_confirmation",
			excludeDeviceImages: true,
			templateDescription: "Guest sign up confimation email with a url back to the web site."
		},
		password_reset: {
			fileName: "password_reset",
			basePath: "",
			excludeDeviceImages: true,
			templateDescription: "Users password reset email with a url back to the web site."
		},
		signup_confirmation: {
			fileName: "signup_confirmation",
			excludeDeviceImages: true,
			templateDescription: "Sign up confirmation email with a url back to the web site"
		},
		dynamic_body: {
			fileName: "dynamic_body",
			excludeDeviceImages: true,
			templateDescription: "Hello userName framed email with an open body. Send email_body_html for a dynamic body content. "
		},
		dynamic_body_no_devices: {
			fileName: "dynamic_body",
			jsonFileName: "dynamic_body_no_devices",
			excludeDeviceImages: true,
			templateDescription: "Hello userName framed email with an open body. Send email_body_html for a dynamic body content withou link to mobil app "
		},
		trigger_app: {
			fileName: "trigger_app",
			excludeDeviceImages: true,
			templateDescription: "Basic e-mail template for trigger app usage"
		},
		resource_share: {
			fileName: "resource_share",
			excludeDeviceImages: true,
			templateDescription: "Resource Sharing email without device links"
		}
	}
};

gulp.task('ejsEmailCompile', ['cleanEmailsHtml'], function () {
	var key, target, basePath, jsonPath, contentJson, json, ejsPath, destBasePath,
		jsonFileName,
		ejs = require('gulp-ejs'),
		replace = require('gulp-replace'),
		globalJson = loadJSON('email/src/ejs/config/_globals.json'),
		rename = require("gulp-rename"),
		_ = require("underscore");

	var defaultLanguageJson = loadJSON("web/src/translations/en/consumer.json", {});


	interateEnvironments(function (file, key, env, lang) {
		
		debugLog("EjsEmailComile using key: " + key);

		target = ejsEmailConfig.files[key];
		basePath = target.basePath ? target.basePath + "/" : "";
		jsonFileName = target.jsonFileName || target.fileName;

		jsonPath = ejsEmailConfig.paths.ejsSrc + "config/" + basePath + jsonFileName + ".json";
		contentJson = loadJSON(jsonPath, {});
		
		

		json = _.extend({}, globalJson, contentJson, defaultLanguageJson);

		ejsPath = ejsEmailConfig.paths.ejsSrc + "templates/" + basePath + target.fileName + ".ejs";
		destBasePath = ejsEmailConfig.paths.output + basePath;
		
		
		var environmentSuffix = lang ? "_" + lang : "";
		var directoryName = key  + environmentSuffix;
		var destDirectory = destBasePath + env + "/" + directoryName + "/";
		
		var salsuEnvConentJson =  loadJSON(ejsEmailConfig.paths.ejsSrc + "config/_" + env + ".json", {});

		var envConentJson = loadJSON(ejsEmailConfig.paths.ejsSrc + "config/_" + lang + ".json", {});
		
		var languageJson = loadJSON("web/src/translations/" + lang + "/consumer.json", {});

		var finalJson = _.extend({}, json, salsuEnvConentJson, envConentJson, languageJson);

		gulp.src(ejsPath)
			.pipe(plumber())
			.pipe(ejs(finalJson))
			.on('error', util.log)
			.pipe(rename(function (path) {
				path.basename = "template";
			}))
			.pipe(gulp.dest(destDirectory))
		
	});
	
});

/**
 * Upload email templates to https://dashboard-dev.aylanetworks.com/oems/213/email_templates
 */
gulp.task("watchEmails", ['ejsEmailCompile'], function () {
	// zip -j -r -X email/output/password_reset_local.zip  email/output/password_reset_local/*

	gulp.watch(["email/src/ejs/**/**.*js*", paths.translations], function (obj) {
		util.log(obj.path);
		gulp.start("ejsEmailCompile");
	});
});

gulp.task("cleanEmailLogoImages", function (cb) {
	var del = require('del'),
		path = [ejsEmailConfig.paths.output + "**/logo_image.png"];
	del(path, cb);
});

gulp.task("cleanEmailDeviceImages", function (cb) {
	var del = require('del'),
		path = [ejsEmailConfig.paths.output + "**/*link_image.png"];
	del(path, cb);
});

gulp.task("cleanEmailsHtml", function (cb) {
	var del = require('del'),
		path = [ejsEmailConfig.paths.output + "**/*.ejs",
			ejsEmailConfig.paths.output + "**/*.html"];

	del(path, cb);
});


var interateEnvironments = function (delegate) {
	var key;
	var environments = (ejsEmailConfig.aylaEnvironments || [""])
	
	for (var j = 0; j < environments.length; j++) {
		var env = environments[j];
		
		var languages = ejsEmailConfig.languages;
		if (env === "dev") {
			languages = languages.concat(ejsEmailConfig.devEnvironments || [""]);
		}
	
		for (key in ejsEmailConfig.files) {
			if (ejsEmailConfig.files.hasOwnProperty(key)) {
				for (var i = 0; i < languages.length; i++) {
					var lang = languages[i];
					delegate(ejsEmailConfig.files[key], key, env, lang, i);
				}
			}
		}
	}
}

gulp.task("copyEmailDeviceImages", ['cleanEmailDeviceImages'], function () {
	var pipes = gulp.src("email/src/images/*link_image.png")
		.pipe(debug({verbose: false}))
		.pipe(plumber());

	//We are going to add a bunch of destination directories so bump up the max listeners count.
	//This is to fix the following error:
	// warning: possible EventEmitter memory leak detected. 11 listeners added.
	pipes.setMaxListeners(200);

	interateEnvironments(function (file, key, env, lang) {
		if (!file.excludeDeviceImages) {
			var environmentSuffix = lang ? "_" + lang : "";
			var directoryName = key + environmentSuffix;

			var destDirectory = ejsEmailConfig.paths.output  + env + "/" + directoryName + "/";

			debugLog(destDirectory);
			pipes.pipe(gulp.dest(destDirectory));
		}
	});

	return pipes;
});


gulp.task("copyEmailLogoImages", ['cleanEmailLogoImages'], function () {
	var pipes = gulp.src("email/src/images/logo_image.png")
		.pipe(debug({verbose: false}))
		.pipe(plumber());

	//We are going to add a bunch of destination directories so bump up the max listeners count.
	//This is to fix the following error:
	// warning: possible EventEmitter memory leak detected. 11 listeners added.
	pipes.setMaxListeners(200);

	interateEnvironments(function (file, key, env, lang) {
		var environmentSuffix = lang ? "_" + lang : "";
		var directoryName = key + environmentSuffix;

		var destDirectory = ejsEmailConfig.paths.output + env + "/" + directoryName + "/";

		debugLog(destDirectory);
		pipes.pipe(gulp.dest(destDirectory));
	});

	return pipes;
});

/**
 * Task that genrates a string to put into terminal to create the ayla email template.
 * The cookies header will need to changes as they will probably be to old.
 */
gulp.task("emailuploads", [], function () {
	var languages = ejsEmailConfig.languages, key, templateId, description,
		languageNames = {
			"da": "Danish",
			"de": "German",
			"en-uk": "English (UK)",
			"fr": "French",
			"nl": "Dutch",
			"no": "Norwegian",
			"pl": "Polish",
			"ro": "Romanian",
			"ru": "Russian",
			"sv": "Swedish"
		};


	for (key in ejsEmailConfig.files) {
		if (ejsEmailConfig.files.hasOwnProperty(key)) {
			for (var i = 0; i < languages.length; i++) {
				var lang = languages[i];

				templateId = key + "_" + lang;
				description = languageNames[lang] + ": " + ejsEmailConfig.files[key].templateDescription;

				util.log("curl 'https://dashboard-eu.aylanetworks.com/oems/17/email_templates' -H 'Cookie: hsfirstvisit=https%3A%2F%2Fwww.aylanetworks.com%2F|https%3A%2F%2Fwww.google.com%2F|1434127190364; __hstc=78646731.dc35d57a76eb7ef003db595ca429770a.1434127190367.1440800094351.1441060843218.7; hubspotutk=dc35d57a76eb7ef003db595ca429770a; _ga=GA1.2.604598025.1434127190; rack.session=BAh7CkkiD3Nlc3Npb25faWQGOgZFRiJFNjlhZTRjMDY0YzgzNGI1N2Q2MmY3%0AZGYxYTFiYTgxYzIwYzExMjFiOTI4NzkxYWZjNDFiYzY5NGJkNGJkOWE4MUki%0AC19mbGFzaAY7AEZ7AEkiEWFjY2Vzc190b2tlbgY7AEZJIiUxOTJkZDk3Y2Y4%0AMmY0NzUxYTZiOTc2ODhhNzRkY2JkZgY7AFRJIhJyZWZyZXNoX3Rva2VuBjsA%0ARkkiJTg2NmU4MGYwNjZhOTQ4N2M5ZTdlM2YzZjIyNTYwMjA1BjsAVEkiC29l%0AbV9pZAY7AEZpFg%3D%3D%0A--c9c611c8692f62c78eedaf7ec8a67a44d2021132; AWSELB=41C915C3144283A999D7E69BAC96356A46E33EEABF9099D7005EF86917C00285114F754A53DD1B9C24389B2E7B7A5628889B991C1AAAC83CB466AEE76A5995C57327B12C8A3CE779F43C5C53D6CC10E9E80F67ABC94C0076CFB8B5EC53D92A27D7D203DA68' -H 'Origin: https://dashboard-eu.aylanetworks.com'  -H 'Content-Type: multipart/form-data; boundary=----WebKitFormBoundarywwsZMPdRuml3ppPB' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Cache-Control: max-age=0' -H 'Referer: https://dashboard-eu.aylanetworks.com/oems/17/email_templates/new' -H 'Connection: keep-alive' --data-binary $'------WebKitFormBoundarywwsZMPdRuml3ppPB\\r\\nContent-Disposition: form-data; name=\"email_template[name]\"\\r\\n\\r\\n" + templateId + "\\r\\n------WebKitFormBoundarywwsZMPdRuml3ppPB\\r\\nContent-Disposition: form-data; name=\"email_template[description]\"\\r\\n\\r\\n" + description + "\\r\\n------WebKitFormBoundarywwsZMPdRuml3ppPB\\r\\nContent-Disposition: form-data; name=\"email_template[unique_id]\"\\r\\n\\r\\n" + templateId + "\\r\\n------WebKitFormBoundarywwsZMPdRuml3ppPB--\\r\\n' --compressed");
			}
		}
	}
});


gulp.task("copyEmailsImages", ['copyEmailLogoImages', "copyEmailDeviceImages"]);


/**
 * Upload email templates to https://dashboard-dev.aylanetworks.com/oems/213/email_templates
 */
gulp.task("generateEmails", ['copyEmailsImages', 'ejsEmailCompile'], function () {

	interateEnvironments(function (file, key, env, lang) {
		var environmentSuffix = lang ? "_" + lang : "";
		var directoryName = key + environmentSuffix;

		var zipPath = ejsEmailConfig.paths.output + env + "/" + directoryName + ".zip";
		var filesPattern = ejsEmailConfig.paths.output + env + "/" + directoryName + "/*";

		//TODO shell out to the system to actuall do this command.
		util.log("zip -j -r -X " + zipPath + " " + filesPattern);
	});
});


gulp.task("lintTranslationFiles", [], function () {
	var jsonlint = require('gulp-jsonlint');

	return gulp.src(['sourceTranslations/**.json'])
			.pipe(jsonlint())
			.pipe(jsonlint.reporter());
});

var isObject = function(a) {
	return (!!a) && (a.constructor === Object);
};

var proccessNode = function (source, language, keyPath, logStream) {
	var node = {}, key;

	if (isObject(source)) {
		for (key in source) {
			if (source.hasOwnProperty(key)) {

				var localKePath = keyPath ? keyPath + "." + key : key;

				if(language.hasOwnProperty(key)) {
					node[key] = proccessNode(source[key], language[key],  localKePath, logStream);

				} else {

					var sourceText = JSON.stringify(source[key]);


					var text = "Missing key: " + localKePath +  "    source:" +  sourceText;
					console.log(text);
					logStream.write(text + "\n");
					node[key] = source[key];
				}
			}
		}
		return node;
	}

	return language || source;
};


/**
 * The translation files we recieve from the client are not valid so we run the following regex find and replace (Intellij):
 *		Find: ^(\t*)([0-9a-zA-Z-_]*):
 * 	 Replace: $1\"$2\":
 */
gulp.task("importTranslationFiles", ["lintTranslationFiles"], function () {

	var languageFiles = ["DA", "DE", "EN-UK", "FR", "NL", "NO", "PL", "RO", "RU", "SV"],
		countryCode,
		sourceObjectEn = loadJSON("web/src/translations/en/consumer.json");


	if (!fs.existsSync("sourceTranslations/output")){
		fs.mkdirSync("sourceTranslations/output");
	}

	for (var i = 0; i < languageFiles.length; i++) {
		countryCode = languageFiles[i];


		var destinationFolder = "web/src/translations/" +  countryCode.toLowerCase();
		var destinationFile =  destinationFolder  + "/consumer.json";
		console.log('Stating with file: ' + destinationFile);

		if (!fs.existsSync(destinationFolder)){
			fs.mkdirSync(destinationFolder);
		}

		var logStream = fs.createWriteStream("sourceTranslations/output/en_consumer_" + countryCode + '-log.txt', {'flags': 'w'});

		var sourceLanguageObject = loadJSON("sourceTranslations/en_consumer_" + countryCode +".json");

		var combine = proccessNode(sourceObjectEn, sourceLanguageObject, "",  logStream);
		var stringJsong = JSON.stringify(combine, null, "\t");

		logStream.end();

		//our file
		fs.writeFileSync(destinationFile, stringJsong);
		//Language file to send back
		fs.writeFileSync("sourceTranslations/output/en_consumer_" + countryCode + ".json", stringJsong);

		console.log('Done with file: ' + destinationFile);
	}
});


var addBBExtendModelConstructors = function () {
	var estream = require("event-stream");
	return estream.map(function (stream, cb) {
		var path = "Salus_model_" + stream.path.substr(stream.base.length).replace(".js", "").replace(/[^A-z0-9_]/g, "_");
		var src = stream.contents.toString('utf8').replace(/(([A-z0-9_]+)\s*=\s*(B\.[A-z]*Model)\.extend\(\{)/g, "$1\n constructor: function " + path + "_$2(){ return $3.apply(this, arguments); },");
		stream.contents = new Buffer(src);
		cb(null, stream);
	});
};

var addMnExtendViewConstructors = function () {
	var estream = require("event-stream");
	return estream.map(function (stream, cb) {
		var path = "Salus_" + stream.path.substr(stream.base.length).replace(".js", "").replace(/[^A-z0-9_]/g, "_");
		var src = stream.contents.toString('utf8').replace(/(([A-z0-9_]+)\s*=\s*(Mn\.[A-z]*View)\.extend\(\{)/g, "$1\n constructor: function " + path + "_$2(){ return $3.apply(this, arguments); },");
		stream.contents = new Buffer(src);
		cb(null, stream);
	});
};


gulp.task("debugModels", function () {
	gulp.src('web/src/js/common/model/api/*.js')
		.pipe(addBBExtendModelConstructors())
		.pipe(gulp.dest('web/src/js/common/model/api/'));
});

gulp.task("debugViews", function () {
	gulp.src('web/src/js/**/*.js')
		.pipe(addMnExtendViewConstructors())
		.pipe(gulp.dest('web/src/js/'));
});

var cleanDebugConstructors = function () {
	var estream = require("event-stream");
	var reg = /^ ?constructor: function Salus_[A-z0-9_]+ ?\(\)\{ return [A-z0-9_]+.[A-z0-9_]+.apply\(this, arguments\); },\n/gm;
	return estream.map(function (stream, cb) {
		var src = stream.contents.toString('utf8').replace(reg, "");
		stream.contents = new Buffer(src);
		cb(null, stream);
	});
};

gulp.task("debugClean", function () {
	gulp.src('web/src/js/common/model/api/*.js')
		.pipe(cleanDebugConstructors())
		.pipe(gulp.dest('web/src/js/common/model/api/'));
});